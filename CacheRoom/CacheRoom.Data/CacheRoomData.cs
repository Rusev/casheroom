﻿namespace CacheRoom.Data
{
    using System;
    using System.Collections.Generic;

    using CacheRoom.Data.Contracts;
    using CacheRoom.Models;
    using CacheRoom.Models.Config;
    using CacheRoom.Models.Econt;

    using Repositories;

    public class CacheRoomData:ICacheRoomData
    {
        public CacheRoomDbContext context;

        private IDictionary<Type, object> repositories; 
        public CacheRoomData(CacheRoomDbContext context)
        {
            this.context = context;
            this.repositories = new Dictionary<Type, object>();
        }

        public IRepository<User> Users
        {
            get
            {
                return this.GetRepository<User>();
            }
        }

        public IRepository<Brand> Brands
        {
            get
            {
                return this.GetRepository<Brand>();
            }
        }

        public IRepository<CartItem> CartItems
        {
            get
            {
                return this.GetRepository<CartItem>();
            }
        }

        public IRepository<Category> Categories
        {
            get
            {
                return this.GetRepository<Category>();
            }
        }

        public IRepository<Collection> Collections
        {
            get
            {
                return this.GetRepository<Collection>();
            }
        }

        public IRepository<Product> Products
        {
            get
            {
                return this.GetRepository<Product>();
            }
        }

        public IRepository<ProductImage> ProductImages
        {
            get
            {
                return this.GetRepository<ProductImage>();
            }
        }

        public IRepository<Quantity> Quantities
        {
            get
            {
                return this.GetRepository<Quantity>();
            }
        }

        public IRepository<Shipment> Shipments
        {
            get
            {
                return this.GetRepository<Shipment>();
            }
        }

        public IRepository<ShipmentProduct> ShipmentProducts
        {
            get
            {
                return this.GetRepository<ShipmentProduct>();
            }
        } 

        public IRepository<Size> Sizes
        {
            get
            {
                return this.GetRepository<Size>();
            }
        }

        public IRepository<ConfigFile> ConfigFiles
        {
            get
            {
                return this.GetRepository<ConfigFile>();
            }
        }

        public IRepository<City> Cities
        {
            get
            {
                return this.GetRepository<City>();
            }
        }

        public IRepository<Country> Countries
        {
            get
            {
                return this.GetRepository<Country>();
            }
        }

        public IRepository<Neighbourhood> Neighbourhoods
        {
            get
            {
                return this.GetRepository<Neighbourhood>();
            }
        }

        public IRepository<Office> Offices
        {
            get
            {
                return this.GetRepository<Office>();
            }
        }

        public IRepository<Street> Streets
        {
            get
            {
                return this.GetRepository<Street>();
            }
        }

        public IRepository<Zone> Zones
        {
            get
            {
                return this.GetRepository<Zone>();
            }
        }

        public IRepository<CarouselImage> CarouselImages
        {
            get
            {
                return this.GetRepository<CarouselImage>();
            }
        }
        public IRepository<MiddlePageImage> MostPopularBrandsImages
        {
            get
            {
                return this.GetRepository<MiddlePageImage>();
            }
        }

        public IRepository<BottomPageImage> MostPopularCollectionsImages
        {
            get
            {
                return this.GetRepository<BottomPageImage>();
            }
        }

        public IRepository<SeenProducts> SeenProducts
        {
            get
            {
                return this.GetRepository<SeenProducts>();
            }
        }

        public int SaveChanges()
        {
            return this.context.SaveChanges();
        }

        public IRepository<T> GetRepository<T>() where T : class
        {
            var type = typeof(T);

            if (!this.repositories.ContainsKey(type))
            {
                var typeOfRepository = typeof(GenericRepository<T>);

                var repository = Activator.CreateInstance(typeOfRepository, this.context);

                this.repositories.Add(type, repository);
            }

            return (IRepository<T>)this.repositories[type];
        } 
    }
}
