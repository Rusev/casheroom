﻿namespace CacheRoom.Data
{
    using System.Data.Entity;

    using CacheRoom.Data.Contracts;
    using CacheRoom.Data.Migrations;
    using CacheRoom.Models;
    using CacheRoom.Models.Config;
    using CacheRoom.Models.Econt;

    using Microsoft.AspNet.Identity.EntityFramework;

    public class CacheRoomDbContext : IdentityDbContext<User>, ICacheRoomDbContext
    {
        public CacheRoomDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<CacheRoomDbContext, Configuration>());
        }

        public static CacheRoomDbContext Create()
        {
            return new CacheRoomDbContext();
        }

        public IDbSet<Brand> Brands { get; set; }

        public IDbSet<CartItem> CartItems { get; set; }


        public IDbSet<Category> Categories { get; set; }


        public IDbSet<Collection> Collections { get; set; }

        public IDbSet<Product> Products { get; set; }


        public IDbSet<ProductImage> ProductImages { get; set; }


        public IDbSet<Quantity> Quantities { get; set; }

        public IDbSet<Shipment> Shipments { get; set; }

        public IDbSet<ShipmentProduct> ShipmentProducts { get; set; }

        public IDbSet<Size> Sizes { get; set; }

        public IDbSet<ConfigFile> ConfigFiles { get; set; }

        public IDbSet<City> Cities { get; set; }

        public IDbSet<Country> Countries { get; set; }

        public IDbSet<Neighbourhood> Neighbourhoods { get; set; }

        public IDbSet<Office> Offices { get; set; }

        public IDbSet<Street> Streets { get; set; }

        public IDbSet<Zone> Zones { get; set; }

        public IDbSet<CarouselImage> CarouselImages { get; set; }

        public IDbSet<MiddlePageImage> MostPopularBrandsImages { get; set; }

        public IDbSet<BottomPageImage> MostPopularCollectionsImages { get; set; }

        public IDbSet<SeenProducts> SeenProducts { get; set; }
    }
}
