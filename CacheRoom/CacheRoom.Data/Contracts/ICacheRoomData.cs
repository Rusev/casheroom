﻿namespace CacheRoom.Data.Contracts
{
    using System.Data.Entity;

    using CacheRoom.Models;
    using CacheRoom.Models.Config;
    using CacheRoom.Models.Econt;

    public interface ICacheRoomData
    {
        IRepository<User> Users { get; }
        IRepository<Brand> Brands { get; }
        IRepository<CartItem> CartItems { get; }
        IRepository<Category> Categories { get; }
        IRepository<Collection> Collections { get; }
        IRepository<Product> Products { get; }
        IRepository<ProductImage> ProductImages { get; }
        IRepository<Quantity> Quantities { get; }
        IRepository<Shipment> Shipments { get; }
        IRepository<ShipmentProduct> ShipmentProducts { get; } 
        IRepository<Size> Sizes { get; }
        IRepository<City> Cities { get; }
        IRepository<Country> Countries { get;}
        IRepository<Neighbourhood> Neighbourhoods { get; }
        IRepository<Office> Offices { get; }
        IRepository<Street> Streets { get; }
        IRepository<Zone> Zones { get; } 
        IRepository<ConfigFile> ConfigFiles { get; }

        IRepository<CarouselImage> CarouselImages { get; } 
        IRepository<MiddlePageImage> MostPopularBrandsImages { get; }
        IRepository<BottomPageImage> MostPopularCollectionsImages { get; }

        IRepository<SeenProducts> SeenProducts { get;} 
        int SaveChanges();
    }
}
