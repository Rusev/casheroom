﻿namespace CacheRoom.Data.Contracts
{
    using System.Data.Entity;
    using CacheRoom.Models;
    using CacheRoom.Models.Econt;
    using CacheRoom.Models.Config;

    public interface ICacheRoomDbContext
    {
        IDbSet<User> Users { get; set; }
        IDbSet<Brand> Brands { get; set; }
        IDbSet<CartItem> CartItems { get; set; }
        IDbSet<Category> Categories { get; set; }
        IDbSet<Collection> Collections { get; set; }
        IDbSet<Product> Products { get; set; }
        IDbSet<ProductImage> ProductImages { get; set; }
        IDbSet<Quantity> Quantities { get; set; }
        IDbSet<Shipment> Shipments { get; set; }
        IDbSet<ShipmentProduct> ShipmentProducts { get; set; }
        IDbSet<Size> Sizes { get; set; }
        IDbSet<ConfigFile> ConfigFiles { get; set; }
        IDbSet<City> Cities { get; set; }
        IDbSet<Country> Countries { get; set; }
        IDbSet<Neighbourhood> Neighbourhoods { get; set; }
        IDbSet<Office> Offices { get; set; }
        IDbSet<Street> Streets { get; set; }
        IDbSet<Zone> Zones { get; set; }

        IDbSet<CarouselImage> CarouselImages { get; set; }
        IDbSet<MiddlePageImage> MostPopularBrandsImages { get; set; }
        IDbSet<BottomPageImage> MostPopularCollectionsImages { get; set; }

        IDbSet<SeenProducts> SeenProducts { get; set; }

    }
}
