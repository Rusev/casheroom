﻿namespace CacheRoom.Data.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;

    using CacheRoom.Models;
    using CacheRoom.Data;
    using CacheRoom.Models.Config;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    internal sealed class Configuration : DbMigrationsConfiguration<CacheRoomDbContext>
    {
        private PasswordHasher passwordHasher = new PasswordHasher();

        private readonly bool _pendingMigrations;

        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(CacheRoomDbContext context)
        {
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);
            var userStore = new UserStore<User>(context);
            var userManager = new UserManager<User>(userStore);

            // Seed roles
            var adminRole = CreateRole(context, roleManager, "AppAdmin");

            var admin = CreateUser(context, userManager, "ilko.dinev@cacheroom.com", "C123456789r1", "0882507577", "Илко Динев");
            userManager.AddToRole(admin.Id, adminRole.Name);

        }

        private IdentityRole CreateRole(
            CacheRoomDbContext context,
            RoleManager<IdentityRole> roleManager,
            string roleName)
        {
            if (context.Roles.Any(r => r.Name == roleName))
            {
                return context.Roles.First(r => r.Name == roleName);
            }
            else
            {
                var role = new IdentityRole { Name = roleName };

                roleManager.Create(role);

                return role;
            }
        }

        private User CreateUser(
            CacheRoomDbContext context,
            UserManager<User> userManager,
            string email,
            string password,
            string phoneNumber,
            string nameAndFamilyName)
        {
            if (context.Users.Any(u => u.UserName == email))
            {
                return context.Users.First(u => u.UserName == email);
            }
            else
            {
                var user = new User
                               {
                                   UserName = email,
                                   Email = email,
                                   PhoneNumber = phoneNumber,
                                   NameAndFamilyName = nameAndFamilyName
                               };

                userManager.Create(user, password);

                return user;
            }
        }

    }
}
