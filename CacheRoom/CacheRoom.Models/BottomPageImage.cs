﻿namespace CacheRoom.Models
{
    public class BottomPageImage
    {
        public int Id { get; set; }

        public string ImagePath { get; set; }

        public CollectionImagePosition Position { get; set; }

        public string ActionLink { get; set; }

        public bool IsVisible { get; set; }

    }
}
