﻿namespace CacheRoom.Models
{
    using System.ComponentModel.DataAnnotations;

    public enum BrandImagePosition
    {
        [Display(Name = "без позиция")]
        none,
        [Display(Name = "ляво-горе")]
        leftTop,
        [Display(Name = "ляво-долу")]
        leftBottom,
        [Display(Name = "дясно-горе")]
        rightTop,
        [Display(Name = "дясно-среда")]
        rightMiddle,
        [Display(Name = "дясно-долу")]
        rightBottom
    }
}