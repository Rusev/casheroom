﻿namespace CacheRoom.Models
{
    public class CarouselImage
    {
        public int Id { get; set; }

        public string ImagePath { get; set; }

        public string ActionLink { get; set; }

    }
}
