﻿namespace CacheRoom.Models
{
    using System.ComponentModel.DataAnnotations;

    public enum CollectionImagePosition
    {
        [Display(Name = "без позиция")]
        none,
        [Display(Name = "първа")]
        first,
        [Display(Name = "втора")]
        second,
        [Display(Name = "трета")]
        third
    }
}
