﻿namespace CacheRoom.Models.Config
{
    using System.ComponentModel.DataAnnotations;

    public class ConfigFile
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Value { get; set; }
    }
}
