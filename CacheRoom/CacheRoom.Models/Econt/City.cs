﻿namespace CacheRoom.Models.Econt
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class City
    {
        public City()
        {
            this.Offices = new HashSet<Office>();
            this.Neighbourhoods = new HashSet<Neighbourhood>();
            this.Streets = new HashSet<Street>();
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public int PostCode { get; set; }

        public string Type { get; set; }

        public int ZoneId { get; set; }

        public virtual Zone Zone { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string NameEn { get; set; }

        //public int CountryId { get; set; }

        //[ForeignKey("CountryId")]
        //public virtual Country Country { get; set; }
        public string RegionName { get; set; }

        public string RegionNameEn { get; set; }

        public int MainOfficeId { get; set; }

        public int ServiceDayId { get; set; }

        public string UpdatedTime { get; set; }

        public string FromDoor { get; set; }

        public string ToDoor { get; set; }

        public string FromOffice { get; set; }

        public string ToOffice { get; set; }

        public virtual ICollection<Office> Offices { get; set; }

        public virtual ICollection<Neighbourhood> Neighbourhoods { get; set; }

        public virtual ICollection<Street> Streets { get; set; }


    }
}
