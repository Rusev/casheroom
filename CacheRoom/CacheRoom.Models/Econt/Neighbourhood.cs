﻿namespace CacheRoom.Models.Econt
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Collections.Generic;

    [Table("Neighbourhoods", Schema = "dbo")]
    public class Neighbourhood
    {
        public Neighbourhood()
        {
            this.Offices = new HashSet<Office>();
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public string Name { get; set; }

        public string NameEn { get; set; }

        public int CityId { get; set; }


        [ForeignKey("CityId")]
        public virtual City City { get; set; }

        public int CityPostCode { get; set; }

        public string UpdatedTime { get; set; }

        public virtual ICollection<Office> Offices { get; set; }
    }
}