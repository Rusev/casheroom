﻿namespace CacheRoom.Models.Econt
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Offices", Schema = "dbo")]
    public class Office
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string NameEn { get; set; }

        public string OfficeCode { get; set; }

        public int IsMachine { get; set; }

        public int? CityId { get; set; }


        [ForeignKey("CityId")]
        public virtual City City { get; set; }

        public string Lattitude { get; set; }

        public string Longitude { get; set; }


        public int? NeighbourhoodId { get; set; }

        [ForeignKey("NeighbourhoodId")]
        public virtual Neighbourhood Neighbourhood { get; set; }

        public int? StreetId { get; set; }

        [ForeignKey("StreetId")]
        public virtual Street Street { get; set; }

        public string StreetNumber { get; set; }

        public string Entrance { get; set; }

        public int Floor { get; set; }

        public int Appartment { get; set; }

        public string Other { get; set; }

        public string Phone { get; set; }

        public string WorkBegin { get; set; }

        public string WorkEnd { get; set; }

        public string WorkBeginSaturday { get; set; }

        public string WorkEndSaturday { get; set; }

        public string TimePriority { get; set; }

        public string UpdatedTime { get; set; }
    }
}