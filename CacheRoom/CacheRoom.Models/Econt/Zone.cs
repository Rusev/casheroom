﻿namespace CacheRoom.Models.Econt
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Zones", Schema = "dbo")]
    public class Zone
    {
        public Zone()
        {
            this.Cities = new HashSet<City>();
            this.Countries = new HashSet<Country>();
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string NameEn { get; set; }

        public int IsNational { get; set; }

        public int IsEcontServed { get; set; }

        public string UpdatedTime { get; set; }

        public virtual ICollection<City> Cities { get; set; }

        public virtual ICollection<Country> Countries { get; set; }
    }
}