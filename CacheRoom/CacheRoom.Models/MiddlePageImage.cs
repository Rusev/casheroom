﻿namespace CacheRoom.Models
{
    public class MiddlePageImage
    {
        public int Id { get; set; }

        public string ImagePath { get; set; }

        public BrandImagePosition Position { get; set; }

        public string ActionLink { get; set; }

        public bool IsVisible { get; set; }

    }
}
