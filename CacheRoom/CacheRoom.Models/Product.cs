﻿namespace CacheRoom.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    public class Product
    {
        public Product()
        {
            IsOnSale = false;
            this.ProductImages = new HashSet<ProductImage>();
            this.Quantities = new HashSet<Quantity>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Наименование")]
        public string Name { get; set; }

        public virtual Category Category { get; set; }

        [Display(Name = "Категория")]
        public int CategoryId { get; set; }

        [Display(Name = "Колекция")]
        public int? CollectionId { get; set; }

        public virtual Collection Collection { get; set; }

        public int? ProductImageId { get; set; }

        public virtual ICollection<ProductImage> ProductImages { get; set; }

        public virtual Brand Brand { get; set; }

        [Display(Name = "Марка")]
        public int BrandId { get; set; }

        [Required]
        [Display(Name="Тип")]
        public ProductType Type { get; set; }

        [Required]
        [Display(Name="На разпродажба")]
        public Boolean IsOnSale { get; set; }

        [Required]
        [Display(Name = "Цена")]
        public decimal Price { get; set; }

        [Display(Name = "Стара цена")]
        public decimal OldPrice { get; set; }

        [Display(Name = "Количества")]
        public virtual ICollection<Quantity> Quantities { get; set; }

        [Display(Name = "Описание")]
        public string Description { get; set; }

        public DateTime DateTimeCreated { get; set; }

    }
}
