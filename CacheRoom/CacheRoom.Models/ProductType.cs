﻿namespace CacheRoom.Models
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public enum ProductType
    {
        [Display(Name = "Мъжко")]
        Мъжко,
        [Display(Name = "Дамско")]
        Дамско,
        [Display(Name = "Детско")]
        Детско
    }
}
