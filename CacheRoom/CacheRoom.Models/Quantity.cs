﻿namespace CacheRoom.Models
{
    using System.ComponentModel.DataAnnotations;

    public class Quantity
    {
        [Key]
        public int Id { get; set; }

        public virtual Product Product { get; set; }

        public int ProductId { get; set; }

        public virtual Size Size { get; set; }

        public int? SizeId { get; set; }

        public int? Amount { get; set; }
    }
}
