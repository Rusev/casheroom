﻿namespace CacheRoom.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class SeenProducts
    {
        [Key]
        public int Id { get; set; }

        public string UserKey { get; set; }

        public int? ProductKey { get; set; }

        public DateTime? TimeStamp { get; set; }
    }
}
