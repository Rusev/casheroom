﻿namespace CacheRoom.Models
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    using EcontServiceRequestBuilders.RequestModels;

    public class Shipment
    {
        public Shipment()
        {
            this.ShipmentProducts = new HashSet<ShipmentProduct>();
        }

        [Key]
        public int Id { get; set; }

        public int UserId { get; set; }

        public virtual User User { get; set; }

        public string ReceiverInformation { get; set; }

        public string ReceiverChosenShipmentType { get; set; }

        public string SenderInformation { get; set; }

        public string PaymentInformation { get; set; }

        public string ShipmentInformation { get; set; }

        public string EcontClientInformation { get; set; }

        public string SystemInformation { get; set; }
        
        public bool IsSended { get; set; }

        public bool IsCanceled { get; set; }

        public DateTime DateCreated { get; set; }

        public string ShipmentNumber { get; set; }

        public string ShipmentIdentityNumber { get; set; }

        public bool PayAfterCheck { get; set; }

        public bool PayAfterTest { get; set; }

        public bool PayAfterTestAndChoose { get; set; }

        public PriorityType PriorityType { get; set; }

        public TimeSpan? PriorityHour { get; set; }

        public string SmartCourierType { get; set; }

        public virtual DateTime? DeliveryDate { get; set; }

        public virtual ICollection<ShipmentProduct> ShipmentProducts { get; set; }

    }
}
