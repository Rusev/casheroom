﻿namespace CacheRoom.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Size
    {
        public Size()
        {
            this.Quantities = new HashSet<Quantity>();
        }
        [Key]
        public int Id { get; set; }

        [Required]
        public string Label { get; set; }

        [Required]
        public SizeType SizeType { get; set; }

        public virtual ICollection<Quantity> Quantities { get; set; }
    }
}
