﻿namespace CacheRoom.Models
{
    using System.ComponentModel.DataAnnotations;

    public enum SizeType
    {
        [Display(Name="Число")]
        Number,
        [Display(Name = "Буква")]
        Letter,
        [Display(Name = "Друго")]
        None
    }
}
