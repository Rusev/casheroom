﻿using System.Web;
using System.Web.Optimization;

namespace CacheRoom.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery-ui.js"));

            bundles.Add(new ScriptBundle("~/bundles/ajax").Include(
                "~/Scripts/jquery.unobtrusive-ajax.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/signalR").Include(
                "~/Scripts/jquery.signalR-2.2.0.js",
                "~/SignalR/hubs"));

            bundles.Add(new ScriptBundle("~/bundles/signalrScripts").Include(
                "~/Scripts/signalr/updateShipmentsCount.js"));

            bundles.Add(new ScriptBundle("~/bundles/googleMaps").Include(
                "~/Scripts/googleMaps/initializeMap.js"));

            bundles.Add(new ScriptBundle("~/bundles/grid").Include(
                "~/Scripts/gridmvc.js"));


            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                    "~/Scripts/app/imageZoomer.js",
                    "~/Scripts/app/changeImage.js",
                    "~/Scripts/app/chooseSize.js",
                    "~/Scripts/app/changeQuantity.js",
                    "~/Scripts/app/autoCompleteInputData.js",
                    "~/Scripts/app/shipmentTypeSelector.js",
                    "~/Scripts/app/setBrandImageIndexPosition.js",
                    "~/Scripts/app/setCollectionImageIndexPosition.js",
                    "~/Scripts/app/setBrandImageIndexLink.js",
                    "~/Scripts/app/setCollectionImageIndexLink.js",
                    "~/Scripts/app/smartCourier.js"));

            bundles.Add(new ScriptBundle("~/bundles/datetimepicker").Include(
                "~/Scripts/app/dateTimePicker.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/Gridmvc.css",
                      "~/Content/app/app.css",
                      "~/Content/app/admin.css",
                      "~/Content/app/appFooter.css",
                      "~/Content/app/appHeader.css",
                      "~/Content/app/appView.css",
                      "~/Content/app/productInfo.css",
                      "~/Content/app/cart.css",
                      "~/Content/app/scrollButton.css",
                      "~/Content/app/home.css",
                      "~/Content/app/autoComplete.css",
                      "~/Content/thumbnail-gallery.css",
                      "~/Content/app/bootstrapClearFix.css",
                      "~/Content/app/productGridImage.css",
                      "~/Content/app/scrollButton.css",
                      "~/Content/app/accordion.css",
                      "~/Content/app/searachInput.css"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap-datetimePicker").Include(
                "~/Scripts/moment.js",
                "~/Scripts/bootstrap-datetimepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/accordion").Include("~/Scripts/app/accordion.js"));

            bundles.Add(new ScriptBundle("~/bundles/editProductQuantity").Include(
                "~/Scripts/app/editProductSizeAndQuantity.js",
                "~/Scripts/app/setOldPrice.js"));

            bundles.Add(new StyleBundle("~/Content/bootstrap-datetimepicker").Include(
                "~/Content/bootstrap-datetimepicker.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/jquery-responsive-table").Include(
                "~/Scripts/jquery-responsive-table.js",
                "~/Scripts/app/responsiveGridView.js"));

            bundles.Add(new ScriptBundle("~/bundles/dropzone").Include(
                "~/Scripts/dropzone/dropzone.js",
                "~/Scripts/dropzone/imageUploader.js"));

        }
    }
}
