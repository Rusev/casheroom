﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CacheRoom.Web
{
    using System.Security.Policy;
    using System.Xml.XPath;

    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "CacheRoom.Web.Controllers" });

            routes.MapRoute(
                name: "Filters",
                url: "{controller}/{action}",
                defaults:
                    new
                        {
                            controller = "Product",
                            action = "Index",
                            type = UrlParameter.Optional,
                            brand = UrlParameter.Optional,
                            category = UrlParameter.Optional
                        },
                namespaces: new[] { "CacheRoom.Web.Areas.Store.Controllers" });

        }
    }
}
