﻿using System.Web.Mvc;

namespace CacheRoom.Web.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "DeletePhoto",
                "Admin/{controller}/{action}/{id}/{photoId}",
                new { action = "Index", id = UrlParameter.Optional },
                new[] { "CacheRoom.Web.Areas.Admin.Controllers" }
            );

            context.MapRoute(
               "Admin_default",
               "Admin/{controller}/{action}/{id}",
               new { action = "Index", id = UrlParameter.Optional },
               new[] { "CacheRoom.Web.Areas.Admin.Controllers" }
           );
        }
    }
}