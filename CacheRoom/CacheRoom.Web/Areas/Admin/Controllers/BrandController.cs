﻿namespace CacheRoom.Web.Areas.Admin.Controllers
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Web.Mvc;

    using CacheRoom.Data.Contracts;
    using CacheRoom.Models;
    using CacheRoom.Web.Areas.Admin.Models.Store;
    using CacheRoom.Web.Controllers;
    using CacheRoom.Web.Extensions;

    using PagedList;

    [Authorize(Roles = "AppAdmin")]
    public class BrandController : BaseController
    {
        public BrandController(ICacheRoomData data)
            : base(data)
        {
        }

        public ActionResult Index(int page = 1, int pageSize = 12)
        {
            return View(this.Data.Brands.All().OrderByDescending(brand => brand.Name)
                .Select(BrandViewModel.ViewModel)
                .ToPagedList(page, pageSize));
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BrandViewModel brandData)
        {
            var brands = this.Data.Brands.All().Select(b => b.Name);

            if (ModelState.IsValid)
            {
                if (brands.Contains(brandData.Name))
                {
                    this.AddNotification("Вече съществува марка с такова име. Моля задайте ново уникално име.", NotificationType.WARNING);

                    return this.View();
                }
                var brand = new Brand();

                if (!String.IsNullOrWhiteSpace(brandData.Name))
                {
                    brand.Name = brandData.Name.Trim();
                }

                if (!String.IsNullOrWhiteSpace(brandData.Description))
                {
                    brand.Description = brandData.Description.Trim();
                }
                

                this.Data.Brands.Add(brand);
                this.Data.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(brandData);
        }

        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var brand = this.Data.Brands.All().Where(b => b.Id == id)
                .Select(BrandViewModel.ViewModel).FirstOrDefault();

            if (brand == null)
            {
                return this.HttpNotFound();
            }
            return this.View(brand);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, BrandViewModel brandData)
        {
            if (ModelState.IsValid)
            {
                var brand = this.Data.Brands.Find(id);
                brand.Name = brandData.Name;
                brand.Description = brandData.Description;

                this.Data.Brands.Update(brand);
                this.Data.SaveChanges();
                return this.RedirectToAction("Index");
            }
            return this.View(brandData);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete([Bind(Include = "Id")] Brand brand)
        {
            this.Data.Brands.Delete(brand);
            this.Data.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}