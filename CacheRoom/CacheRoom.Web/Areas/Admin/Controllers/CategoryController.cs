﻿namespace CacheRoom.Web.Areas.Admin.Controllers
{
    using System.Linq;
    using System.Net;
    using System.Web.Mvc;

    using CacheRoom.Data.Contracts;
    using CacheRoom.Models;
    using CacheRoom.Web.Areas.Admin.Models.Store;
    using CacheRoom.Web.Controllers;
    using CacheRoom.Web.Extensions;

    using PagedList;

    [Authorize(Roles = "AppAdmin")]
    public class CategoryController : BaseController
    {
        public CategoryController(ICacheRoomData data)
            : base(data)
        {
        }

        public ActionResult Index(int page = 1, int pageSize = 12)
        {
            return View(this.Data.Categories.All().OrderByDescending(category => category.Name)
                .Select(CategoryViewModel.ViewModel)
                .ToPagedList(page, pageSize));
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = this.Data.Categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CategoryViewModel categoryData)
        {
            var categories = this.Data.Categories.All().Select(c => c.Name);
            if (ModelState.IsValid)
            {
                if (categories.Contains(categoryData.Name))
                {
                    this.AddNotification("Вече съществува категория с такова име. Моля задайте ново уникално име.", NotificationType.WARNING);

                    return this.View();
                }

                var category = new Category();
                category.Name = categoryData.Name;
                category.Description = categoryData.Description;
                category.SizeType = categoryData.SizeType;

                this.Data.Categories.Add(category);
                this.Data.SaveChanges();

                return RedirectToAction("Index");
            }

            return View(categoryData);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var category = this.Data.Categories.All().Where(c => c.Id == id).Select(CategoryViewModel.ViewModel).FirstOrDefault();
            if (category == null)
            {
                return HttpNotFound();
            }
            return this.View(category);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id,CategoryViewModel categoryData)
        {
            if (ModelState.IsValid)
            {
                var category = this.Data.Categories.Find(id);
                category.Name = categoryData.Name;
                category.Description = categoryData.Description;
                category.SizeType = categoryData.SizeType;

                this.Data.Categories.Update(category);
                this.Data.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(categoryData);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete([Bind(Include = "Id")] Category category)
        {
            this.Data.Categories.Delete(category);
            this.Data.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}