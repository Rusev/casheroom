﻿namespace CacheRoom.Web.Areas.Admin.Controllers
{
    using System.Linq;
    using System.Net;
    using System.Web.Mvc;

    using CacheRoom.Data.Contracts;
    using CacheRoom.Models;
    using CacheRoom.Web.Areas.Admin.Models.Store;
    using CacheRoom.Web.Controllers;
    using CacheRoom.Web.Extensions;

    using PagedList;
    public class CollectionController : BaseController
    {
        public CollectionController(ICacheRoomData data)
            : base(data)
        {
        }

        public ActionResult Index(int page = 1, int pageSize = 12)
        {
            return
                this.View(
                    this.Data.Collections.All()
                        .OrderByDescending(collection => collection.Name)
                        .Select(CollectionViewModel.ViewModel)
                        .ToPagedList(page,pageSize));

        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var collection = this.Data.Collections.Find(id);

            if (collection == null)
            {
                return this.HttpNotFound();
            }

            return this.View(collection);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CollectionViewModel collectionData)
        {
            var collections = this.Data.Collections.All().Select(c => c.Name);

            if (ModelState.IsValid)
            {
                if (collections.Contains(collectionData.Name))
                {
                    this.AddNotification("Вече съществува колекция с такова име. Моля задайте ново уникално име.", NotificationType.WARNING);

                    return this.View();
                }
                var collection = new Collection();
                collection.Name = collectionData.Name;
                collection.Description = collectionData.Description;

                this.Data.Collections.Add(collection);
                this.Data.SaveChanges();
                return RedirectToAction("Index");
            }

            return this.View(collectionData);
        }

        public ActionResult Edit(int? id, CollectionViewModel collectionData)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var collection =
                this.Data.Collections.All()
                    .Where(c => c.Id == id)
                    .Select(CollectionViewModel.ViewModel)
                    .FirstOrDefault();

            if (collection == null)
            {
                return this.HttpNotFound();
            }
            return this.View(collection);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id,CollectionViewModel collectionData)
        {
            if (ModelState.IsValid)
            {
                var collection = this.Data.Collections.Find(id);
                collection.Name = collectionData.Name;
                collection.Description = collectionData.Description;

                this.Data.Collections.Update(collection);
                this.Data.SaveChanges();

                return this.RedirectToAction("Index");
            }

            return this.View(collectionData);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete([Bind(Include = "Id")] Collection collection)
        {
            this.Data.Collections.Delete(collection);
            this.Data.SaveChanges();
            return this.RedirectToAction("Index");
        }
    }
}