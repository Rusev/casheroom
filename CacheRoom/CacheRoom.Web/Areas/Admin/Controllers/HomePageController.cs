﻿namespace CacheRoom.Web.Areas.Admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Web;
    using System.Web.Mvc;

    using CacheRoom.Data.Contracts;
    using CacheRoom.Models;
    using CacheRoom.Web.Controllers;
    using CacheRoom.Web.Extensions;
    using CacheRoom.Web.Models;
    
    using CacheRoom.Web.Areas.Admin.Models.HomePage;
    using CacheRoom.Web.Areas.Admin.Models.Store;

    using BrandViewModel = CacheRoom.Web.Areas.Admin.Models.HomePage.BrandViewModel;

    public class HomePageController : BaseController
    {
        public HomePageController(ICacheRoomData data)
            : base(data)
        {
        }

        public ActionResult Index()
        {
            var carouselImages = this.Data.CarouselImages.All().Select(CarouselImageViewModel.ViewModel).ToList();

            var brandsImages = this.Data.MostPopularBrandsImages.All().Select(BrandImagesViewModel.ViewModel).ToList();

            var collectionsImages =
                this.Data.MostPopularCollectionsImages.All().Select(CollectionImagesViewModel.ViewModel).ToList();

            this.ViewBag.BrandId = this.SetBrandSelectList();
            this.ViewBag.CollectionId = this.SetCollectionSelectList();

            var homePage = new HomePageImagesViewModel()
                               {
                                   BrandsImages = brandsImages,
                                   CollectionImages = collectionsImages,
                                   CarouselImages = carouselImages,
                               };

            return this.View(homePage);
        }

        [HttpGet]
        public ActionResult AddPhotoCarousel()
        {
            var carouselImages = this.Data.CarouselImages.All().Select(CarouselImageViewModel.ViewModel).Take(5);

            return this.View(carouselImages);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddPhotoCarousel(HttpPostedFileBase file)
        {

            var basePath = "~/Images/Index/Carousel";

            if (file != null && file.ContentLength > 0)
                try
                {

                    bool basePathExist = Directory.Exists(Server.MapPath(basePath));
                    if (!basePathExist)
                    {
                        Directory.CreateDirectory(Server.MapPath(basePath));
                    }

                    string path = Path.Combine(Server.MapPath(basePath), Path.GetFileName(file.FileName));

                    file.SaveAs(path);

                    var imagePathInDatabase = basePath.Substring(1, basePath.Length - 1) + "/" + file.FileName;

                    var image = new CarouselImage() { ImagePath = imagePathInDatabase };

                    this.Data.CarouselImages.Add(image);
                    this.Data.SaveChanges();

                    ViewBag.Message = "File uploaded successfully";
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            else
            {
                ViewBag.Message = "You have not specified a file.";
            }

            return RedirectToAction("AddPhotoCarousel", "HomePage");
        }

        public ActionResult DeletePhoto(string type, int photoId)
        {

            if (type.Equals("Carousel"))
            {
                var image = this.Data.CarouselImages.All().FirstOrDefault(c => c.Id == photoId);
                this.Data.CarouselImages.Delete(image);
                this.Data.SaveChanges();
                this.DeletePhysicalFiles(image.ImagePath);

                return this.RedirectToAction("Index", "HomePage");
            }
            if (type.Equals("Brand"))
            {
                var image = this.Data.MostPopularBrandsImages.All().FirstOrDefault(c => c.Id == photoId);
                this.Data.MostPopularBrandsImages.Delete(image);
                this.Data.SaveChanges();
                this.DeletePhysicalFiles(image.ImagePath);

                return this.RedirectToAction("Index", "HomePage");
            }
            if (type.Equals("Collection"))
            {
                var image = this.Data.MostPopularCollectionsImages.All().FirstOrDefault(c => c.Id == photoId);
                this.Data.MostPopularCollectionsImages.Delete(image);
                this.Data.SaveChanges();
                this.DeletePhysicalFiles(image.ImagePath);

                return this.RedirectToAction("Index", "HomePage");
            }
            this.AddNotification(
                "Възникна проблем при изтриването на снимката! Моля опитайте пак",
                NotificationType.ERROR);

            return this.RedirectToAction("AddPhotoCarousel", "HomePage");
        }

        [HttpGet]
        public ActionResult AddPhotoBrand()
        {

            var brandsImages = this.Data.MostPopularBrandsImages.All().Select(BrandImagesViewModel.ViewModel);

            return this.View(brandsImages);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddPhotoBrand(HttpPostedFileBase file)
        {

            var basePath = "~/Images/Index/Brands";

            if (file != null && file.ContentLength > 0)
                try
                {

                    bool basePathExist = Directory.Exists(Server.MapPath(basePath));
                    if (!basePathExist)
                    {
                        Directory.CreateDirectory(Server.MapPath(basePath));
                    }

                    string path = Path.Combine(Server.MapPath(basePath), Path.GetFileName(file.FileName));

                    Image img = new Bitmap(file.InputStream);

                    var convertedImage = ResizeImage(img, 675, 330);

                    convertedImage.Save(path);

                    var imagePathInDatabase = basePath.Substring(1, basePath.Length - 1) + "/" + file.FileName;

                    var image = new MiddlePageImage() { ImagePath = imagePathInDatabase };

                    this.Data.MostPopularBrandsImages.Add(image);
                    this.Data.SaveChanges();

                    ViewBag.Message = "File uploaded successfully";
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            else
            {
                ViewBag.Message = "You have not specified a file.";
            }

            return RedirectToAction("AddPhotoBrand", "HomePage");
        }

        [HttpPost]
        public JsonResult SetBrandImagePosition(SetBrandImagePositionViewModel brandImageData)
        {

            if (ModelState.IsValid)
            {
                var image = this.Data.MostPopularBrandsImages.All().FirstOrDefault(i => i.Id == brandImageData.ImageId);
                var position = brandImageData.Position;

                image.Position = ((BrandImagePosition)position);
                var allBrandsImages =
                    this.Data.MostPopularBrandsImages.All()
                        .Where(i => i.Position.ToString().Equals(image.Position.ToString()));

                foreach (var b in allBrandsImages)
                {
                    b.IsVisible = false;
                    this.Data.MostPopularBrandsImages.Update(b);
                }
                if (image.Position.ToString().Equals(BrandImagePosition.rightTop)
                    || image.Position.ToString().Equals(BrandImagePosition.rightMiddle)
                    || image.Position.ToString().Equals(BrandImagePosition.rightBottom))
                {

                    Bitmap img = new Bitmap(image.ImagePath);
                    ResizeImage(img, 600, 270);
                }
                image.IsVisible = true;
                
                this.Data.MostPopularBrandsImages.Update(image);
                this.Data.SaveChanges();

                return Json(new { IsSetPosition = true }, JsonRequestBehavior.AllowGet);
            }
            return Json(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        public JsonResult SetBrandImageLink(LinkViewModel model)
        {
            if (ModelState.IsValid)
            {
                var image = this.Data.MostPopularBrandsImages.All().FirstOrDefault(i => i.Id == model.ImageId);
                var brand = this.Data.Brands.All().FirstOrDefault(b => b.Id == model.LinkId);

                image.ActionLink = String.Format("/Store/Product?brand={0}",brand.Name);

                this.Data.MostPopularBrandsImages.Update(image);
                this.Data.SaveChanges();

                return Json(new {IsSetLink = true},JsonRequestBehavior.AllowGet);
            }
            return Json(new { IsSetLink = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddPhotoCollection()
        {
            var collectionImages =
                this.Data.MostPopularCollectionsImages.All().Select(CollectionImagesViewModel.ViewModel);

            return this.View(collectionImages);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddPhotoCollection(HttpPostedFileBase file)
        {
            var basePath = "~/Images/Index/Collections";

            if (file != null && file.ContentLength > 0)
                try
                {

                    bool basePathExist = Directory.Exists(Server.MapPath(basePath));
                    if (!basePathExist)
                    {
                        Directory.CreateDirectory(Server.MapPath(basePath));
                    }

                    string path = Path.Combine(Server.MapPath(basePath), Path.GetFileName(file.FileName));

                    Image img = new Bitmap(file.InputStream);

                    var convertedImage = ResizeImage(img, 470, 600);

                    convertedImage.Save(path);

                    var imagePathInDatabase = basePath.Substring(1, basePath.Length - 1) + "/" + file.FileName;

                    var image = new BottomPageImage() { ImagePath = imagePathInDatabase };

                    this.Data.MostPopularCollectionsImages.Add(image);
                    this.Data.SaveChanges();

                    ViewBag.Message = "File uploaded successfully";
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            else
            {
                ViewBag.Message = "You have not specified a file.";
            }

            return RedirectToAction("AddPhotoCollection", "HomePage");
        }

        [HttpPost]
        public JsonResult SetCollectionImagePosition(SetCollectionImagePositionViewModel collectionImageData)
        {

            if (ModelState.IsValid)
            {
                var image =
                    this.Data.MostPopularCollectionsImages.All()
                        .FirstOrDefault(i => i.Id == collectionImageData.ImageId);
                var position = collectionImageData.Position;

                image.Position = ((CollectionImagePosition)position);
                var allCollectionImages =
                    this.Data.MostPopularCollectionsImages.All()
                        .Where(i => i.Position.ToString().Equals(image.Position.ToString()));

                foreach (var b in allCollectionImages)
                {
                    b.IsVisible = false;
                    this.Data.MostPopularCollectionsImages.Update(b);
                }
                if (image.Position.ToString().Equals(CollectionImagePosition.first)
                    || image.Position.ToString().Equals(CollectionImagePosition.second)
                    || image.Position.ToString().Equals(CollectionImagePosition.third))
                {

                    Bitmap img = new Bitmap(image.ImagePath);
                    ResizeImage(img, 470, 600);
                }
                image.IsVisible = true;
                this.Data.MostPopularCollectionsImages.Update(image);
                this.Data.SaveChanges();

                return Json(new { IsSetPosition = true }, JsonRequestBehavior.AllowGet);
            }
            return Json(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        public JsonResult SetCollectionImageLink(LinkViewModel model)
        {
            if (ModelState.IsValid)
            {
                var image = this.Data.MostPopularCollectionsImages.All().FirstOrDefault(i => i.Id == model.ImageId);
                var collection = this.Data.Collections.All().FirstOrDefault(b => b.Id == model.LinkId);

                image.ActionLink = String.Format("/Store/Product?collection={0}", collection.Name);

                this.Data.MostPopularCollectionsImages.Update(image);
                this.Data.SaveChanges();

                return Json(new { IsSetLink = true }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { IsSetLink = false }, JsonRequestBehavior.AllowGet);
        }

        private SelectList SetCollectionSelectList()
        {
            var collectionEmpty = new CollectionListViewModel() { Id = null, Name = "Без колекция" };

            var collections = this.Data.Collections.All().Select(CollectionListViewModel.ViewModel);

            var collectionsList = new List<CollectionListViewModel>();

            collectionsList.Add(collectionEmpty);

            foreach (var c in collections)
            {
                collectionsList.Add(c);
            }

            var collectionSelectList = new SelectList(collectionsList, "Id", "Name");

            return collectionSelectList;
        }

        private SelectList SetBrandSelectList()
        {
            var brandEmpty = new BrandViewModel() { Id = null, Name = "Без марка" };

            var brands = this.Data.Brands.All().Select(BrandViewModel.ViewModel);

            var brandList = new List<BrandViewModel>();

            brandList.Add(brandEmpty);

            brandList.AddRange(brands);

            var brandSelectList = new SelectList(brandList, "Id", "Name");

            return brandSelectList;

        }
    }
}