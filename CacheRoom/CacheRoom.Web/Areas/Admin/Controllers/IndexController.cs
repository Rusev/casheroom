﻿namespace CacheRoom.Web.Areas.Admin.Controllers
{
    using System.Linq.Dynamic;
    using System.Web.Mvc;
    using System.Linq;
    using CacheRoom.Data.Contracts;
    using CacheRoom.Web.Controllers;

    [Authorize(Roles = "AppAdmin")]
    public class IndexController : BaseController
    {
        public IndexController(ICacheRoomData data)
            : base(data)
        {
        }

        public ActionResult Index()
        {
            var notSendedShipments = this.Data.Shipments.All().Count(s => s.IsSended == false);
            var sendedShipments = this.Data.Shipments.All().Count(s => s.IsSended);
            var canceledShipments = this.Data.Shipments.All().Count(s => s.IsCanceled);
            this.ViewBag.NotSended = notSendedShipments;
            this.ViewBag.Sended = sendedShipments;
            this.ViewBag.Canceled = canceledShipments;
            return this.View();
        }
    }
}