﻿namespace CacheRoom.Web.Areas.Admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.SqlTypes;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Web;
    using System.Web.Mvc;

    using CacheRoom.Data.Contracts;
    using CacheRoom.Models;
    using CacheRoom.Web.Controllers;

    using PagedList;
    using System.Drawing;
    using System.Text;
    using System.Threading;

    using CacheRoom.Data;
    using CacheRoom.Web.Areas.Admin.Models.Store;
    using CacheRoom.Web.Extensions;
    using CacheRoom.Web.Search;

    using EntityFramework.Extensions;

    public class ProductController : BaseController
    {
        public ProductController(ICacheRoomData data)
            : base(data)
        {
        }

        public ActionResult Index()
        {
            var products =
                this.Data.Products.All()
                    .OrderByDescending(product => product.Name)
                    .Include(p => p.Brand)
                    .Include(p => p.Category)
                    .Include(p => p.ProductImages)
                    .Select(ProductViewModel.ViewModel)
                    .ToList();

            if (products.Any())
            {
                foreach (var p in products)
                {
                    p.Amount = this.Data.Quantities.All().Where(q => q.ProductId == p.Id).Sum(q => q.Amount) ?? 0;
                }
            }
            
            return this.View(products);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var product = this.Data.Products.Find(id);

            if (product == null)
            {
                return this.HttpNotFound();
            }

            return this.View(product);
        }

        private SelectList SetCollectionSelectList()
        {
            var collectionEmpty = new CollectionListViewModel() { Id = null, Name = "Без колекция" };

            var collections = this.Data.Collections.All().Select(CollectionListViewModel.ViewModel);

            var collectionsList = new List<CollectionListViewModel>();

            collectionsList.Add(collectionEmpty);

            foreach (var c in collections)
            {
                collectionsList.Add(c);
            }

            var collectionSelectList = new SelectList(collectionsList, "Id", "Name");

            return collectionSelectList;
        }

        public ActionResult Create()
        {
            if (!this.Data.Sizes.All().Any())
            {
                this.AddNotification(
                    "Няма създадени размери. Моля създайте размери, към които да задавате количества, при създаване на нов продукт.",
                    NotificationType.WARNING);
                return this.RedirectToAction("Create", "Size");
            }

            ViewBag.BrandId = new SelectList(this.Data.Brands.All(), "Id", "Name");
            ViewBag.CategoryId = new SelectList(this.Data.Categories.All(), "Id", "Name");


            ViewBag.CollectionId = this.SetCollectionSelectList();

            return this.View();
        }

        private void SetInitialSizes(Product product)
        {
            var category = this.Data.Categories.All().FirstOrDefault(c => c.Id == product.CategoryId);

            var categorySizes = this.Data.Sizes.All().Where(s => s.SizeType == category.SizeType).ToList();

            foreach (var size in categorySizes)
            {
                var quantity = new Quantity();

                quantity.SizeId = size.Id;
                quantity.ProductId = product.Id;
                quantity.Amount = 0;

                this.Data.Quantities.Add(quantity);
                this.Data.SaveChanges();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProductViewModel productData)
        {
            var products = this.Data.Products.All().Select(p => p.Name.Trim());

            if (ModelState.IsValid)
            {
                if (products.Contains(productData.Name.Trim()))
                {
                    this.AddNotification(
                        "Вече съществува продукт с такова име. Моля задайте ново уникално име.",
                        NotificationType.WARNING);
                    ViewBag.BrandId = new SelectList(this.Data.Brands.All(), "Id", "Name");
                    ViewBag.CategoryId = new SelectList(this.Data.Categories.All(), "Id", "Name");
                    ViewBag.CollectionId = this.SetCollectionSelectList();

                    return this.RedirectToAction("Create");
                }
                var product = new Product();
                product.Name = productData.Name.Trim();
                product.CategoryId = productData.CategoryId;
                product.BrandId = productData.BrandId;
                product.Type = productData.Type;
                product.Price = productData.Price;
                product.Description = productData.Description;
                product.CollectionId = productData.CollectionId;
                product.IsOnSale = productData.IsOnSale;
                product.DateTimeCreated = DateTime.Now;

                this.Data.Products.Add(product);
                this.Data.SaveChanges();
                this.SetInitialSizes(product);

                this.UpdateLucene();
                return this.RedirectToAction("Add", "Quantity", new { id = product.Id });


            }

            ViewBag.BrandId = new SelectList(this.Data.Brands.All(), "Id", "Name", productData.BrandId);
            ViewBag.CategoryId = new SelectList(this.Data.Categories.All(), "Id", "Name", productData.CategoryId);

            return this.View(productData);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var product = this.Data.Products.Find(id);

            if (product == null)
            {
                return this.HttpNotFound();
            }

            ViewBag.BrandId = new SelectList(this.Data.Brands.All(), "Id", "Name", product.BrandId);
            ViewBag.CategoryID = new SelectList(this.Data.Categories.All(), "Id", "Name", product.CategoryId);

            return this.View(product);
        }

        private void ProcessDirectory(string startLocation)
        {
            foreach (var directory in Directory.GetDirectories(startLocation))
            {
                ProcessDirectory(directory);
                if (Directory.GetFiles(directory).Length == 0 && Directory.GetDirectories(directory).Length == 0)
                {
                    Directory.Delete(directory, false);
                }
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(
            [Bind(Include = "Id,Name,CategoryId,BrandId,Type,IsOnSale,Price,OldPrice,Description")] Product product)
        {
            if (ModelState.IsValid)
            {
                var productUpdate = this.Data.Products.All().FirstOrDefault(p => p.Id == product.Id);
                var oldBrand = productUpdate.Brand.Name.Trim();
                var oldCategory = productUpdate.Category.Name.Trim();
                var oldProductName = productUpdate.Name.Trim();
                var updatedPathBase = String.Empty;
                var imageOldDirectory = String.Empty;
                var newBrand = string.Empty;
                var newCategory = string.Empty;
                var newProductName = string.Empty;
                var backupDirectory = "~/Images/temp";

                if (productUpdate != null)
                {
                    productUpdate.Name = product.Name;
                    productUpdate.CategoryId = product.CategoryId;
                    productUpdate.BrandId = product.BrandId;
                    productUpdate.Type = product.Type;
                    productUpdate.IsOnSale = product.IsOnSale;
                    productUpdate.Price = product.Price;
                    productUpdate.OldPrice = product.OldPrice;
                    productUpdate.Description = product.Description;
                    this.Data.Products.Update(productUpdate);
                    this.Data.SaveChanges();

                    var productUpdateImages =
                        this.Data.ProductImages.All().Where(i => i.ProductId == productUpdate.Id).ToList();

                    if (productUpdateImages.Any())
                    {
                        newBrand = productUpdate.Brand.Name.Trim();
                        newCategory = productUpdate.Category.Name.Trim();
                        newProductName = productUpdate.Name.Trim();

                        updatedPathBase = String.Format(
                            @"/Images/Products/{0}/{1}/{2}/",
                            newBrand,
                            newCategory,
                            newProductName);

                        imageOldDirectory = String.Format(
                            @"/Images/Products/{0}/{1}/{2}/",
                            oldBrand,
                            oldCategory,
                            oldProductName);

                        if (!updatedPathBase.Equals(imageOldDirectory))
                        {
                            if (!Directory.Exists(Server.MapPath(updatedPathBase)))
                            {
                                Directory.CreateDirectory(Server.MapPath(updatedPathBase));
                            }

                            foreach (var image in productUpdateImages)
                            {
                                var startImagePath = image.ImagePath.LastIndexOf('/');
                                var imageName = image.ImagePath.Substring(
                                    startImagePath + 1,
                                    image.ImagePath.Length - startImagePath - 1);

                                image.ImagePath = updatedPathBase + imageName;
                                this.Data.ProductImages.Update(image);
                                this.Data.SaveChanges();
                                System.IO.File.Move(
                                    this.Server.MapPath(Path.Combine(imageOldDirectory, imageName)),
                                    this.Server.MapPath(Path.Combine(updatedPathBase, imageName)));
                            }
                            this.ProcessDirectory(Server.MapPath(String.Format(@"/Images/Products/{0}", oldBrand)));

                        }

                    }

                    return this.RedirectToAction("Index");
                }
            }

            ViewBag.BrandId = new SelectList(this.Data.Brands.All(), "Id", "Name", product.BrandId);
            ViewBag.CategoryId = new SelectList(this.Data.Categories.All(), "Id", "Name", product.CategoryId);

            return this.View(product);
        }
        
        public ActionResult Delete([Bind(Include = "Id")] Product product)
        {
            var productImages = this.Data.ProductImages.All().Where(i => i.ProductId == product.Id).ToList();
            if (productImages.Any())
            {
                var imagesPath = productImages.FirstOrDefault().ImagePath;
                var dir = imagesPath.LastIndexOf("/");
                var dirToDelete = imagesPath.Substring(0, dir);

                var directoryMappedPath = Server.MapPath(string.Format("~/{0}/", dirToDelete.Trim()));
                foreach (var image in productImages)
                {
                    this.Data.ProductImages.Delete(image);
                }
                this.Data.SaveChanges();

                if (Directory.Exists(directoryMappedPath))
                {
                    Directory.Delete(directoryMappedPath, true);
                }

            }
            this.Data.Products.Delete(product.Id);
            this.Data.SaveChanges();
            this.UpdateLucene();
            return this.RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult AddPhoto(int id)
        {
            this.ViewBag.productId = id;
            var images = this.Data.ProductImages.All().Where(i => i.ProductId == id).Select(i => i).ToList();

            return this.View(images);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddPhoto()
        {
            var id = int.Parse(RouteData.Values["id"].ToString());
            this.ViewBag.productId = id;

            var product = this.Data.Products.All().FirstOrDefault(p => p.Id == id);

            var brandPath = "~/Images/Products/" + product.Brand.Name.Trim();

            var wholePath = brandPath + "/" + product.Category.Name.Trim() + "/" + product.Name.Trim();
            var data = this.Request.Files;
            for (int i = 0; i < data.Count; i++)
            {

                HttpPostedFileBase file = Request.Files[i];
                if (file != null && file.ContentLength > 0)
                    try
                    {

                        bool brandFolderExist = Directory.Exists(Server.MapPath(brandPath));
                        if (!brandFolderExist)
                        {
                            Directory.CreateDirectory(Server.MapPath(brandPath));
                        }

                        bool cathegoryFolderExist = Directory.Exists(Server.MapPath(wholePath));
                        if (!cathegoryFolderExist)
                        {
                            Directory.CreateDirectory(Server.MapPath(wholePath));
                        }


                        string path = Path.Combine(Server.MapPath(wholePath), Path.GetFileName(file.FileName));

                        file.SaveAs(path);

                        var imagePathInDatabase = wholePath.Substring(1, wholePath.Length - 1) + "/" + file.FileName;

                        var image = new ProductImage()
                                        {
                                            ProductId = id,
                                            ImagePath = imagePathInDatabase,
                                            TimeUploaded = DateTime.Now
                                        };

                        this.Data.ProductImages.Add(image);
                        product.ProductImages.Add(image);

                        this.Data.SaveChanges();

                        ViewBag.Message = "File uploaded successfully";
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Message = "ERROR:" + ex.Message.ToString();
                    }
                else
                {
                    ViewBag.Message = "You have not specified a file.";
                }
            }


            var productImages = this.Data.ProductImages.All().Where(i => i.ProductId == id).Select(i => i).ToList();

            this.ViewBag.images = productImages;

            return RedirectToAction("AddPhoto", "Product", new { id = id });
        }

        public ActionResult DeletePhoto(int id, int photoId)
        {
            var image = this.Data.ProductImages.All().FirstOrDefault(p => p.Id == photoId);

            this.Data.ProductImages.Delete(image);
            this.Data.SaveChanges();
            this.DeletePhysicalFiles(image.ImagePath);

            this.AddNotification(
                String.Format("Вие успешно изтрихте снимка с наименование - {0}", image.ImagePath),
                NotificationType.SUCCESS);
            return this.RedirectToAction("EditAlbum", "Product", new { id = id });
        }

        public ActionResult EditAlbum(int id)
        {
            var productImages = this.Data.Products.All().FirstOrDefault(p => p.Id == id).ProductImages.ToList();
            var changedImagesPaths = new List<ProductImage>();
            foreach (var image in productImages)
            {
                byte[] bytes = Encoding.UTF8.GetBytes(image.ImagePath);
                var pathUtf8 = Encoding.UTF8.GetString(bytes);
                image.ImagePath = pathUtf8.Trim();
                this.Data.ProductImages.Update(image);
                this.Data.SaveChanges();
            }

            this.ViewBag.ProductId = id;
            return this.View(productImages);

        }

        [HttpPost]
        public ActionResult GetProductQuantities(int productId, string sizeType)
        {

            var productQuantities =
                this.Data.Quantities.All()
                    .Where(q => q.ProductId == productId)
                    .Select(q => new EditQuantityViewModel() { Amount = q.Amount, SizeLabel = q.Size.Label })
                    .ToDictionary(q => q.SizeLabel, q => q.Amount);

            var sizes = this.Data.Sizes.All().Where(s => s.SizeType.ToString() == sizeType).Select(s => new SizeViewModel()
                                            {
                                                Id = s.Id,
                                                Label = s.Label,
                                                SizeType = s.SizeType
                                            }).ToList();

            var dataToReturn = new Dictionary<string, string>();

            foreach (var size in sizes)
            {
                if (productQuantities.ContainsKey(size.Label))
                {
                    dataToReturn.Add(size.Label, productQuantities[size.Label].ToString());
                }
                else
                {
                    dataToReturn.Add(size.Label, "0");
                }
            }

            return this.Json(dataToReturn, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult ChangeProductQuantities(ChangeProductQuantityViewModel model)
        {
            if (ModelState.IsValid)
            {
                var quantities = this.Data.Quantities.All().Where(q => q.ProductId == model.ProductId).ToList();

                var modelQuantities = model.Quantities.ToDictionary(q => q.SizeLabel, q => q.Amount);

                foreach (var quantity in modelQuantities)
                {
                    if (quantities.Any(q => q.Size.Label == quantity.Key))
                    {
                        var changeQuantity =
                            this.Data.Quantities.All().FirstOrDefault(q => q.Size.Label.Equals(quantity.Key));
                        changeQuantity.Amount = quantity.Value;

                        this.Data.Quantities.Update(changeQuantity);
                        this.Data.SaveChanges();
                    }
                    else
                    {
                        var sizeId = this.Data.Sizes.All().FirstOrDefault(s => s.Label == quantity.Key).Id;

                        var newQuantity = new Quantity()
                                              {
                                                  Amount = quantity.Value,
                                                  ProductId = model.ProductId,
                                                  SizeId = sizeId
                                              };

                        this.Data.Quantities.Add(newQuantity);
                        this.Data.SaveChanges();
                    }
                }
            }
            var newQuantities =
                this.Data.Quantities.All()
                    .Where(q => q.ProductId == model.ProductId)
                    .Select(q => new EditQuantityViewModel() { Amount = q.Amount, SizeLabel = q.Size.Label })
                    .ToDictionary(q => q.SizeLabel, q => q.Amount);
            return this.Json(newQuantities, JsonRequestBehavior.AllowGet);
        }
        private void UpdateLucene()
        {
            LuceneSearch.ClearLuceneIndex();
            LuceneSearch.AddUpdateLuceneIndex(this.Data.Products.All());
        }
    }
}