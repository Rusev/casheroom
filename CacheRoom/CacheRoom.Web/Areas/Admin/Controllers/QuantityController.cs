﻿namespace CacheRoom.Web.Areas.Admin.Controllers
{
    using System.Linq;
    using System.Net;
    using System.Web.Mvc;

    using CacheRoom.Data.Contracts;
    using CacheRoom.Models;
    using CacheRoom.Web.Areas.Admin.Models.Store;
    using CacheRoom.Web.Controllers;
    using CacheRoom.Web.Extensions;

    public class QuantityController : BaseController
    {
        public QuantityController(ICacheRoomData data)
            : base(data)
        {
        }

        public ActionResult Add(int? id)
        {
            if (!this.Data.Sizes.All().Any())
            {
                this.AddNotification("Няма създадени размери. Моля добавете размер и след това добавете количество от желаният размер към продукт.",NotificationType.WARNING);
                return this.RedirectToAction("Create", "Size");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var product = this.Data.Products.Find(id);
            if (product == null)
            {
                return this.HttpNotFound();
            }

            QuantityViewModel viewModel = new QuantityViewModel();

            viewModel.ProductId = product.Id;

            viewModel.Sizes = this.Data.Sizes.All().Where(size => size.SizeType == product.Category.SizeType).ToList();

            return this.View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add([Bind(Include = "ProductId,Sizes")] QuantityViewModel viewModel)
        {

            var product = this.Data.Products.Find(viewModel.ProductId);
            viewModel.Sizes = this.Data.Sizes.All().Where(size => size.SizeType == product.Category.SizeType).ToList();

            if (ModelState.IsValid)
            {
                foreach (var size in viewModel.Sizes)
                {
                    int amount = 0;
                    int.TryParse(this.Request[size.Label], out amount);
                    //if (amount == 0)
                    //{
                    //    continue;
                    //}

                    var quantity = this.Data.Quantities.All().SingleOrDefault(q => q.ProductId == product.Id && q.SizeId == size.Id);

                    if (quantity == null)
                    {
                        quantity = new Quantity();
                        quantity.ProductId = product.Id;
                        quantity.SizeId = size.Id;
                        quantity.Amount = amount;
                        this.Data.Quantities.Add(quantity);
                    }
                    else
                    {
                        quantity.Amount += amount;
                        this.Data.Quantities.Update(quantity);
                    }
                }

                this.Data.SaveChanges();

                return this.RedirectToAction("AddPhoto", "Product", new { id = viewModel.ProductId });
            }

            return this.View(viewModel);
        }
    }
}