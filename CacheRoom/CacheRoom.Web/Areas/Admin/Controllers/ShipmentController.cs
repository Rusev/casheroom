﻿namespace CacheRoom.Web.Areas.Admin.Controllers
{
    using System;
    using System.Web.Mvc;
    using System.Linq;
    using System.Xml.Linq;
    using System.Collections.Generic;
    using System.Web.WebPages;
    using System.Xml.XPath;
    
    using CacheRoom.Data.Contracts;
    using CacheRoom.EcontServiceXmlWorkers;
    using CacheRoom.Web.Areas.Admin.Models.Econt;
    using CacheRoom.Web.Controllers;
    using CacheRoom.Web.Extensions;
    using CacheRoom.Web.Infrastructure.Email;
    using CacheRoom.Web.Models;
    using CacheRoom.Web.Properties;

    using EcontServiceRequestBuilders;
    using EcontServiceRequestBuilders.RequestModels;

    using Ninject.Infrastructure.Language;
    
    [Authorize(Roles = "AppAdmin")]
    public class ShipmentController : BaseController
    {
        public ShipmentController(ICacheRoomData data)
            : base(data)
        {
        }
        public ActionResult Index()
        {
            var allShipments =
                this.Data.Shipments.All()
                    .OrderByDescending(shipment => shipment.DateCreated)
                    .Select(ShipmentViewModel.ViewModel)
                    .ToEnumerable();

            return this.View(allShipments);
        }

        public ActionResult Sended()
        {
            return
                this.View(this.Data.Shipments.All().Where(s => s.IsSended).OrderByDescending(s => s.DateCreated)
                    .Select(ShipmentViewModel.ViewModel).AsEnumerable());
        }

        public ActionResult NotSended()
        {
            return
                this.View(
                    this.Data.Shipments.All()
                        .Where(s => !s.IsSended)
                        .OrderByDescending(s => s.DateCreated)
                        .Select(ShipmentViewModel.ViewModel).AsEnumerable());
        }

        public ActionResult Canceled()
        {
            return
                this.View(
                    this.Data.Shipments.All()
                        .Where(s => s.IsCanceled)
                        .OrderByDescending(s => s.DateCreated)
                        .Select(CanceledShipmentViewModel.ViewModel)
                        .AsEnumerable());
        }
        public ActionResult Prepare(int id)
        {
            
            var receiverInformation = this.Data.Shipments.All().FirstOrDefault(s => s.Id == id).ReceiverInformation;
            var productsCount = this.Data.ShipmentProducts.All().Where(s => s.ShipmentId == id).Sum(s=>s.Quantity);

            XDocument doc = XDocument.Parse(receiverInformation);

            var receiverName = (string)doc.Root.Element("name");
            var receiverEmail = (string)doc.Root.Element("receiver_email");
            var receiverPhone = (string)doc.Root.Element("phone_num");

            var shipmentProducts =
                this.Data.ShipmentProducts.All()
                    .Where(sp => sp.ShipmentId == id)
                    .Select(ShipmentProductViewModel.ViewModel)
                    .ToList();

            var shipment = new PrepareShipmentViewModel()
                {   
                    ShipmentId = id,
                    ReceiverName = receiverName,
                    ReceiverEmail = receiverEmail,
                    ReceiverPhone = receiverPhone,
                    ShipmentProducts = shipmentProducts,
                    ProductsCount = productsCount
                };

            return this.View(shipment);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Prepare(PrepareShipmentViewModel shipmentData)
        {
            ModelState.Remove("ShipmentProducts");
            if (ModelState.IsValid)
            {
                var shipment = this.Data.Shipments.All().FirstOrDefault(s => s.Id == shipmentData.ShipmentId);

                var shipmentInformation = shipmentData.ShipmentSetup;

                var shipmentPaymentInformation = shipmentData.PaymentSetup;

                var shipmentProducts = this.Data.ShipmentProducts.All().Where(s=>s.ShipmentId==shipmentData.ShipmentId).ToList();
                
                var requestBuilder = new ShipmentRequestBuilder();
                
                var addressInfo = new AddressViewModel() { Block = null, Apartment = null, Entrance = null, Floor = null };
                if (shipmentInformation.TarrifSubCode.ToString() == "DOOR")
                {
                    addressInfo.Block = Settings.Default.SenderBlock;
                    addressInfo.Entrance = Settings.Default.SenderEntrance;
                    addressInfo.Floor = Settings.Default.SenderFloor;
                    addressInfo.Apartment = Settings.Default.SenderApartment;
                }
                
                var senderModel = new SenderModel()
                    {
                        CompanyName = Settings.Default.CompanyName,
                        PersonName = Settings.Default.ContactPersonName,
                        PostCode = Settings.Default.SenderPostCode,
                        AddressAppartment = addressInfo.Apartment ?? string.Empty,
                        AddressBlock = addressInfo.Block ?? string.Empty,
                        AddressEntrance = addressInfo.Entrance ?? string.Empty,
                        AddressFloor = addressInfo.Floor ?? string.Empty,
                        AddressOther = Settings.Default.SenderAddressOther ?? string.Empty,
                        CityName = Settings.Default.SenderCityName ?? string.Empty,
                        Email = Settings.Default.ContactPersonEmail,
                        PhoneNumber = Settings.Default.ContactPersonPhoneNumber,
                        EmailOnDelivery = Settings.Default.ContactPersonEmail,
                        Neighbourhood = Settings.Default.SenderNeighbourhood ?? string.Empty,
                        Street = Settings.Default.SenderStreet ?? string.Empty,
                        StreetNumber = Settings.Default.SenderStreetNumber ?? string.Empty,
                        OfficeCode = Settings.Default.OfficeCode
                    };

                var senderXmlData = requestBuilder.CreateSenderPart(senderModel);
                shipment.SenderInformation = senderXmlData.ToString();
                this.Data.Shipments.Update(shipment);

                var paymentModel = new PaymentModel()
                    {
                        PaymentSide = shipmentPaymentInformation.PaymentSide.ToString(),
                        KeyWord = string.Empty,
                        PaymentMethod =
                            shipmentPaymentInformation.PaymentMethod.ToString(),
                        PercentShare = shipmentPaymentInformation.SharePercent ?? 0,
                        ReceiverShareSum = shipmentPaymentInformation.ReceiverShareSum ?? 0
                    };

                var shipmentPaymentXmlData = requestBuilder.CreatePaymentPart(paymentModel);
                shipment.PaymentInformation = shipmentPaymentXmlData.ToString();
                this.Data.Shipments.Update(shipment);

                var shipmentModel = new ShipmentModel()
                    {
                        DeliveryDate = shipmentInformation.DeliveryDay,
                        EnvelopeNumber = shipmentInformation.EnvelopeNumber,
                        InstructionsReturn =
                            shipmentInformation.ReturnsInstruction.ToString(),
                        PackCount = shipmentProducts.Sum(s => s.Quantity),
                        PayAfterAccept = shipment.PayAfterCheck ? "1":"0",
                        PayAfterTest = shipment.PayAfterTest ? "1":"0",
                        SendDate = shipmentInformation.SendDate,
                        ShipmentDescription = shipmentInformation.ShipmentDescription,
                        ShipmentType = shipmentInformation.ShipmentType.ToString(),
                        SizeUnderSixty = shipmentInformation.SizeUnderSixty,
                        Weight = shipmentInformation.ShipmentWeight,
                        TariffSubCode = shipmentInformation.TarrifSubCode + "_" + shipment.ReceiverChosenShipmentType,
                    };

                var shipmentInformationXmlData = requestBuilder.CreateShipmentPart(shipmentModel);
                shipment.ShipmentInformation = shipmentInformationXmlData.ToString();
                this.Data.Shipments.Update(shipment);

                var econtClientElement = requestBuilder.CreateClientPart(
                    Settings.Default.EcontClientUserName,
                    Settings.Default.EcontClientPassword);

                shipment.EcontClientInformation = econtClientElement.ToString();

                var systemElement = requestBuilder.CreateSystemPart();

                shipment.SystemInformation = systemElement.ToString();
                this.Data.Shipments.Update(shipment);
                this.Data.SaveChanges();

                return this.RedirectToAction("Send",new{id=shipmentData.ShipmentId});
            }

            return this.View(shipmentData);
        }

        public ActionResult Send(int id)
        {

            var shipment = this.Data.Shipments.All().FirstOrDefault(s => s.Id == id);

            var overallProductsPrice = shipment.ShipmentProducts.Sum(p => p.Quantity * p.Product.Price);

            var additionalServicesViewModel = new AdditionalServicesViewModel();
            
            additionalServicesViewModel.ShipmentId = id;
            
            return this.View(additionalServicesViewModel);

        }

        public int GetNotSendedShipmentsCount()
        {
            var shipmentsCount = this.Data.Shipments.All().Count(s => s.IsSended == false);

            return shipmentsCount;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Send(AdditionalServicesViewModel additionalServices)
        {
            if (ModelState.IsValid)
            {
                var shipment = this.Data.Shipments.All().FirstOrDefault(s => s.Id == additionalServices.ShipmentId);

                var systemElement = XElement.Parse(shipment.SystemInformation);

                var econtClientElement = XElement.Parse(shipment.EcontClientInformation);

                var receiverElement = XElement.Parse(shipment.ReceiverInformation);

                var senderElement = XElement.Parse(shipment.SenderInformation);

                var paymentElement = XElement.Parse(shipment.PaymentInformation);

                var shipmentElement = XElement.Parse(shipment.ShipmentInformation);
                var priorityType = shipment.PriorityType.ToString();
                var priorityHour = string.Empty;

                if (!priorityType.Equals(PriorityType.None.ToString()))
                {
                    priorityHour = shipment.PriorityHour.ToString();
                }

                var servicesData = new ServicesModel()
                {
                    PriorityHour = priorityHour,
                    BackReceip = additionalServices.BackReceip ? "ON" : string.Empty,
                    BillOfGoods =
                        additionalServices.BillOfGoods ? "ON" : string.Empty,
                    CachCurrency = additionalServices.CachCurrency,
                    CachOnDeliveryAgreementNum =
                        additionalServices.CachOnDeliveryAgreementNum ?? string.Empty,
                    CachOnDeliveryType =
                        additionalServices.CachOnDeliveryType.ToString(),
                    PriorityType = priorityType,
                    ProductPrice =
                        this.Data.ShipmentProducts.All()
                        .Where(p => p.ShipmentId == shipment.Id)
                        .Sum(p => p.Quantity * p.Product.Price),
                    TwoWayShipment = additionalServices.TwoWayShipment ? "ON" : string.Empty
                };

                var requestBuilder = new ShipmentRequestBuilder();

                var servicesElement = requestBuilder.CreateServicesPart(servicesData);

                var shipmentRequestDocument = requestBuilder.CreateXmlRequest(
                    systemElement,
                    econtClientElement,
                    senderElement,
                    receiverElement,
                    shipmentElement,
                    paymentElement,
                    servicesElement);

                var xmlRequester = new XmlRequester();

                var responseDocument = xmlRequester.InitiateRequest(ParcelImportUrl, shipmentRequestDocument).ToString();

                XDocument doc = XDocument.Parse(responseDocument);
                var error = doc.XPathSelectElement("/response/result/e/error").Value;
                var shipmentId = doc.XPathSelectElement("/response/result/e/loading_id").Value;
                var shipmentNumber = doc.XPathSelectElement("/response/result/e/loading_num").Value;
                var shipmentDeliveryDate = doc.XPathSelectElement("/response/result/e/delivery_date").Value;
                shipment.IsSended = true;
                shipment.ShipmentNumber = shipmentNumber;
                shipment.ShipmentIdentityNumber = shipmentId;
                shipment.DeliveryDate = DateTime.Parse(shipmentDeliveryDate);
                this.Data.Shipments.Update(shipment);

                this.SendMail("info@cacheroom.com", "Проследяване на поръчка", EmailContainer.EMAIL_TO_USER_READY_SHIPMENT_NUMBER_FOR_TRACKING);

                this.Data.SaveChanges();
                return this.RedirectToAction("Sended");
            }

            return this.View(additionalServices);
        }

        public ActionResult CancelShipment(string shipmentNumber)
        {
            var requester = new RefuseShipmentRequestBuilder();

            var cancelShipmentNumber = requester.CreateShipmentsPart(shipmentNumber);
            var client = requester.CreateClientPart(Settings.Default.EcontClientUserName, Settings.Default.EcontClientPassword);

            var cancelShipmentXml = requester.CreateCancelShipmentsRequest(client, cancelShipmentNumber);
            var xmlRequester = new XmlRequester();

            var responseDocument = xmlRequester.InitiateRequest(ServicesToolUrl, cancelShipmentXml).ToString();

            var shipment = this.Data.Shipments.All().FirstOrDefault(s => s.ShipmentNumber.Equals(shipmentNumber));
            shipment.IsCanceled = true;
            this.Data.Shipments.Update(shipment);
            this.Data.SaveChanges();
            
            this.AddNotification(String.Format("Успешно анулирахте пратка с номер - {0}",shipmentNumber),NotificationType.SUCCESS);
            return this.RedirectToAction("Canceled");

        }
        
        public ActionResult TraceShipment(string shipmentNumber)
        {
            var requester = new TrackShipmentRequestBuilder();

            var trackShipmentNumber = requester.CreateShipmentsPart(shipmentNumber);

            var client = requester.CreateClientPart(Settings.Default.EcontClientUserName, Settings.Default.EcontClientPassword);

            var trackShipmentXml = requester.CreateTrackShipmentRequest(client, trackShipmentNumber);

            var xmlRequester = new XmlRequester();

            var responseDocument = xmlRequester.InitiateRequest(ServicesToolUrl, trackShipmentXml);

            var loadingNumber = responseDocument.XPathSelectElement("/response/shipments/e/loading_num").Value;

            var isImported = responseDocument.XPathSelectElement("/response/shipments/e/is_imported").Value.AsBool();

            var storage = responseDocument.XPathSelectElement("/response/shipments/e/storage").Value;

            var blankUrl = responseDocument.XPathSelectElement("/response/shipments/e/blank_yes").Value;

            var trackEvents = responseDocument.XPathSelectElements("/response/shipments/e/tracking/row");

            var events = new List<ShipmentEventViewModel>();

            if (trackEvents.Any())
            {
                foreach (var e in trackEvents)
                {
                    var trackEvent = new ShipmentEventViewModel()
                                         {
                                             Event = e.XPathSelectElement("/row/event").Value,
                                             Name = e.XPathSelectElement("/row/name").Value,
                                             Time = e.XPathSelectElement("/row/time").Value
                                         };

                    events.Add(trackEvent);
                }
            }
            var trackShipmentViewModel = new TrackShipmentViewModel()
                        {
                            ShipmentNumber = loadingNumber,
                            IsImported = isImported ? "Заведена" : "Не заведена",
                            Storage = storage,
                            ShipmentEvents = events,
                            BlankUrl = blankUrl
                        };
            var trackShipments = new List<TrackShipmentViewModel>();
            trackShipments.Add(trackShipmentViewModel);
            
            return this.View(trackShipments);
        }

    }
}