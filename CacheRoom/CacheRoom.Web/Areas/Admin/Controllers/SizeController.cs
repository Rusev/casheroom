﻿namespace CacheRoom.Web.Areas.Admin.Controllers
{
    using System.Linq;
    using System.Net;
    using System.Web.Mvc;

    using CacheRoom.Data.Contracts;
    using CacheRoom.Models;
    using CacheRoom.Web.Areas.Admin.Models.Store;
    using CacheRoom.Web.Controllers;
    using CacheRoom.Web.Extensions;

    using PagedList;
    public class SizeController : BaseController
    {
        public SizeController(ICacheRoomData data)
            : base(data)
        {
        }

        public ActionResult Index(int page = 1, int pageSize = 12)
        {
            return this.View(this.Data.Sizes.All().OrderByDescending(size => size.Label)
                .Select(SizeViewModel.ViewModel)
                .ToPagedList(page, pageSize));
        }

        public ActionResult Create()
        {
            return this.View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SizeViewModel sizeData)
        {
            var sizes = this.Data.Sizes.All().Select(s => s.Label);

            if (ModelState.IsValid)
            {
                if (sizes.Contains(sizeData.Label))
                {
                    this.AddNotification("Вече съществува размер с такова име. Моля задайте ново уникално име.", NotificationType.WARNING);

                    return this.View();
                }
                var size = new Size();
                size.Label = sizeData.Label;
                size.SizeType = sizeData.SizeType;

                this.Data.Sizes.Add(size);
                this.Data.SaveChanges();

                return this.RedirectToAction("Index");
            }

            return this.View(sizeData);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var size = this.Data.Sizes.All()
                .Where(s => s.Id == id)
                .Select(SizeViewModel.ViewModel)
                .FirstOrDefault();

            if (size == null)
            {
                return this.HttpNotFound();
            }

            return this.View(size);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, SizeViewModel sizeData)
        {
            if (ModelState.IsValid)
            {
                var size = this.Data.Sizes.Find(id);
                size.Label = sizeData.Label;
                size.SizeType = sizeData.SizeType;

                this.Data.Sizes.Update(size);
                this.Data.SaveChanges();

                return this.RedirectToAction("Index");
            }

            return this.View(sizeData);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete([Bind(Include="Id")] Size size)
        {
            this.Data.Sizes.Delete(size);
            this.Data.SaveChanges();
            return this.RedirectToAction("Index");
        }

    }
}