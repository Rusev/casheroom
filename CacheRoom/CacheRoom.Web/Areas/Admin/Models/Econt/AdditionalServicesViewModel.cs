﻿namespace CacheRoom.Web.Areas.Admin.Models.Econt
{
    using System.ComponentModel.DataAnnotations;
    
    using EcontServiceRequestBuilders.RequestModels;

    public class AdditionalServicesViewModel
    {
        public int ShipmentId { get; set; }

        [Required(ErrorMessage = "Полето е задължително!")]
        [Display(Name = "Обратна Разписка")]
        public bool BackReceip { get; set; }

        [Required(ErrorMessage = "Полето е задължително!")]
        [Display(Name = "Стокова разписка")]
        public bool BillOfGoods { get; set; }

        [Required(ErrorMessage = "Полето е задължително!")]
        [Display(Name = "Двупосочна пратка")]
        public bool TwoWayShipment { get; set; }

        [Required(ErrorMessage = "Полето е задължително!")]
        [Display(Name = "Тип на наложения платеж")]
        public CachOnDeliveryType CachOnDeliveryType { get; set; }

        [Required(ErrorMessage = "Полето е задължително!")]
        [Display(Name = "Валута на наложения платеж")]
        public string CachCurrency { get; set; }

        [Display(Name = "Номер на споразумението за изплащане")]
        public string CachOnDeliveryAgreementNum { get; set; }

        public string DeclaredValue { get; set; }
    }
}