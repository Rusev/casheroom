﻿namespace CacheRoom.Web.Areas.Admin.Models.Econt
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq.Expressions;

    using CacheRoom.Models;

    public class CanceledShipmentViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Потребител")]
        public User User { get; set; }

        public string ReceiverInformation { get; set; }

        [Display(Name = "Статус")]
        public bool IsCanceled { get; set; }

        [Display(Name = "Дата на поръчка")]
        public DateTime DateCreated { get; set; }

        public ICollection<ShipmentProduct> ShipmentProducts { get; set; }

        [Display(Name = "Номер на пратка")]
        public string ShipmentNumber { get; set; }
        public static Expression<Func<Shipment, CanceledShipmentViewModel>> ViewModel
        {
            get
            {
                return
                    s =>
                    new CanceledShipmentViewModel()
                    {
                        Id = s.Id,
                        ReceiverInformation = s.ReceiverInformation,
                        IsCanceled = s.IsCanceled,
                        DateCreated = s.DateCreated,
                        ShipmentProducts = s.ShipmentProducts,
                        User = s.User,
                        ShipmentNumber = s.ShipmentNumber
                    };
            }
        }
    }
}