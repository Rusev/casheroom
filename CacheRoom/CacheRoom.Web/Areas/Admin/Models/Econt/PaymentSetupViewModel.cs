﻿namespace CacheRoom.Web.Areas.Admin.Models.Econt
{
    using System.ComponentModel.DataAnnotations;
   
    using EcontServiceRequestBuilders.RequestModels;

    public class PaymentSetupViewModel
    {
        [Required(ErrorMessage = "не може да бъде без стойност")]
        [Display(Name = "Страна платец")]
        public PaymentSide PaymentSide { get; set; }

        [Required(ErrorMessage = "не може да бъде без стойност")]
        [Display(Name = "Начин на плащане")]
        public PaymentMethod PaymentMethod { get; set; }
        
        [Display(Name = "Сума споделяне")]
        public decimal? ReceiverShareSum { get; set; }

        [Display(Name = "Процент споделяне")]
        public int? SharePercent { get; set; }

    }
}