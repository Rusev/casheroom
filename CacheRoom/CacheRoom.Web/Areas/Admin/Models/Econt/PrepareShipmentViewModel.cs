﻿namespace CacheRoom.Web.Areas.Admin.Models.Econt
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web;

    public class PrepareShipmentViewModel
    {
        public int ShipmentId { get; set; }

        [Display(Name="Име")]
        [Required(ErrorMessage = "Името на получателя не може да бъде без стойност.")]
        public string ReceiverName { get; set; }

        [Display(Name="Имейл")]
        [Required(ErrorMessage="Имейлът на получателя не може да бъде без стойност.")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "не е валиден имейл адрес")]
        public string ReceiverEmail { get; set; }

        [Display(Name = "Телефон")]
        [Required(ErrorMessage = "Телефонът на получателя не може да бъде без стойност.")]
        [DataType(DataType.PhoneNumber)]
        [Phone(ErrorMessage = "не е валиден телефонен номер")]
        public string ReceiverPhone { get; set; }

        public ICollection<ShipmentProductViewModel> ShipmentProducts { get; set; }

        [Display(Name = "Оператор")]
        public SenderViewModel Sender { get; set; }

        [Display(Name = "Настройки за пратката")]
        public ShipmentSetupViewModel ShipmentSetup { get; set; }

        [Display(Name = "Настройки за плащане на товарителницата")]
        public PaymentSetupViewModel PaymentSetup { get; set; }

        public int ProductsCount { get; set; }

        public string SendType { get; set; }
    }
}