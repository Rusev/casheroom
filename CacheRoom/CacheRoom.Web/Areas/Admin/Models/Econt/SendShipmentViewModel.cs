﻿namespace CacheRoom.Web.Areas.Admin.Models.Econt
{
    public class SendShipmentViewModel
    {
        public decimal ShipmentProductsPrice { get; set; }

        public AdditionalServicesViewModel AdditionalServices { get; set; }
        
    }
}