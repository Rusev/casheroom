﻿namespace CacheRoom.Web.Areas.Admin.Models.Econt
{
    using System.ComponentModel.DataAnnotations;

    using Microsoft.AspNet.Identity;

    public class SenderViewModel
    {
        [Required(ErrorMessage = "не може да е без стойност")]
        [Display(Name="Потребителско име за Е-Еконт")]
        public string EcontUserName { get; set; }

        [Required(ErrorMessage = "не може да е без стойност")]
        [Display(Name = "Парола за Е-Еконт")]
        [DataType(DataType.Password)]
        public string EcontPassword { get; set; }

        [Required(ErrorMessage = "не може да е без стойнсот")]
        [Display(Name="Име и фамилия")]
        public string OperatorName { get; set; }

        [Required(ErrorMessage = "не може да е без стойност")]
        [Display(Name = "Име на фирмата")]
        public string CompanyName { get; set; }
        
        [Required(ErrorMessage = "не може да е без стойност")]
        [Display(Name = "Телефон")]
        [DataType(DataType.PhoneNumber)]
        [Phone(ErrorMessage="невалиден телефонен номер")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "не може да е без стойност")]
        [Display(Name = "Имейл")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "невалиден имейл адрес")]
        public string Email { get; set; }

        [Required(ErrorMessage = "не може да e без стойност")]
        [Display(Name = "Населено място*")]
        public string LivingPlace { get; set; }

        [Display(Name = "Квартал")]
        public string Neighbourhood { get; set; }

        [Required(ErrorMessage = "не е попълнено")]
        [Display(Name = "Улица*")]
        public string Street { get; set; }

        [Required(ErrorMessage = "не е попълнено")]
        [Display(Name = "Номер на улица*")]
        public string StreetNumber { get; set; }

        [Display(Name = "Адрес")]
        [RegularExpression("([бл. ]+[0-9]+)+[, ]+([вх. A-Яа-я0-9]+)+[, ]+([ап. ]+[0-9]+)", ErrorMessage = "форматът на данните за адреса трябва да бъде следният: бл. 111, вх. А, ап. 11")]
        public string Address { get; set; }

        [Display(Name = "Уточнения по адреса")]
        public string AdditionalInfo { get; set; }
    }
}