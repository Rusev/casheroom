﻿namespace CacheRoom.Web.Areas.Admin.Models.Econt
{
    public class ShipmentEventViewModel
    {
        public string Time { get; set; }

        public string Event { get; set; }

        public string Name { get; set; }
    }
}