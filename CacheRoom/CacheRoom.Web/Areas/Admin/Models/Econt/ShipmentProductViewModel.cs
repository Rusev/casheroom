﻿namespace CacheRoom.Web.Areas.Admin.Models.Econt
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Linq.Expressions;

    using CacheRoom.Models;

    public class ShipmentProductViewModel
    {
        public int Id { get; set; }

        [Display(Name="Количество")]
        public int Quantity { get; set; }
        
        [Display(Name="Продукт")]
        public Product Product { get; set; }

        [Display(Name="Размер")]
        public Size Size { get; set; }

        public static Expression<Func<ShipmentProduct, ShipmentProductViewModel>> ViewModel
        {
            get
            {
                return
                    sp =>
                    new ShipmentProductViewModel() { Quantity = sp.Quantity, Product = sp.Product, Size = sp.Size };
            }
        }
    }
}