﻿namespace CacheRoom.Web.Areas.Admin.Models.Econt
{
    using System;
    using System.ComponentModel.DataAnnotations;

    using EcontServiceRequestBuilders.RequestModels;

    public class ShipmentSetupViewModel
    {
        [Display(Name="Номер на опаковката")]
        public string EnvelopeNumber { get; set; }

        [Display(Name = "Описание на пратката")]
        [Required(ErrorMessage = "не може да бъде без стойност")]
        public string ShipmentDescription { get; set; }

        [Required(ErrorMessage = "не може да бъде без стойност")]
        [Display(Name = "Тип на пратката")]
        public ShipmentType ShipmentType { get; set; }

        [Required(ErrorMessage = "не може да бъде без стойност")]
        [Display(Name="Брой пакети")]
        public int PackCount { get; set; }

        [Display(Name="Тегло на пратката")]
        [Required(ErrorMessage = "не може да бъде без стойност")]
        public decimal ShipmentWeight { get; set; }

        [Required(ErrorMessage = "не може да бъде без стойност")]
        [Display(Name = "Начин на доставка")]
        public TariffSubCode TarrifSubCode { get; set; }

        [Display(Name = "Инструкции при връщане")]
        public InstructionsReturn ReturnsInstruction { get; set; }

        [Display(Name = "Дата на изпращане")]
        public virtual DateTime? SendDate { get; set; }

        [Display(Name = "Дата на доставка")]
        public virtual DateTime? DeliveryDay { get; set; }

        [Display(Name = "Размер под 60 см.")]
        public bool SizeUnderSixty { get; set; }
    }
}