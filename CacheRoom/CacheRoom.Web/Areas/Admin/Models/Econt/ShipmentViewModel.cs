﻿namespace CacheRoom.Web.Areas.Admin.Models.Econt
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq.Expressions;

    using CacheRoom.Models;

    public class ShipmentViewModel
    {
        public int Id { get; set; }

        [Display(Name="Потребител")]
        public User User { get; set; }

        public string ReceiverInformation { get; set; }

        [Display(Name="Статус")]
        public bool IsSended { get; set; }

        [Display(Name="Дата на поръчка")]
        public DateTime DateCreated { get; set; }

        public ICollection<ShipmentProduct> ShipmentProducts { get; set; }

        public string ShipmentNumber { get; set; }
        public static Expression<Func<Shipment, ShipmentViewModel>> ViewModel
        {
            get
            {
                return
                    s =>
                    new ShipmentViewModel()
                        {
                            Id = s.Id,
                            ReceiverInformation = s.ReceiverInformation,
                            IsSended = s.IsSended,
                            DateCreated = s.DateCreated,
                            ShipmentProducts = s.ShipmentProducts,
                            User = s.User,
                            ShipmentNumber = s.ShipmentNumber
                        };
            }
        }
    }
}