﻿namespace CacheRoom.Web.Areas.Admin.Models.Econt
{
    using System.Collections.Generic;

    public class TrackShipmentViewModel
    {
        public string ShipmentNumber { get; set; }

        public string IsImported { get; set; }

        public string Storage { get; set; }

        public ICollection<ShipmentEventViewModel> ShipmentEvents { get; set; }

        public string BlankUrl { get; set; }
    }
}