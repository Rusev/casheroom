﻿namespace CacheRoom.Web.Areas.Admin.Models.HomePage
{
    using System;
    using System.Linq.Expressions;

    using CacheRoom.Models;
    
    public class BrandImagesViewModel
    {
        public int Id { get; set; }

        public string ImagePath { get; set; }

        public BrandImagePosition Position { get; set; }

        public string ActionLink { get; set; }

        public bool IsVisible { get; set; }
        
        public static Expression<Func<MiddlePageImage, BrandImagesViewModel>> ViewModel
        {
            get
            {
                return b => new BrandImagesViewModel() { Id = b.Id, ImagePath = b.ImagePath,Position = b.Position,ActionLink = b.ActionLink};
            }
        }
    }
}