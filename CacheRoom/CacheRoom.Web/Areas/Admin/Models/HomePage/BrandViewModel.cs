﻿namespace CacheRoom.Web.Areas.Admin.Models.HomePage
{
    using System;
    using System.Linq.Expressions;

    using CacheRoom.Models;
    using CacheRoom.Web.Areas.Admin.Models.Store;

    public class BrandViewModel
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public static Expression<Func<Brand, BrandViewModel>> ViewModel
        {
            get
            {
                return
                    p =>
                    new BrandViewModel()
                    {
                        Id = p.Id,
                        Name = p.Name
                    };
            }
        }
    }
}