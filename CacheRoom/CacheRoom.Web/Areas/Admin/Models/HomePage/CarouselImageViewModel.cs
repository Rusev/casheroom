﻿namespace CacheRoom.Web.Areas.Admin.Models.HomePage
{
    using System;
using System.Linq.Expressions;

    using CacheRoom.Models;

    public class CarouselImageViewModel
    {
        public int Id { get; set; }

        public string ImagePath { get; set; }

        public string ActionLink { get; set; }

        public bool IsActiveInitial { get; set; }

        public static Expression<Func<CarouselImage, CarouselImageViewModel>> ViewModel
        {
            get
            {
                return c => new CarouselImageViewModel() { Id = c.Id, ImagePath = c.ImagePath };
            }
        }
    }
}