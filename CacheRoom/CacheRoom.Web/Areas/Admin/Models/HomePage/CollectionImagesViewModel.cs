﻿namespace CacheRoom.Web.Areas.Admin.Models.HomePage
{
    using System.Linq.Expressions;

    using CacheRoom.Models;
    using System;

    public class CollectionImagesViewModel
    {
        public int Id { get; set; }

        public string ImagePath { get; set; }

        public CollectionImagePosition Position { get; set; }

        public string ActionLink { get; set; }

        public bool IsVisible { get; set; }

        public static Expression<Func<BottomPageImage, CollectionImagesViewModel>> ViewModel
        {
            get
            {
                return c => new CollectionImagesViewModel()
                                {
                                    Id = c.Id,
                                    ImagePath = c.ImagePath,
                                    Position = c.Position,
                                    ActionLink = c.ActionLink
                                };
            }
        }
    }
}