﻿namespace CacheRoom.Web.Areas.Admin.Models.HomePage
{
    using System;

    using CacheRoom.Models;
    using System.Collections.Generic;
    using System.Linq.Expressions;

    public class EditProductAlbumViewModel
    {
        public int ProductId { get; set; }
        public ProductImage ProductImage { get; set; }
        
        public static Expression<Func<Product, EditProductAlbumViewModel>> ViewModel
        {
            get
            {
                return p => new EditProductAlbumViewModel()
                                {
                                    ProductId = p.Id
                                };
            }
        }
    }
}