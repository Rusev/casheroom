﻿namespace CacheRoom.Web.Areas.Admin.Models.HomePage
{
    public class LinkViewModel
    {
        public int ImageId { get; set; }

        public int LinkId { get; set; }
    }
}