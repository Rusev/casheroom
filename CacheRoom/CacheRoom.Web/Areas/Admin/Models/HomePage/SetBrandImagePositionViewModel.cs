﻿namespace CacheRoom.Web.Areas.Admin.Models.HomePage
{
    public class SetBrandImagePositionViewModel
    {
        public int Position { get; set; }

        public int ImageId { get; set; }
        
    }
}