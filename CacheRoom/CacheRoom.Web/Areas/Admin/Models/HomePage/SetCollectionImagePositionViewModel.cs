﻿namespace CacheRoom.Web.Areas.Admin.Models.HomePage
{
    public class SetCollectionImagePositionViewModel
    {
        public int Position { get; set; }

        public int ImageId { get; set; }
    }
}