﻿namespace CacheRoom.Web.Areas.Admin.Models.Store
{
    using System;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Linq.Expressions;

    using CacheRoom.Models;

    public class BrandViewModel
    {
        public int Id { get; set; }

        [Required]
        [DisplayName("Име")]
        public string Name { get; set; }

        [DisplayName("Описание")]
        public string Description { get; set; }

        public static Expression<Func<Brand, BrandViewModel>> ViewModel
        {
            get
            {
                return b => new BrandViewModel()
                    {
                        Id = b.Id, Name = b.Name, Description = b.Description
                    };
            }
        }
    }
}