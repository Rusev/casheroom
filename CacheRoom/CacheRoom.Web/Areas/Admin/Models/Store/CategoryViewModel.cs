﻿namespace CacheRoom.Web.Areas.Admin.Models.Store
{
    using System;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Linq.Expressions;

    using CacheRoom.Models;

    using Microsoft.Ajax.Utilities;

    public class CategoryViewModel
    {
        public int Id { get; set; }

        [DisplayName("Име")]
        [Required]
        public string Name { get; set; }

        [DisplayName("Описание")]
        public string Description { get; set; }

        [DisplayName("Тип Номерация")]
        [Required]
        public SizeType SizeType { get; set; }

        public static Expression<Func<Category, CategoryViewModel>> ViewModel
        {
            get
            {
                return
                    c =>
                    new CategoryViewModel()
                        {
                            Id = c.Id,
                            Name = c.Name,
                            Description = c.Description,
                            SizeType = c.SizeType
                        };
            }
        }
    }
}