﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CacheRoom.Web.Areas.Admin.Models.Store
{
    public class ChangeProductQuantityViewModel
    {
        public IEnumerable<EditQuantityViewModel> Quantities { get; set; }

        public int ProductId { get; set; }
    }
}