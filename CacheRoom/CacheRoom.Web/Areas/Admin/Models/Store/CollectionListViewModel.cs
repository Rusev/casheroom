﻿namespace CacheRoom.Web.Areas.Admin.Models.Store
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq.Expressions;

    using CacheRoom.Models;
    using System;

    public class CollectionListViewModel
    {
       public int? Id { get; set; }

       public string Name { get; set; }


       public static Expression<Func<Collection, CollectionListViewModel>> ViewModel
       {
           get
           {
               return
                   p =>
                   new CollectionListViewModel()
                   {
                       Id = p.Id,
                       Name = p.Name
                   };
           }
       }
    }
}