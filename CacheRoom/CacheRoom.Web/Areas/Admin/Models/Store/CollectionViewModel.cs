﻿namespace CacheRoom.Web.Areas.Admin.Models.Store
{
    using System;
    using System.ComponentModel;
    using System.Linq.Expressions;
    using CacheRoom.Models;
    public class CollectionViewModel
    {
        public int Id { get; set; }

        [DisplayName("Име")]
        public string Name { get; set; }

        [DisplayName("Описание")]
        public string Description { get; set; }

        public static Expression<Func<Collection, CollectionViewModel>> ViewModel
        {
            get
            {
                return c => new CollectionViewModel()
                    {
                        Id = c.Id, Name = c.Name, Description = c.Description
                    };
            }
        }

    }
}