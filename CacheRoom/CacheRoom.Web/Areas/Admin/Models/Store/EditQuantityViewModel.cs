﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CacheRoom.Web.Areas.Admin.Models.Store
{
    using CacheRoom.Models;

    public class EditQuantityViewModel
    {
        public int? Amount { get; set; }

        public string SizeLabel { get; set; }
    }
}