﻿namespace CacheRoom.Web.Areas.Admin.Models.Store
{
    using System;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Linq.Expressions;

    using CacheRoom.Models;
    
    public class ProductViewModel
    {
        public int Id { get; set; }

        [DisplayName("Име")]
        public string Name { get; set; }

        [Display(Name = "Категория")]
        public int CategoryId { get; set; }

        public string CategoryName { get; set; }

        [Display(Name = "Марка")]
        public int BrandId { get; set; }

        public string BrandName { get; set; }

        [Display(Name = "Колекция")]
        public int? CollectionId { get; set; }

        [Display(Name = "Тип")]
        public ProductType Type { get; set; }

        public Boolean IsOnSale { get; set; }

        [DisplayName("Цена")]
        public decimal Price { get; set; }

        [Display(Name = "Описание")]
        public string Description { get; set; }

        [DisplayName("Общо Количество")]
        public int? Amount { get; set; }

        public static Expression<Func<Product, ProductViewModel>> ViewModel
        {
            get
            {
                return
                    p =>
                    new ProductViewModel()
                        {
                            Id = p.Id,
                            Name = p.Name,
                            CategoryId = p.CategoryId,
                            CategoryName = p.Category.Name,
                            BrandId = p.BrandId,
                            BrandName = p.Brand.Name,
                            CollectionId = p.CollectionId,
                            Type = p.Type,
                            IsOnSale = p.IsOnSale,
                            Price = p.Price,
                            Description = p.Description,
                        };
            }
        }
    }
}