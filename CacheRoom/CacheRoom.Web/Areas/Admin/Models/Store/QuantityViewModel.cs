﻿namespace CacheRoom.Web.Areas.Admin.Models.Store
{
    using CacheRoom.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    public class QuantityViewModel
    {
        public int ProductId { get; set; }

        public List<Size> Sizes { get; set; } 
    }
}