﻿namespace CacheRoom.Web.Areas.Admin.Models.Store
{
    using System;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Linq.Expressions;

    using CacheRoom.Models;

    public class SizeViewModel
    {
        public int Id { get; set; }

        [Required]
        [DisplayName("Етикет")]
        public string Label { get; set; }

        [Required]
        [DisplayName("Тип")]
        public SizeType SizeType { get; set; }

        public static Expression<Func<Size, SizeViewModel>> ViewModel
        {
            get
            {
                return s => new SizeViewModel()
                        {
                            Id = s.Id,
                            Label = s.Label, 
                            SizeType = s.SizeType
                        };
            }
        } 
    }
}