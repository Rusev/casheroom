﻿namespace CacheRoom.Web.Areas.Store.Controllers
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;
    using System.Text;
    using System.IO;

    using CacheRoom.Data.Contracts;
    using CacheRoom.Models;
    using CacheRoom.Web.Areas.Store.Models.Cart;
    using CacheRoom.Web.Areas.Store.Models.Econt;
    using CacheRoom.Web.Areas.Store.Models.Product;
    using CacheRoom.Web.Attributes;
    using CacheRoom.Web.Controllers;
    using CacheRoom.Web.Extensions;
    using CacheRoom.Web.Infrastructure.Cart;
    using CacheRoom.Web.Infrastructure.Email;
    using CacheRoom.Web.Models;

    using EcontServiceRequestBuilders;
    using EcontServiceRequestBuilders.RequestModels;

    using Microsoft.AspNet.Identity;

    public class CartController : BaseController
    {
        private EmailCreator EmailCreator { get; set; }
            

        public CartController(ICacheRoomData data)
            : base(data)
        {
            
        }
        public ActionResult Index()
        {
            var cart = this.GetCart();
            var items = cart.Items();
            var cartItemsViewModels = new List<CartItemViewModel>();
            var userCartId = cart.ItemsCount() > 0 ? cart.Items().FirstOrDefault().CartId : string.Empty;
            this.ViewBag.CartId = userCartId;
            foreach (var i in items)
            {
                var utf8ProductImage = string.Empty;
                if (i.Product.ProductImages.Any())
                {
                    var firstProductImage = i.Product.ProductImages.OrderBy(d => d.TimeUploaded)
                        .FirstOrDefault()
                        .ImagePath.Trim();
                    var bytes = Encoding.UTF8.GetBytes(firstProductImage);
                    utf8ProductImage = Encoding.UTF8.GetString(bytes);
                }
                var itemViewModel = new CartItemViewModel()
                {
                    Id = i.Id,
                    CartId = i.CartId,
                    ProductId = i.ProductId,
                    ProductPrice = i.Product.Price,
                    ProductName = i.Product.Name,
                    ProductBrand = i.Product.Brand.Name,
                    ProductImagePath = utf8ProductImage,
                    ChosenSize = i.Size,
                    Quantity = i.Quantity,
                    CartItemTotal = i.Quantity * i.Product.Price
                };
                cartItemsViewModels.Add(itemViewModel);
            }

            var viewModel = new CartIndexViewModel
            {
                CartItems = cartItemsViewModels,
                CartTotal = cartItemsViewModels.Sum(t => t.CartItemTotal)
            };

            return this.View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(ProductAddedViewModel addedProduct)
        {
            var addedItem = this.Data.Products.All()
               .FirstOrDefault(p => p.Id == addedProduct.ProductId);

            if (addedProduct.ChosenSizeId == null)
            {
                this.AddNotification("Моля, изберете размер за желаният от вас продукт.", NotificationType.WARNING);
                return this.RedirectToAction("Details", "Product", new { id = addedProduct.ProductId });
            }

            var cart = this.GetCart();

            cart.Add(addedItem, (int)addedProduct.ChosenSizeId);
            this.ViewBag.TotalItems = cart.ItemsCount();
            return this.RedirectToAction("Index");

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [HttpParamActionAttribute]
        public ActionResult Remove(string productId, string chosenSize)
        {
            var cart = GetCart();
            cart.Remove(int.Parse(productId), int.Parse(chosenSize));
            this.AddNotification("Успешно премахнахте продукта от вашата количка.", NotificationType.SUCCESS);
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [HttpParamActionAttribute]
        public ActionResult Clear()
        {
            var cart = GetCart();
            cart.Clear();

            return RedirectToAction("Index");
        }

        [ChildActionOnly]
        public ActionResult Items()
        {
            var cart = this.GetCart();
            this.ViewData["ItemsCount"] = cart.ItemsCount();

            return PartialView("_Items");
        }

        public ActionResult PreCheckout(string userCartId)
        {
            var userCart = this.GetCart();
            TempData["Cart"] = userCart;
            this.ViewBag.CartId = userCartId;
            if (this.User.Identity.IsAuthenticated)
            {

                return this.RedirectToAction("CheckOut");
            }

            return this.View();
        }

        public ActionResult CheckOut()
        {
            var currentCart = TempData["Cart"] as Cart;
            var cartItems = currentCart.Items();
            var cartId = currentCart.ItemsCount() > 0 ? currentCart.Items().FirstOrDefault().CartId : string.Empty;
            var counter = 0;
            if (this.User.Identity.IsAuthenticated)
            {
                currentCart.Migrate(this.User.Identity.GetUserName(), cartId);
            }
            if (cartItems.Count == 0)
            {
                this.AddNotification("Няма продукти в количката. Моля изберете продукти, които желаете да закупите.", NotificationType.WARNING);
                return this.RedirectToAction("Index");
            }

            var cartItemsGroupedbyProduct = cartItems.GroupBy(p => p.ProductId).ToList();

            foreach (var cartItem in cartItemsGroupedbyProduct)
            {
                foreach (var item in cartItem)
                {
                    var tempQuan =
                        this.Data.Quantities.All()
                            .FirstOrDefault(q => q.ProductId == cartItem.Key && q.SizeId == item.SizeId).Amount;

                    if (item.Quantity > tempQuan && item.Quantity != 0)
                    {
                        this.AddNotification(String.Format("Наличното количество за избраният от вас продукт - {0} и размер {1} е {2} бр.", item.Product.Name, item.Size.Label, tempQuan), NotificationType.WARNING);
                        return this.RedirectToAction("Index");
                    }
                    if (item.Quantity == 0)
                    {
                        this.AddNotification(String.Format("Минималното количество за даден продукт е 1."), NotificationType.WARNING);
                        return this.RedirectToAction("Index");
                    }
                }
            }

            if (this.User.Identity.IsAuthenticated)
            {
                var clientInfo = new ClientInfoViewModel()
                    {
                        Email = this.UserProfile.Email,
                        NameAndFamilyName = this.UserProfile.NameAndFamilyName,
                        PhoneNumber = this.UserProfile.PhoneNumber
                    };
                var checkoutViewModel = new CheckoutViewModel() { ClientInfo = clientInfo };

                return this.View(checkoutViewModel);
            }
            return this.View();
        }

        private bool CheckEcontData(CheckoutViewModel checkoutData)
        {
            var isValidAddress = true;
            var isValidOffice = true;

            if (checkoutData.EcontInfo.ToAddressInformation.LivingPlace == null)
            {
                isValidAddress = false;
            }
            if (checkoutData.EcontInfo.ToOfficeInformation.LivingPlace == null)
            {
                isValidOffice = false;
            }

            return isValidAddress || isValidOffice;
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CheckOut(CheckoutViewModel checkoutData)
        {
            var chosenShipmentType = string.Empty;
            var isValid = this.CheckEcontData(checkoutData);

            if (!isValid)
            {
                this.AddNotification(String.Format("Трябва да изберете дали доставката да бъде извършена до офис или адрес и да въведете необходимите данни."), NotificationType.WARNING);
                return this.View(checkoutData);
            }

            if (checkoutData.ClientInfo.ShipmentType.Equals("to-door"))
            {
                while (ModelState.FirstOrDefault(ms => ms.Key.ToString().Contains("ToOfficeInformation")).Value != null)
                {
                    ModelState.Remove(ModelState.FirstOrDefault(ms => ms.Key.ToString().Contains("ToOfficeInformation")));
                }
                chosenShipmentType = "DOOR";
            }

            if (checkoutData.ClientInfo.ShipmentType.Equals("to-office"))
            {
                while (ModelState.FirstOrDefault(ms => ms.Key.ToString().Contains("ToAddressInformation")).Value != null)
                {
                    ModelState.Remove(ModelState.FirstOrDefault(ms => ms.Key.ToString().Contains("ToAddressInformation")));
                }

                chosenShipmentType = "OFFICE";
            }

            if (this.ModelState.IsValid)
            {
                var userCart = this.GetCart();
                var clientLivingPlaceInfo = new List<string>();
                var officeCode = string.Empty;
                if (checkoutData.ClientInfo.ShipmentType.Equals("to-door"))
                {
                    clientLivingPlaceInfo = checkoutData.EcontInfo.ToAddressInformation.LivingPlace.Split('-').ToList();
                }
                else if (checkoutData.ClientInfo.ShipmentType.Equals("to-office"))
                {
                    clientLivingPlaceInfo = checkoutData.EcontInfo.ToOfficeInformation.LivingPlace.Split('-').ToList();
                    officeCode = checkoutData.EcontInfo.ToOfficeInformation.OfficeCode;
                }

                var clientCityName = clientLivingPlaceInfo[0];
                var clientCityPostCode = clientLivingPlaceInfo[1];

                var shipmentBuilder = new ShipmentRequestBuilder();
                var addressInfo = new AddressViewModel() { Block = null, Apartment = null, Entrance = null, Floor = null };
                var street = string.Empty;
                var streetNumber = string.Empty;
                var additionalInfo = string.Empty;
                var neighbourhood = string.Empty;

                if (checkoutData.EcontInfo.ToAddressInformation != null)
                {
                    if (checkoutData.EcontInfo.ToAddressInformation.Address != null)
                    {
                        var address = checkoutData.EcontInfo.ToAddressInformation.Address.Split(',');
                        addressInfo.Block = address[0];
                        addressInfo.Entrance = address[1];
                        addressInfo.Floor = address[2];
                        addressInfo.Apartment = address[3];
                    }

                    street = checkoutData.EcontInfo.ToAddressInformation.Street;
                    streetNumber = checkoutData.EcontInfo.ToAddressInformation.StreetNumber;
                    neighbourhood = checkoutData.EcontInfo.ToAddressInformation.Neighbourhood;
                    additionalInfo = checkoutData.EcontInfo.ToAddressInformation.AdditionalInfo;
                }
                var receiverInformation = new ReceiverModel()
                {
                    CityName = clientCityName,
                    Street = street,
                    StreetNumber = streetNumber,
                    PostCode = clientCityPostCode,
                    AddressBlock = addressInfo.Block ?? string.Empty,
                    AddressEntrance = addressInfo.Entrance ?? string.Empty,
                    AddressFloor = addressInfo.Floor ?? string.Empty,
                    AddressAppartment = addressInfo.Apartment ?? string.Empty,
                    AddressOther = additionalInfo,
                    CompanyName = string.Empty,
                    Email = checkoutData.ClientInfo.Email,
                    Neighbourhood = neighbourhood,
                    OfficeCode = officeCode,
                    PersonName = checkoutData.ClientInfo.NameAndFamilyName,
                    PhoneNumber = checkoutData.ClientInfo.PhoneNumber,
                };
                var receiver = shipmentBuilder.CreateRecieverPart(receiverInformation);


                var shipment = new Shipment()
                {
                    ReceiverInformation = receiver.ToString(),
                    IsSended = false,
                    DateCreated = DateTime.Now,
                    ReceiverChosenShipmentType = chosenShipmentType,
                    User = this.UserProfile,
                    PayAfterTest = checkoutData.CashOnDelivery.PayAfterTest,
                    PayAfterCheck = checkoutData.CashOnDelivery.PayAfterAccept,
                    PayAfterTestAndChoose = checkoutData.CashOnDelivery.PayAfterTestAndChoose,
                    PriorityType = checkoutData.AdditionalOptions.PriorityType,
                    PriorityHour = checkoutData.AdditionalOptions.PriorityHour,
                    SmartCourierType = checkoutData.SmartCourier.SmartCourierType
                };

                this.Data.Shipments.Add(shipment);
                this.Data.SaveChanges();

                var pathClient = this.Server.MapPath("~/EmailTemplates/ClientInformation.html");

                this.EmailCreator = new EmailCreator(System.IO.File.ReadAllText(pathClient));

                if (chosenShipmentType.Equals("OFFICE"))
                {
                    this.EmailCreator.InsertShipmentDetails(shipment.Id.ToString(), "Еконт Експрес - до офис");
                }
                else
                {
                    this.EmailCreator.InsertShipmentDetails(shipment.Id.ToString(), "Еконт Експрес - до адрес");
                }

                this.EmailCreator.InsertContacts(receiverInformation.PhoneNumber, receiverInformation.Email);

                if (!String.IsNullOrEmpty(receiverInformation.OfficeCode))
                {
                    var office =
                        this.Data.Offices.All().FirstOrDefault(o => o.OfficeCode == receiverInformation.OfficeCode);

                    var city = this.Data.Cities.All().FirstOrDefault(c => c.Id == office.CityId);

                    var officeNeighbourhood =
                        this.Data.Neighbourhoods.All().FirstOrDefault(n => n.Id == office.NeighbourhoodId);

                    var officeStreet = this.Data.Streets.All().FirstOrDefault(s => s.Id == office.StreetId);

                    this.EmailCreator.InsertOfficeInfo(
                        receiverInformation.PersonName,
                        office.Name,
                        office.OfficeCode,
                        city == null ? String.Empty : city.Name,
                        officeNeighbourhood == null ? String.Empty : officeNeighbourhood.Name,
                        officeStreet == null ? String.Empty : officeStreet.Name,
                        office.StreetNumber == null ? String.Empty : office.StreetNumber,
                        city.PostCode.ToString(),
                        "Bulgaria");
                }
                else
                {
                    this.EmailCreator.InsertAddressInfo(
                        receiverInformation.PersonName,
                        receiverInformation.CityName,
                        receiverInformation.Neighbourhood,
                        receiverInformation.Street,
                        receiverInformation.StreetNumber,
                        receiverInformation.PostCode, "Bulgaria");

                }
                foreach (var i in userCart.Items())
                {
                    var shipmentProduct = new ShipmentProduct()
                    {
                        ShipmentId =
                            this.Data.Shipments.All()
                            .FirstOrDefault(s => s.Id == shipment.Id)
                            .Id,
                        Quantity = i.Quantity,
                        SizeId = i.SizeId,
                        ProductId = i.ProductId
                    };

                    this.Data.ShipmentProducts.Add(shipmentProduct);
                    this.Data.SaveChanges();
                }

                var shipmentProducts = this.Data.ShipmentProducts.All().Where(p => p.ShipmentId == shipment.Id).ToList();

                var pathProduct = this.Server.MapPath("~/EmailTemplates/ProductsInformation.html");

                var tempProductData = System.IO.File.ReadLines(pathProduct).ToList();
                var newData = new StringBuilder();
                newData.Append(this.EmailCreator.Html);
                var total = 0m;
                foreach (var product in shipmentProducts)
                {
                    var productData = this.Data.Products.All().FirstOrDefault(p => p.Id == product.ProductId);

                    var productPrice = productData.Price;
                    var productName = productData.Name;
                    var productQuantity = product.Quantity;

                    var subTotal = productPrice * productQuantity;
                    total += subTotal;

                    foreach (var data in tempProductData)
                    {
                        if (!data.Contains("product-container"))
                        {
                            newData.AppendLine(data);
                        }
                        else if (data.Contains("</table>"))
                        {
                            var toDestination = string.Empty;

                            if (chosenShipmentType.Equals("OFFICE"))
                            {
                                toDestination = "до офис";
                            }
                            else
                            {
                                toDestination = "до адрес";
                            }

                            var overallInfo = this.EmailCreator.InsertTotalCheckData(total, 0, toDestination);
                            newData.AppendLine(overallInfo);
                            newData.AppendLine(data);
                        }
                        else
                        {
                            newData.AppendLine(data);
                            var productInfo = this.EmailCreator.InsertProductsData(
                                productName,
                                productPrice.ToString(),
                                productQuantity.ToString(),
                                subTotal);

                            newData.AppendLine(productInfo);
                        }
                    }
                }
                
                foreach (var i in userCart.Items())
                {
                    var updateQuantity = this.Data.Quantities.All().FirstOrDefault(q => q.ProductId == i.ProductId);
                    updateQuantity.Amount -= i.Quantity;
                    this.Data.Quantities.Update(updateQuantity);
                    this.Data.SaveChanges();
                }

                userCart.Clear();
                var htmlReady = newData.ToString();

                this.SendMail(checkoutData.ClientInfo.Email, "Получена поръчка", htmlReady);
                this.SendMail("info@cacheroom.com", "Нова поръчка", String.Format(EmailContainer.EMAIL_TO_ADMINISTRATOR + "{0}", 
                    this.UserProfile == null ? receiverInformation.PersonName : this.UserProfile.NameAndFamilyName));

                this.AddNotification("Вашата поръчка е приета за обработка. Благодарим ви, че пазарувахте от нас.", NotificationType.SUCCESS);
                return this.RedirectToAction("Index", "Home", new { area = "" });
            }

            return this.View(checkoutData);
        }
        private Cart GetCart()
        {
            if (Request.IsAuthenticated)
            {
                return Cart.GetCart(User.Identity.Name);
            }

            return Cart.GetCart(this.HttpContext);

        }
        public void ChooseSize(HttpContext context)
        {
            var size = context.Request.Form["size-id"];

            this.ViewBag.Size = this.Data.Sizes.All().FirstOrDefault(s => s.Id == int.Parse(size));
        }

        public ActionResult ChangeQuantity(ChangeQuantityViewModel model)
        {
            var cartItem = this.Data.CartItems.All().FirstOrDefault(ci => ci.Id == model.CartItemId);

            var availableQuantity =
            this.Data.Quantities.All()
                .FirstOrDefault(q => q.ProductId == cartItem.ProductId && q.SizeId == cartItem.SizeId);
            if (model.CartItemQuantity == 0)
            {
                cartItem.Quantity = model.CartItemQuantity;
                this.Data.CartItems.Update(cartItem);
                this.Data.SaveChanges();

            }
            else
            {
                if (ModelState.IsValid)
                {
                    cartItem.Quantity = model.CartItemQuantity;
                    this.Data.CartItems.Update(cartItem);
                    this.Data.SaveChanges();
                    var tempOverallPrice = cartItem.Quantity * cartItem.Product.Price;
                    return this.Json(new { Ok = true, quantity = model.CartItemQuantity, tempPrice = tempOverallPrice }, JsonRequestBehavior.AllowGet);

                }
            }

            return this.Json(new { Ok = false, currentQuantity = availableQuantity.Amount }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult CalculateShipmentPrice(CheckoutViewModel checkoutData)
        {
            return this.PartialView();
        }
        public JsonResult AutoCompleteCity(string term)
        {
            var resultBulgarian =
                 this.Data.Cities.All()
                     .Where(c => c.Name.ToLower().Contains(term.ToLower()))
                     .Select(
                         c => new
                         {
                             c.Name,
                             c.PostCode
                         })
                     .Distinct()
                     .Take(10);


            return this.Json(resultBulgarian, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AutoCompleteNeighbourhood(string term)
        {
            var resultBulgarian =
                this.Data.Neighbourhoods.All()
                    .Where(n => n.Name.ToLower().Contains(term.ToLower()))
                    .Select(n => new { n.Name })
                    .Distinct()
                    .Take(10);

            var resultEnglish =
                this.Data.Neighbourhoods.All()
                    .Where(n => n.NameEn.ToLower().Contains(term.ToLower()))
                    .Select(n => new { n.NameEn })
                    .Distinct()
                    .Take(10);

            return this.Json(resultBulgarian, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AutoCompleteStreet(string term)
        {
            var resultBulgarian =
                this.Data.Streets.All()
                    .Where(s => s.Name.ToLower().Contains(term.ToLower()))
                    .Select(s => new { s.Name })
                    .Distinct()
                    .Take(10);

            var resultEnglish =
                this.Data.Neighbourhoods.All()
                    .Where(s => s.NameEn.ToLower().Contains(term.ToLower()))
                    .Select(s => new { s.NameEn })
                    .Distinct()
                    .Take(10);

            return this.Json(resultBulgarian, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOffices(GetOfficesViewModel model)
        {

            var pushedCity = int.Parse(model.PostCode);


            var officesInGivenCity =
                this.Data.Offices.All()
                    .Where(o => o.City.PostCode == pushedCity)
                    .Select(
                        c =>
                        new
                            {
                                officeId = c.Id,
                                officeName = c.Name,
                                officeCity = c.City.Name,
                                officeNeighbourhood = c.Neighbourhood.Name,
                                officeStreet = c.Street.Name,
                                officeStreetNum = c.StreetNumber ?? string.Empty,
                                officeLongitude = c.Longitude,
                                officeLatitude = c.Lattitude
                            });


            return this.Json(officesInGivenCity, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetOfficeCode(GetOfficeCodeViewModel gpsData)
        {
            var officeId = int.Parse(gpsData.OfficeId);
            var office =
                this.Data.Offices.All().FirstOrDefault(o => o.Id == officeId).OfficeCode;


            return this.Json(office, JsonRequestBehavior.AllowGet);
        }
    }
}