﻿namespace CacheRoom.Web.Areas.Store.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Data.Entity;
    using System.Globalization;
    using System.Linq;
    using System.Net;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web.Mvc;
    using System.Web.Routing;

    using CacheRoom.Data.Contracts;
    using CacheRoom.Models;
    using CacheRoom.Web.Search;
    using CacheRoom.Web.Areas.Store.Models.Product;
    using CacheRoom.Web.Controllers;


    using PagedList;

    public class ProductController : BaseController
    {
        private Dictionary<string, string> ProductTypes { get; set; }
        public ProductController(ICacheRoomData data)
            : base(data)
        {
            this.ProductTypes = new Dictionary<string, string>();
            
            var productTypesEnumValues = typeof(ProductType).GetEnumValues();
            foreach (var value in productTypesEnumValues)
            {
                var valueDisplayAttribute = typeof(ProductType).GetMember(value.ToString()).First().GetCustomAttribute<DisplayAttribute>().GetName().ToString(CultureInfo.InvariantCulture);
                this.ProductTypes.Add(valueDisplayAttribute,value.ToString());
            }

        }
        public ActionResult Index(string brand,string collection,string category,string type,string sale,string newest,string search,int? page)
        {
            // Filter the products
            var filters = new RouteValueDictionary();
            var productsQuery = this.Data.Products.All();
            if (newest != null)
            {
                filters.Add("newest", true);
                var newProducts =
                    productsQuery.Where(
                        p =>
                        p.DateTimeCreated.Year == DateTime.Now.Year && p.DateTimeCreated.Month == DateTime.Now.Month);
                if (newProducts.Any())
                {
                    productsQuery = productsQuery.OrderByDescending(p => p.DateTimeCreated);
                }
            }
            
            if (brand != null)
            {
                filters.Add("brand",brand);
                productsQuery = productsQuery.Where(p => p.Brand.Name == brand);
            }
           
            if (category != null)
            {
                filters.Add("category", category);
                productsQuery = productsQuery.Where(p => p.Category.Name == category);
                ViewBag.Category = null;
            }
           
            if (type != null)
            {
                filters.Add("type", type);

                productsQuery = productsQuery.Where(p => p.Type.ToString().Equals(type));
               
            }
            
            if (sale != null)
            {
                filters.Add("sale", sale);
                productsQuery = productsQuery.Where(p => p.IsOnSale == true);
            }
            
            if (collection != null)
            {
                filters.Add("collection", collection);
                productsQuery = productsQuery.Where(p => p.Collection.Name == collection);
            }
            
            IEnumerable<Product> products;
            if (search != null)
            {
                filters.Add("search", search);
                var searchResults = LuceneSearch.Search(search, "Name").Select(p => p.Id);
                productsQuery = productsQuery.Where(p => searchResults.Contains(p.Id));
            }
            
            products = productsQuery.OrderByDescending(product => product.Name).Include(p => p.Brand).Include(p => p.Category).Include(p=>p.Collection);

            ViewBag.FilterProductsWith = new Func<RouteValueDictionary, string, object, RouteValueDictionary>(FilterProductsWith);
            ViewBag.FilterProductsWithout = new Func<RouteValueDictionary, string, RouteValueDictionary>(FilterProductsWithout);
            
            SelectorViewModel selector = new SelectorViewModel
            {
                Brands = this.Data.Brands.All(),
                Categories = this.Data.Categories.All(),
                Collections = this.Data.Collections.All(),
                Types = Enum.GetValues(typeof(ProductType)).Cast<ProductType>(),
                Filters = filters
            };
            if (page == null)
            {
                page = 1;
            }
            ProductIndexViewModel productIndex = new ProductIndexViewModel
            {
                Selector = selector,
           
                Products = products.ToPagedList((int)page, 12)

            };
            ViewData["Brand"] = brand;
            ViewData["Category"] = category;
            ViewData["Collection"] = collection;
            ViewData["Type"] = type;
            ViewData["Sale"] = sale;
            ViewData["Newest"] = newest;
            ViewData["Search"] = search;
            ViewData["Page"] = page;
            return View(productIndex);
        }

        private List<Brand> GetBrands(string collection,string category,string type)
        {
            var brandsByCollection = this.Data.Products.All().Where(p => p.Collection.Name == collection).Select(p => p.Brand).ToList();
            var brandsByCategory = this.Data.Products.All().Where(p => p.Category.Name == category).Select(p => p.Brand).ToList();
            var brandsByType = this.Data.Products.All().Where(p => p.Type.ToString() == type).Select(p => p.Brand).ToList();

            var result = new List<Brand>();
            result.AddRange(brandsByCollection);
            result.AddRange(brandsByCategory);
            result.AddRange(brandsByType);

            return result.Distinct().OrderBy(b=>b.Name).ToList();
        }

        private IEnumerable<ProductSeenViewModel> GetSeenProducts()
        {
            var userId = string.Empty;

            if (this.User.Identity.IsAuthenticated)
            {
                userId = this.Data.Users.All().FirstOrDefault(u => u.Email == this.User.Identity.Name).Id;
            }

            var seenProducts = new List<SeenProducts>();

            if (String.IsNullOrWhiteSpace(userId))
            {
                seenProducts = this.Data.SeenProducts.All().OrderByDescending(d => d.TimeStamp).Take(5).ToList();
            }
            else
            {
                seenProducts = this.Data.SeenProducts.All().Where(p => p.UserKey.Equals(userId))
                  .OrderByDescending(d => d.TimeStamp)
                  .Take(5).ToList();  
            }
            

            var seenProductsModel = new List<ProductSeenViewModel>();
            foreach (var prod in seenProducts)
            {
                var productData = this.Data.Products.All().FirstOrDefault(p => p.Id == prod.ProductKey);

                var productImage = this.Data.ProductImages.All().FirstOrDefault(i => i.ProductId == prod.ProductKey);

                var imageLink = string.Empty;

                if (productImage != null)
                {
                    imageLink = productImage.ImagePath;
                }

                var seenData = new ProductSeenViewModel() { Product = productData, ProductImage = imageLink };
                seenProductsModel.Add(seenData);
            }

            return seenProductsModel;
        }

        private void MarkProductAsSeen(int? id)
        {
            string userId = "anonymous";

            if (this.User.Identity.IsAuthenticated)
            {
                userId = this.Data.Users.All().FirstOrDefault(u => u.Email == this.User.Identity.Name).Id;
            }



            var seenProduct = new SeenProducts() { ProductKey = id, TimeStamp = DateTime.Now, UserKey = userId };

            var checkSeenProduct =
                this.Data.SeenProducts.All()
                    .FirstOrDefault(p => p.ProductKey == seenProduct.ProductKey && p.UserKey == seenProduct.UserKey);

            if (checkSeenProduct == null)
            {
                this.Data.SeenProducts.Add(seenProduct);
                this.Data.SaveChanges();
            }
        }
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            var product = this.Data.Products.Find(id);
            if (product.ProductImages.Any())
            {
                this.ViewBag.ChosenImage = product.ProductImages.OrderBy(t=>t.TimeUploaded)
                    .FirstOrDefault()
                    .ImagePath.Trim();
            }
            else
            {
                this.ViewBag.ChosenImage = Url.Content("~/Content/images/info/NoImage.png");
            }
            if (product == null)
            {
                return HttpNotFound();
            }
            
            ProductDetailsViewModel productDetails = new ProductDetailsViewModel
            {
                Product = product,
                RelatedProducts = this.Data.Products.All()
                .Include(p => p.Brand)
                .Include(p => p.Category)
                .Where(p => p.CategoryId == product.CategoryId && p.Id != product.Id && p.Type==product.Type)
                .Select(RelatedProductViewModel.ViewModel)
                .Take(4).ToList()
            };

            productDetails.SeenProducts = this.GetSeenProducts();
            this.MarkProductAsSeen(id);

            return View(productDetails);
        }

        private RouteValueDictionary FilterProductsWith(RouteValueDictionary filter, string key, object value)
        {
            var newFilter = new RouteValueDictionary(filter);
            
            newFilter.Add(key, value);
           
            return newFilter;
        }

        private RouteValueDictionary FilterProductsWithout(RouteValueDictionary filter, string key)
        {
            var newFilter = new RouteValueDictionary(filter);
            newFilter.Remove(key);
            return newFilter;
        }

    }
}