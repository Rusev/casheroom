﻿namespace CacheRoom.Web.Areas.Store.Models.Cart
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    using CacheRoom.Models;

    public class CartIndexViewModel
    {
        
        [Key]
        public List<CartItemViewModel> CartItems { get; set; }

        public decimal CartItemsTotal { get; set; }
        public decimal CartTotal { get; set; }
    }
}