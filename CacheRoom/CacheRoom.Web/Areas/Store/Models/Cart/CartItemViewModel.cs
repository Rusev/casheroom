﻿namespace CacheRoom.Web.Areas.Store.Models.Cart
{
    using CacheRoom.Models;

    public class CartItemViewModel
    {
        public int Id { get; set; }

        public string CartId { get; set; }

        public Size ChosenSize { get; set; }

        public int Quantity { get; set; }

        public int ProductId { get; set; }

        public decimal ProductPrice { get; set; }

        public string ProductBrand { get; set; }

        public string ProductName { get; set; }

        public string ProductImagePath { get; set; }

        public decimal CartItemTotal { get; set; }

    }
}