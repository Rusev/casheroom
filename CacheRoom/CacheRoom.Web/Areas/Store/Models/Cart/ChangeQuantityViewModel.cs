﻿namespace CacheRoom.Web.Areas.Store.Models.Cart
{
    public class ChangeQuantityViewModel
    {
        public int CartItemId { get; set; }

        public int CartItemQuantity { get; set; }

        public decimal TotalSum { get; set; }
    }
}