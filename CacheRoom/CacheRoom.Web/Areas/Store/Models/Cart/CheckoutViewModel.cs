﻿namespace CacheRoom.Web.Areas.Store.Models.Cart
{
    using System.ComponentModel.DataAnnotations;

    using CacheRoom.Web.Areas.Store.Models.Econt;

    public class CheckoutViewModel
    {
        [Required]
        public ClientInfoViewModel ClientInfo { get; set; }

        [Required]
        public EcontInfoViewModel EcontInfo { get; set; }

        public ExpressCityCourierViewModel SmartCourier { get; set; }

        public CashOnDeliveryViewModel CashOnDelivery { get; set; }

        public AdditionalOptions AdditionalOptions { get; set; }

    }
}