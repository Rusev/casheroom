﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CacheRoom.Web.Areas.Store.Models.Cart
{
    using CacheRoom.Web.Areas.Store.Models.Econt;

    public class EcontInfoViewModel
    {
        public ToDoorViewModel ToAddressInformation { get; set; }

        public ToOfficeViewModel ToOfficeInformation { get; set; }
    }
}