﻿namespace CacheRoom.Web.Areas.Store.Models.Cart
{
    using ServiceStack.DataAnnotations;

    public class GetOfficeCodeViewModel
    {
        [Required]
        public string OfficeId { get; set; }

    }
}