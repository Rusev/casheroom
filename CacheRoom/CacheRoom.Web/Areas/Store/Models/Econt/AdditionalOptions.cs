﻿namespace CacheRoom.Web.Areas.Store.Models.Econt
{
    using System;
    using System.ComponentModel.DataAnnotations;

    using EcontServiceRequestBuilders.RequestModels;

    public class AdditionalOptions
    {
        [Display(Name = "Приоритет за доставка")]
        public PriorityType PriorityType { get; set; }

        [Display(Name = "Приоритетен час за доставка")]
        public TimeSpan? PriorityHour { get; set; }

        ////[Required(ErrorMessage = "Полето е задължително!")]
        //[Display(Name = "Обявена стойност")]
        //public decimal? DeclaredValue { get; set; }
    }
}