﻿namespace CacheRoom.Web.Areas.Store.Models.Econt
{
    using System.ComponentModel.DataAnnotations;

    public class CashOnDeliveryViewModel
    {
        [Display(Name = "Плащане след проверка")]
        public bool PayAfterAccept { get; set; }

        [Display(Name = "Плащане след тест")]
        public bool PayAfterTest { get; set; }

        [Display(Name = "Преглед тест и избор преди заплащане")]
        public bool PayAfterTestAndChoose { get; set; }

    }
}