﻿namespace CacheRoom.Web.Areas.Store.Models.Econt
{
    using System.ComponentModel.DataAnnotations;

    public class ClientInfoViewModel
    {
        [Required(ErrorMessage="не може да е без стойност")]
        [Display(Name="Име и Фамилия")]
        public string NameAndFamilyName { get; set; }

        [Required(ErrorMessage = "не е валиден email адрес")]
        [Display(Name="Имейл")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "не може да бъде без стойност")]
        [Display(Name="Телефон")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        public string ShipmentType { get; set; }
    }
}