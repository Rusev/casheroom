﻿namespace CacheRoom.Web.Areas.Store.Models.Econt
{
    using System.ComponentModel.DataAnnotations;

    public class ToDoorViewModel
    {
        
        [Required(ErrorMessage = "не може да e без стойност")]
        [Display(Name = "Населено място*")]
        public string LivingPlace { get; set; }

        [Display(Name = "Квартал")]
        public string Neighbourhood { get; set; }

        [Required(ErrorMessage = "не е попълнено")]
        [Display(Name = "Улица*")]
        public string Street { get; set; }

        [Required(ErrorMessage = "не е попълнено")]
        [Display(Name = "Номер на улица*")]
        public string StreetNumber { get; set; }

        [Display(Name = "Адрес")]
        [RegularExpression("([бл. ]+[0-9]+)+[, ]+([вх. A-Яа-я0-9]+)+[, ]+([ап. ]+[0-9]+)", ErrorMessage = "форматът на данните за адреса трябва да бъде следният: бл. 111, вх. А, ап. 11")]
        public string Address { get; set; }

        [Display(Name = "Уточнения по адреса")]
        public string AdditionalInfo { get; set; }
    }
}