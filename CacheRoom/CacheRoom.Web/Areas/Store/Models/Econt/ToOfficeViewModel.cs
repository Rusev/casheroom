﻿namespace CacheRoom.Web.Areas.Store.Models.Econt
{
    using System.ComponentModel.DataAnnotations;

    public class ToOfficeViewModel
    {
        [Required(ErrorMessage = "не може да e без стойност")]
        [Display(Name = "Населено място*")]
        public string LivingPlace { get; set; }

        [Required(ErrorMessage = "не може да е без стойност")]
        [Display(Name = "Офис")]
        public string Office { get; set; }

        public string OfficeCode { get; set; }
    }
}