﻿namespace CacheRoom.Web.Areas.Store.Models.Product
{
    using CacheRoom.Models;
    public class ProductAddedViewModel
    {
        public int ProductId { get; set; }

        public int? ChosenSizeId { get; set; }
    }
}