﻿namespace CacheRoom.Web.Areas.Store.Models.Product
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    using CacheRoom.Models;
    public class ProductDetailsViewModel
    {
        [Required]
        public Product Product { get; set; }

        [Required]
        public IEnumerable<RelatedProductViewModel> RelatedProducts { get; set; }

        public int ChosenSize { get; set; }

        public IEnumerable<ProductSeenViewModel> SeenProducts { get; set; }
    }
}