﻿namespace CacheRoom.Web.Areas.Store.Models.Product
{
    using System.ComponentModel.DataAnnotations;
    using CacheRoom.Models;
    using PagedList;
    public class ProductIndexViewModel
    {
        [Required]
        public SelectorViewModel Selector { get; set; }

        [Required]
        public IPagedList<Product> Products { get; set; }

    }
}