﻿namespace CacheRoom.Web.Areas.Store.Models.Product
{
    using CacheRoom.Models;

    public class ProductSeenViewModel
    {
        public Product Product { get; set; }

        public string ProductImage { get; set; }
    }
}