﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CacheRoom.Web.Areas.Store.Models.Product
{
    using System.Linq.Expressions;

    using CacheRoom.Models;
    using CacheRoom.Web.Areas.Admin.Models;

    public class RelatedProductViewModel
    {
        public int Id { get; set; }

        public decimal Price { get; set; }

        public string BrandName { get; set; }

        public string Name { get; set; }

        public string ImagePath { get; set; }

        public static Expression<Func<Product, RelatedProductViewModel>> ViewModel
        {
            get
            {
                return
                    p =>
                    new RelatedProductViewModel()
                    {
                       Id = p.Id,
                       BrandName = p.Brand.Name,
                       Name = p.Name,
                       Price = p.Price,
                       ImagePath = p.ProductImages.Any() ? p.ProductImages.FirstOrDefault().ImagePath : string.Empty
                    };
            }
        }
    }
}