﻿namespace CacheRoom.Web.Controllers
{
    using System.Web.Mvc;

    using CacheRoom.Data.Contracts;

    public class AboutController : BaseController
    {
        public AboutController(ICacheRoomData data)
            : base(data)
        {
            
        }

        public ActionResult AboutUs()
        {
            return this.View();
        }

        public ActionResult TermsAndConditions()
        {
            return this.View();
        }

        public ActionResult Shipment()
        {
            return this.View();
        }
    }
}