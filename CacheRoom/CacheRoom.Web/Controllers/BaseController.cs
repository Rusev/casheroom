﻿namespace CacheRoom.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Drawing.Imaging;
    using System.Web.Mvc;
    using CacheRoom.Models;
    using CacheRoom.Data;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Web.Routing;

    using CacheRoom.Data.Contracts;
    using CacheRoom.Web.Areas.Store.Models.Product;
    using CacheRoom.Web.Infrastructure.Cart;
    using CacheRoom.Web.Search;

    using PagedList;

    public abstract class BaseController:Controller
    {
        protected const string ParcelImportUrl = "http://e-econt.econt.com/xml_parcel_import2.php";

        protected const string ServicesToolUrl = "http://e-econt.econt.com/xml_service_tool.php";

        private ICacheRoomData data;

        private User userProfile;

        protected ICacheRoomData Data { get; private set; }

        protected User UserProfile { get; private set; }

        protected BaseController(ICacheRoomData data)
        {
            this.Data = data;
        }
        protected BaseController(ICacheRoomData data, User userProfile)
            : this(data)
        {
            this.UserProfile = userProfile;
        }

        protected override IAsyncResult BeginExecute(
            System.Web.Routing.RequestContext requestContext,
            AsyncCallback callback,
            object state)
        {
            if (requestContext.HttpContext.User.Identity.IsAuthenticated)
            {
                var username = requestContext.HttpContext.User.Identity.Name;
                var user = this.Data.Users.All().FirstOrDefault(x => x.UserName == username);

                this.UserProfile = user;
            }

            return base.BeginExecute(requestContext, callback, state);
        }
        protected void SendMail(string receiverEmail,string mailSubject,string mailBody)
        {
            SmtpClient smtpClient = new SmtpClient();

            smtpClient.UseDefaultCredentials = true;
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.EnableSsl = false;
            smtpClient.Host = "mail.cacheroom.com";
            smtpClient.Port = 25;
            smtpClient.Credentials = new NetworkCredential("info@cacheroom.com", "C123456789r1");
            MailMessage mail = new MailMessage();
            mail.Subject = mailSubject;
            mail.Body = mailBody;
            mail.IsBodyHtml = true;

            //Setting From , To and CC
            mail.From = new MailAddress("info@cacheroom.com", "cacheroom");
            mail.To.Add(new MailAddress(receiverEmail));
            
            smtpClient.Send(mail);
        }

        protected void DeletePhysicalFiles(string fileName)
        {
            System.IO.File.Delete(Request.PhysicalApplicationPath + fileName);
        }

        protected Image ResizeImage(Image imgPhoto, int Width, int Height)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;

            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)Width / (float)sourceWidth);
            nPercentH = ((float)Height / (float)sourceHeight);

            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = Convert.ToInt16((Width - (sourceWidth * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = Convert.ToInt16((Height - (sourceHeight * nPercent)) / 2);
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bmPhoto = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.Clear(Color.White);
            grPhoto.CompositingQuality = CompositingQuality.HighQuality;
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();

            return bmPhoto;
        }

        protected void OpenPdf(string url)
        {
            WebClient client = new WebClient();
            Byte[] buffer = client.DownloadData(url);

            if (buffer != null)
            {
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-length", buffer.Length.ToString());
                Response.BinaryWrite(buffer);
                Response.End();
            }
        }
    }
}