﻿namespace CacheRoom.Web.Controllers
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Dynamic;
    using System.Web.Mvc;
    using System.Web.Routing;

    using CacheRoom.Data.Contracts;
    using CacheRoom.Models;
    using CacheRoom.Web.Areas.Admin.Models.HomePage;
    using CacheRoom.Web.Models;

    public class HomeController : BaseController
    {
        public HomeController(ICacheRoomData data)
            : base(data)
        {

        }

        public ActionResult Index()
        {
            var carouselImages = this.Data.CarouselImages.All().Select(CarouselImageViewModel.ViewModel).Take(5).ToList();
            var indexBrandImages = this.Data.MostPopularBrandsImages.All();
            var indexCollectionImages = this.Data.MostPopularCollectionsImages.All();
            var brandsImages = new List<BrandImagesViewModel>();
            var collectionImages = new List<CollectionImagesViewModel>();

            if(indexBrandImages.Any())
            {
                var imageLeftTop =
                    indexBrandImages
                   .Where(i => i.Position.ToString().Equals(BrandImagePosition.leftTop.ToString()) && i.IsVisible)
                   .Select(BrandImagesViewModel.ViewModel).ToList().FirstOrDefault();

                var imageLeftBottom =
                       indexBrandImages
                       .Where(i => i.Position.ToString().Equals(BrandImagePosition.leftBottom.ToString()) && i.IsVisible)
                       .Select(BrandImagesViewModel.ViewModel).ToList().FirstOrDefault();

                var imageRightTop =
                       indexBrandImages
                       .Where(i => i.Position.ToString().Equals(BrandImagePosition.rightTop.ToString()) && i.IsVisible)
                       .Select(BrandImagesViewModel.ViewModel).ToList().FirstOrDefault();

                var imageRightMiddle =
                       indexBrandImages
                       .Where(i => i.Position.ToString().Equals(BrandImagePosition.rightMiddle.ToString()) && i.IsVisible)
                       .Select(BrandImagesViewModel.ViewModel).ToList().FirstOrDefault();

                var imageRightBottom =
                       indexBrandImages
                       .Where(i => i.Position.ToString().Equals(BrandImagePosition.rightBottom.ToString()) && i.IsVisible)
                       .Select(BrandImagesViewModel.ViewModel).ToList().FirstOrDefault();

                brandsImages.Add(imageLeftTop);
                brandsImages.Add(imageLeftBottom);
                brandsImages.Add(imageRightTop);
                brandsImages.Add(imageRightMiddle);
                brandsImages.Add(imageRightBottom);
            }
            brandsImages.RemoveAll(i => i == null);

            if (indexCollectionImages.Any())
            {
                var imagesFirst =
                    indexCollectionImages.Where(
                        i => i.Position.ToString().Equals(CollectionImagePosition.first.ToString()) && i.IsVisible)
                        .Select(CollectionImagesViewModel.ViewModel)
                        .ToList()
                        .FirstOrDefault();
                var imagesSecond =
                    indexCollectionImages.Where(
                        i => i.Position.ToString().Equals(CollectionImagePosition.second.ToString()) && i.IsVisible)
                        .Select(CollectionImagesViewModel.ViewModel)
                        .ToList()
                        .FirstOrDefault();
                var imagesThird =
                    indexCollectionImages.Where(
                        i => i.Position.ToString().Equals(CollectionImagePosition.third.ToString()) && i.IsVisible)
                        .Select(CollectionImagesViewModel.ViewModel)
                        .ToList()
                        .FirstOrDefault();

                collectionImages.Add(imagesFirst);
                collectionImages.Add(imagesSecond);
                collectionImages.Add(imagesThird);

            }
            collectionImages.RemoveAll(i => i == null);

            var brandRightTop = string.Empty;
            var brandRightMiddle = string.Empty;
            var brandRightBottom = string.Empty;

            var brandLeftTop = string.Empty;
            var brandLeftBottom = string.Empty;

            var collectionLeft = string.Empty;
            var collectionMiddle = string.Empty;
            var collectionRight = string.Empty;

            if (carouselImages.Any())
            {
                carouselImages[0].IsActiveInitial = true;
            }

             if (brandsImages.Any(b => b.Position.ToString() == BrandImagePosition.rightTop.ToString()))
            {
                brandRightTop =
                    brandsImages.FirstOrDefault(b => b.Position.ToString() == BrandImagePosition.rightTop.ToString()).ActionLink;
            }

            if (brandsImages.Any(b => b.Position == BrandImagePosition.rightMiddle))
            {
                brandRightMiddle =
                    brandsImages.FirstOrDefault(b => b.Position == BrandImagePosition.rightMiddle).ActionLink;
            }

            if (brandsImages.Any(b => b.Position == BrandImagePosition.rightBottom))
            {
                brandRightBottom =
                    brandsImages.FirstOrDefault(b => b.Position == BrandImagePosition.rightBottom).ActionLink;
            }

            if (brandsImages.Any(b => b.Position == BrandImagePosition.leftTop))
            {
                brandLeftTop =
                    brandsImages.FirstOrDefault(b => b.Position == BrandImagePosition.leftTop).ActionLink;
            }

            if (brandsImages.Any(b => b.Position == BrandImagePosition.leftTop))
            {
                brandLeftBottom =
                    brandsImages.FirstOrDefault(b => b.Position == BrandImagePosition.leftBottom).ActionLink;
            }

            if (collectionImages.Any(c => c.Position == CollectionImagePosition.first))
            {
                collectionLeft =
                    collectionImages.FirstOrDefault(c => c.Position == CollectionImagePosition.first).ActionLink;
            }
            if (collectionImages.Any(c => c.Position == CollectionImagePosition.second))
            {
                collectionMiddle =
                    collectionImages.FirstOrDefault(c => c.Position == CollectionImagePosition.second).ActionLink;
            }
            if (collectionImages.Any(c => c.Position == CollectionImagePosition.third))
            {
                collectionRight =
                    collectionImages.FirstOrDefault(c => c.Position == CollectionImagePosition.third).ActionLink;
            }

            this.ViewBag.BrandRightTop = brandRightTop;
            this.ViewBag.BrandRightMiddle = brandRightMiddle;
            this.ViewBag.BrandRightBottom = brandRightBottom;

            this.ViewBag.BrandLeftTop = brandLeftTop;
            this.ViewBag.BrandLeftBottom = brandLeftBottom;
            this.ViewBag.CollectionFirst = collectionLeft;
            this.ViewBag.CollectionSecond = collectionMiddle;
            this.ViewBag.CollectionThird = collectionRight;

            var homeImages = new HomePageImagesViewModel()
            {
                CarouselImages = carouselImages,
                BrandsImages = brandsImages,
                CollectionImages = collectionImages
            };
            
            return this.View(homeImages);
        }
    }
}