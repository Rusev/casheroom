﻿namespace CacheRoom.Web.Extensions
{
    using System;
    using System.Web.Mvc;
    using System.Linq.Expressions;
    using System.Reflection;

    public static class ModelStateDictionaryExtensions
    {
        
        public static void Remove<TViewModel>(
            this ModelStateDictionary me,
            Expression<Func<TViewModel, object>> lambdaExpression)
        {
            me.Remove(GetPropertyName(lambdaExpression));
        }

        private static string GetPropertyName(this Expression lambdaExpression)
        {
            var e = lambdaExpression;

            while (true)
            {
                switch (e.NodeType)
                {
                    case ExpressionType.Lambda:
                        e = ((LambdaExpression)e).Body;
                        break;

                    case ExpressionType.MemberAccess:
                        var propertyInfo =
                            ((MemberExpression)e).Member
                            as PropertyInfo;

                        return propertyInfo != null
                                   ? propertyInfo.Name
                                   : null;

                    case ExpressionType.Convert:
                        e = ((UnaryExpression)e).Operand;
                        break;

                    default:
                        return null;
                }
            }
        }
    }
}