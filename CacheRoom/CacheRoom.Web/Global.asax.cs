﻿using FluentScheduler;
using FluentScheduler.Model;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using ServiceStack.Logging;
namespace CacheRoom.Web
{
    using System;

    using CacheRoom.Web.Infrastructure;

    using Owin;

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            TaskManager.UnobservedTaskException += TaskManager_UnobservedTaskException;
            TaskManager.Initialize(new EcontTaskRegistry());
        }

        protected void Application_End(object sender, EventArgs e)
        {
            TaskManager.Stop();
        }

        static void TaskManager_UnobservedTaskException(TaskExceptionInformation sender, UnhandledExceptionEventArgs e)
        {
            var log = LogManager.GetLogger(typeof(MvcApplication));
            log.Fatal("An error happened with a scheduled task: " + sender.Name + "\n" + e.ExceptionObject);
        }
    }
}
