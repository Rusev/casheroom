﻿namespace CacheRoom.Web.Hubs
{
    using System.Linq.Dynamic;

    using CacheRoom.Data;
    using System.Linq;

    using CacheRoom.Web.Controllers;

    using Microsoft.AspNet.SignalR;

    public class ShipmentsHub : Hub
    {
        public void Send()
        {
            var data = new CacheRoomData(new CacheRoomDbContext());

            var shipmentsCount = data.Shipments.All().Count(s => s.IsSended == false);

            Clients.All.broadcastMessage(shipmentsCount);
        }
    }
}