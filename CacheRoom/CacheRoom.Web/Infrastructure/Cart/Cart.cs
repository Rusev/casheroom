﻿namespace CacheRoom.Web.Infrastructure.Cart
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using CacheRoom.Data;
    using CacheRoom.Data.Contracts;
    using CacheRoom.Models;
    public class Cart
    {
        private const string SessionKey = "CartId";
        private string CartId { get; set; }

        private ICacheRoomData Data = new CacheRoomData(new CacheRoomDbContext());
        public static Cart GetCart(HttpContextBase context)
        {
            var cart = new Cart();
            cart.CartId = cart.GetCartId(context);
            return cart;
        }

        public static Cart GetCart(string userName)
        {
            var cart = new Cart();
            cart.CartId = userName;
            return cart;
        }

        public int Add(Product item, int chosenSize)
        {
            var cartItem = this.Data.CartItems.All().SingleOrDefault(
                c => c.CartId == CartId && c.ProductId == item.Id && c.SizeId == chosenSize);

            if (cartItem == null)
            {
                cartItem = new CartItem
                {
                    ProductId = item.Id,
                    CartId = CartId,
                    Quantity = 1,
                    DateCreated = DateTime.Now,
                    SizeId = chosenSize
                };
                this.Data.CartItems.Add(cartItem);
            }
            else
            {
                cartItem.Quantity++;
            }


            this.Data.SaveChanges();

            return cartItem.Quantity;
        }

        public int Remove(int id,int chosenSize)
        {
            var cartItem = Data.CartItems.All().Single(
                cart => cart.CartId == this.CartId
                && cart.ProductId == id && cart.SizeId==chosenSize);

            int itemCount = 0;

            if (cartItem != null)
            {
                if (cartItem.Quantity > 1)
                {
                    cartItem.Quantity--;
                    itemCount = cartItem.Quantity;
                }
                else
                {
                    this.Data.CartItems.Delete(cartItem);
                }

                this.Data.SaveChanges();
            }
            return itemCount;
        }

        public void Clear()
        {
            var cartItems = this.Data.CartItems.All().Where(
                cart => cart.CartId == CartId);

            foreach (var cartItem in cartItems)
            {
                this.Data.CartItems.Delete(cartItem);
            }

            this.Data.SaveChanges();
        }

        public List<CartItem> Items()
        {
            return this.Data.CartItems.All().Where(
                cart => cart.CartId == CartId).ToList();
        }

        public int ItemsCount()
        {
            int? count = (from cartItems in this.Data.CartItems.All()
                          where cartItems.CartId == CartId
                          select (int?)cartItems.Quantity).Sum();

            return count ?? 0;
        }

        public void Migrate(string userName,string currentCartId)
        {
            var shoppingCart = this.Data.CartItems.All().Where(
                c => c.CartId == currentCartId);

            foreach (var item in shoppingCart)
            {
                item.CartId = userName;
            }
            this.Data.SaveChanges();
        }

        private string GetCartId(HttpContextBase context)
        {
            if (context.Session[SessionKey] == null)
            {
                if (!string.IsNullOrWhiteSpace(context.User.Identity.Name))
                {
                    context.Session[SessionKey] =
                        context.User.Identity.Name;
                }
                else
                {
                    Guid tempCartId = Guid.NewGuid();
                    context.Session[SessionKey] = tempCartId.ToString();
                }
            }
            return context.Session[SessionKey].ToString();
        }

    }

}