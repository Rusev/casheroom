﻿namespace CacheRoom.Web.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using FluentScheduler;
    using System.Web.Hosting;
    using CacheRoom.Web.Infrastructure.Econt;
    public class EcontTaskRegistry : Registry
    {
        public EcontTaskRegistry()
        {
            //Schedule<InitialDataTask>()
            //    .ToRunNow();

            Schedule<UpdateDataTask>()
                .ToRunEvery(1)
                .Weeks()
                .On(DayOfWeek.Tuesday)
                .At(3, 35);

        }
    }
}