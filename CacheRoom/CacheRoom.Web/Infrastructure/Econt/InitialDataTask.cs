﻿namespace CacheRoom.Web.Infrastructure.Econt
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Hosting;

    using CacheRoom.Data;

    using EcontServiceRequestBuilders;
    using FluentScheduler;
    using CacheRoom.Models;
    using CacheRoom.EcontServiceXmlWorkers;
    using CacheRoom.Web.Controllers;
    public class InitialDataTask : ITask, IRegisteredObject
    {
        private readonly object _lock = new object();
        private bool _shuttingDown;

        public InitialDataTask()
        {
            HostingEnvironment.RegisterObject(this);
        }

        public void Execute()
        {
            lock (_lock)
            {
                if (_shuttingDown)
                {
                    return;
                }
                var econtLiveServiceUrl = "http://e-econt.econt.com/xml_service_tool.php";

                //Downloading the necessary data for syncronization with the ECONT System

                XmlRequester downloader = new XmlRequester();
                var requestBuilder = new EcontRequestBuilder();
                var client = requestBuilder.CreateClientPart("МАСОВКА", "40865");

                var offices = downloader.InitiateRequest(econtLiveServiceUrl, requestBuilder.CreateDataRequest(client, RequestType.offices));


                var zones = downloader.InitiateRequest(econtLiveServiceUrl, requestBuilder.CreateDataRequest(client, RequestType.cities_zones));


                var cities = downloader.InitiateRequest(econtLiveServiceUrl, requestBuilder.CreateDataRequest(client, RequestType.cities));


                var streets = downloader.InitiateRequest(econtLiveServiceUrl, requestBuilder.CreateDataRequest(client, RequestType.cities_streets));


                var quarters = downloader.InitiateRequest(econtLiveServiceUrl, requestBuilder.CreateDataRequest(client, RequestType.cities_quarters));


                var countries = downloader.InitiateRequest(econtLiveServiceUrl, requestBuilder.CreateDataRequest(client, RequestType.countries));


                var regions = downloader.InitiateRequest(econtLiveServiceUrl, requestBuilder.CreateDataRequest(client, RequestType.cities_regions));

                ////Initiate Importing of the Data between ECONT Server and Our Software.

                var db = new CacheRoomDbContext();

                db.Database.CreateIfNotExists();

                XmlImporter dbImporter = new XmlImporter();


                dbImporter.InitiateZonesImport(zones);
                
                dbImporter.InitiateCountriesImport(countries);
                
                dbImporter.InitiateCitiesImport(cities);
                
                dbImporter.InitiateNeigbourhoodsImport(quarters);
                
                dbImporter.InitiateStreetsImport(streets);
               
                dbImporter.InitiateOfficesImport(offices);
                
            }
        }

        public void Stop(bool immediate)
        {
            // Locking here will wait for the lock in Execute to be released until this code can continue.
            lock (_lock)
            {
                _shuttingDown = true;
            }
            HostingEnvironment.UnregisterObject(this);
        }

    }
}