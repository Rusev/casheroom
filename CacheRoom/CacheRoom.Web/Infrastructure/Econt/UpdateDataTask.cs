﻿namespace CacheRoom.Web.Infrastructure.Econt
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FluentScheduler;
    using System.Web;
    using System.Web.Hosting;
    using EntityFramework.Audit;
    using EcontServiceRequestBuilders;
    using EcontServiceXmlWorkers;
    public class UpdateDataTask : ITask,IRegisteredObject
    {
        private readonly object _lock = new object();
        private bool _shuttingDown;

        public UpdateDataTask()
        {
            HostingEnvironment.RegisterObject(this);
        }
        public void Execute()
        {
            lock (_lock)
            {
                if (_shuttingDown)
                {
                    return;
                }
                
                var auditConfiguration = AuditConfiguration.Default;
                auditConfiguration.IncludeRelationships = true;
                auditConfiguration.LoadRelationships = true;
                auditConfiguration.DefaultAuditable = true;

                var updater = new XmlUpdater();
                var requestBuilder = new EcontRequestBuilder();

                var updates = updater.DownloadNewData(requestBuilder.CreateClientPart("itpartner", "itpartner"));

                var zonesReport = updater.InitiateCitiesUpdate(updates);
                var countriesReport = updater.InitiateCountriesUpdate(updates);
                var citiesReport = updater.InitiateCitiesUpdate(updates);
                var neighboursReport = updater.InitiateNeighbourhoodsUpdate(updates);
                var streetsReport = updater.InitiateStreetsUpdate(updates);
                var officesReport = updater.InitiateOfficesUpdate(updates);
            }
                        
        }

        public void Stop(bool immediate)
        {
            // Locking here will wait for the lock in Execute to be released until this code can continue.
            lock (_lock)
            {
                _shuttingDown = true;
            }
            HostingEnvironment.UnregisterObject(this);
        }

    }

}