﻿namespace CacheRoom.Web.Infrastructure.Email
{
    using System;
    using System.Text;
    public static class EmailContainer
    {
        public const string EMAIL_TO_ADMINISTRATOR = "Получена е нова поръчка, от клиент с потребителско име - ";

        public const string EMAIL_TO_USER_FOR_RECIEVED_ORDER =
            "Вашата поръчка е приета за обработка. Очаквайте с вас да се свърже оператор за потвърждение на поръчката.";

        public const string EMAIL_TO_USER_READY_SHIPMENT_NUMBER_FOR_TRACKING =
            "Вашата поръчка е обработена и изпратена към куриер. Можете да проследите статуса на поръчката си от линка по долу.";
    }
}