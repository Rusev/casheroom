﻿namespace CacheRoom.Web.Infrastructure.Email
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Web;
    
    public class EmailCreator
    {
        private string html;

        public string Html
        {
            get
            {
                return this.html;
            }
            set
            {
                this.html = value;
            }
        }

        public EmailCreator(string html)
        {
            this.Html = html;
        }
        
        public void InsertShipmentDetails(string shipmentNumber, string shipmentType)
        {

            var html1 = this.Html.Replace("#shipmentNumber#", shipmentNumber);
            var html2 = html1.Replace("#Date#", DateTime.Now.ToString(CultureInfo.InvariantCulture));
            var html3 = html2.Replace("#paymentType#", "Екон Експрес - наложен платеж");
            var html4 = html3.Replace("#shipmentType#", shipmentType);

            this.Html = html4;
        }

        public void InsertContacts(string phoneNumber, string email)
        {
            var ip = this.GetClientIpAddress();

            var html1 = this.Html.Replace("#email#", email);
            var html2 = html1.Replace("#phone#", phoneNumber);
            var html3 = html2.Replace("#ip#", ip);

            this.Html = html3;
        }

        public void InsertAddressInfo(string clientName, string addressCity,string addressNeighbourhood, string addressStreet,string streetNumber, string postCode, string country)
        {
            var addressData = String.Format(
                "До адрес: {0}, кв. {1}, {2}, № {3}",
                addressCity,
                addressNeighbourhood,
                addressStreet,
                streetNumber);

            var html1 = this.Html.Replace("#clientName#", clientName);
            var html2 = html1.Replace("#shipToAddress#", String.Format("До адрес: {0}",addressData));
            var html3 = html2.Replace("#shipToPostCode#", String.Format("{0} - {1}",addressCity,postCode));
            var html4 = html3.Replace("#shipToCountry#", country);

            this.Html = html4;
        }

        public void InsertOfficeInfo(string clientName,string officeName, string officeCode, string officeCity, string officeNeighbourhood, string officeStreet, string streetNumber,string postCode,string country)
        {
            var officeAddress = String.Format(
                "{0}, кв. {1}, {2} № {3}",
                officeCity,
                officeNeighbourhood,
                officeStreet,
                streetNumber);

            var officeData = String.Format("До офис: {0}, {1}, {2}", officeCode, officeName, officeAddress);

            var html1 = this.Html.Replace("#clientName#", clientName);
            var html2 = html1.Replace("#shipToAddress#", officeData);
            var html3 = html2.Replace("#shipToPostCode#", String.Format("{0} - {1}", officeCity, postCode));
            var html4 = html3.Replace("#shipToCountry#", country);

            this.Html = html4;
        }


        public string InsertProductsData(string productName, string productPrice, string productQuantity,decimal subTotal)
        {
            var nameElement = String.Format("<td style=\"border:solid 1px;border-right:0px;\">{0}</td>", productName);
            var priceElement = String.Format("<td style=\"border:solid 1px;border-right:0px;\">{0}</td>", productPrice);
            var modelElement = String.Format("<td style=\"border:solid 1px;border-right:0px;\">{0}</td>", productName);
            var quantityElement = String.Format("<td style=\"border:solid 1px;border-right:0px;\">{0}</td>", productQuantity);
            var overallPrice = String.Format("<td style=\"border:solid 1px;border-right:0px;\">{0}</td>", subTotal);

            return String.Format(
                "{0}\n{1}\n{2}\n{3}\n{4}\n",
                nameElement,
                modelElement,
                quantityElement,
                priceElement,
                overallPrice);
        }

        public string InsertTotalCheckData(decimal total, decimal shipmentAmount,string to)
        {
            var overallSum = total + shipmentAmount;

            var totalDds = String.Format("<tr><td style=\"border: solid 1px; border-right: 0px;text-weight:bold;text-align:right\">С ДДС:</td><td style=\"border: solid 1px;\">{0}</td></tr>", total);
            var shipmentPrice = String.Format("<tr><td style=\"border: solid 1px; border-right: 0px;text-weight:bold;text-align:right\">Еконт Експрес - {0}:</td><td style=\"border: solid 1px;\">{1}</td></tr>",to, shipmentAmount);
            var overall = String.Format("<tr><td style=\"border: solid 1px; border-right: 0px;text-weight:bold;text-align:right\">Общо:</td><td style=\"border: solid 1px;\">{0}</td></tr>",Math.Round(overallSum,2));

            return String.Format("{0}\n{1}\n{2}\n", totalDds, shipmentPrice, overall);
        }
        public string GetData()
        {
            return this.Html;
        }
        public string GetClientIpAddress()
        {
            var clientIp = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (string.IsNullOrEmpty(clientIp))
            {
                clientIp = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }

            return clientIp;
        }
    }
}