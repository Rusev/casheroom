﻿namespace CacheRoom.Web.Models.Account
{
    using System.ComponentModel.DataAnnotations;

    public class LoginViewModel
    {

        [Required(ErrorMessage = "Името и фамилията ви са задължителни")]
        [Display(Name = "Имейл Адрес")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "не е валиден имейл адрес")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Паролата е задължителна")]
        [DataType(DataType.Password)]
        [Display(Name = "Парола")]
        public string Password { get; set; }
        
        [Display(Name = "Запомни ме?")]
        public bool RememberMe { get; set; }
    }
}