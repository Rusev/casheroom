﻿namespace CacheRoom.Web.Models.Account
{
    using System.ComponentModel.DataAnnotations;

    public class ManageViewModel
    {
        [Required(ErrorMessage = "Въведете вашата текуща парола")]
        [DataType(DataType.Password)]
        [Display(Name = "Текуща парола")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Въведете вашата нова парола")]
        [StringLength(100, ErrorMessage = "Паролата {0} трябва да бъде поне {2} символа(букви и цифри).", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Нова парола")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Потвърди новата парола")]
        [Compare("NewPassword", ErrorMessage = "Няма съвпадение с въведената парола.")]
        public string ConfirmPassword { get; set; }
    }
}