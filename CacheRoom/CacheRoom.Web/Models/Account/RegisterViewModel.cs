﻿namespace CacheRoom.Web.Models.Account
{
    using System.ComponentModel.DataAnnotations;

    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Името и фамилията ви са задължителни")]
        [Display(Name = "Име и фамилия")]
        public string NameAndFamilyName { get; set; }

        [Required(ErrorMessage = "Имейл адресът е задължителен")]
        [Display(Name = "Имейл Адрес")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "не е валиден имейл адрес")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Телефоният номер е задължителен")]
        [DataType(DataType.PhoneNumber)]
        [Phone(ErrorMessage = "не е валиден телефонен номер")]
        [Display(Name = "Телефонен Номер")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Паролата е задължителна")]
        [StringLength(100, ErrorMessage = "Паролата {0} трябва да бъде поне {2} символа(букви и цифри).", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Парола")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Потвърждение на паролата")]
        [Compare("Password", ErrorMessage = "Няма съвпадение с въведената парола.")]
        public string ConfirmPassword { get; set; }
    }
}