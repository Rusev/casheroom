﻿namespace CacheRoom.Web.Models
{
    public class AddressViewModel
    {
        public string Block { get; set; }

        public string Entrance { get; set; }

        public string Floor { get; set; }

        public string Apartment { get; set; }
    }
}