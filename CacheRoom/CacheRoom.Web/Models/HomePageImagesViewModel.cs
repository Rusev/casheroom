﻿namespace CacheRoom.Web.Models
{
    using System.Collections.Generic;

    using CacheRoom.Models;
    using CacheRoom.Web.Areas.Admin.Models.HomePage;

    public class HomePageImagesViewModel
    {
        public ICollection<CarouselImageViewModel> CarouselImages { get; set; }

        public ICollection<BrandImagesViewModel> BrandsImages { get; set; }

        public ICollection<CollectionImagesViewModel> CollectionImages { get; set; }

        public BrandImagePosition BrandPosition { get; set; }

        public int BrandId { get; set; }

        public CollectionImagePosition CollectionPosition { get; set; }

        public int CollectionId { get; set; }

    }
}