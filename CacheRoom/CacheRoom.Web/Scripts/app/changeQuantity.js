﻿var changeTimer = false;
$('.quantity-dynamic').keyup(function (e) {
    e.preventDefault();
    var itemIdData = $(e.currentTarget).attr("id");
    var splittedItemIdData = itemIdData.split("-");
    var interactedItemId = splittedItemIdData[1];

    var tempData = $('#inputSmall-' + interactedItemId).val();
    if (tempData != null && tempData >= 1) {
        $("#quantity-change").removeClass("has-error");
        $('#inputSmall-' + interactedItemId).popover('hide');
    }
    else {
        $("#quantity-change").addClass("has-error");
    }
    $('inputSmall-' + interactedItemId).val(tempData);
    $('#tempQuantity').val(tempData);
    var jsonObject = { "CartItemId": interactedItemId, "CartItemQuantity": tempData };

    if (changeTimer !== false) clearTimeout(changeTimer);
    changeTimer = setTimeout(function () {
        $.ajax({
            type: "POST",
            url: 'Cart/ChangeQuantity',
            data: JSON.stringify(jsonObject),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async:true,
            success: function (data) {
                if (data.Ok) {
                    $('#subtotalCol-' + interactedItemId).text(data.tempPrice + " лв");

                    $('#inputSmall-' + interactedItemId).val(data.quantity);

                    var quantitySum = 0;
                    $('.quantity-dynamic').each(function () {
                        quantitySum += parseInt(this.value);
                    });
                    $('#productsCount').text(quantitySum + " продукт/a");

                    var totalSum = 0;
                    var sumValues = $('td.subTotal').map(function () {
                        return $(this).text().replace(" лв", "");
                    }).get();
                    for (var i = 0; i < sumValues.length; i++) {
                        totalSum += parseFloat(sumValues[i]);
                    }
                    $('#sum-total').text("Обща сума: " + totalSum + " лв");
                }
                else {
                    $('#inputSmall-' + interactedItemId).attr('data-original-title', 'Warning!').attr('data-content', 'Наличното количество от избраният от вас продукт и размер е ' + data.currentQuantity + ' бр.');
                    $('#inputSmall-' + interactedItemId).popover('show');
                }
            }
        });
        changeTimer = false;
    }, 500);


});