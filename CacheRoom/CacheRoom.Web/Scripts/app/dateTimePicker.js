﻿$(function () {
    $('#ShipmentSetup_SendDate').datetimepicker(
    {
        format: 'DD-MMM-YYYY',
        sideBySide: true,
        showTodayButton: true
    });
    $('#ShipmentSetup_DeliveryDay').datetimepicker(
    {
        format: 'DD-MMM-YYYY',
        sideBySide: true,
        showTodayButton: true
    });
    $('#AdditionalOptions_PriorityHour').datetimepicker({
        format: 'HH:mm'
    });
});