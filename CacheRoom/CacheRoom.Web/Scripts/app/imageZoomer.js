﻿$(document).ready(function () {
    var native_width = 0;
    var native_height = 0;
    $("#large").fadeOut(0);
    $("#magnify").mouseleave(function() {
        $("#large").fadeOut(0);
    });

    $("#magnify").mousemove(function(e) {
        $("#large").fadeIn(0)
        if (!native_width && !native_height) {
            var image_object = new Image();
            image_object.src = $("#small").attr("src");
            native_width = image_object.width;
            native_height = image_object.height;
        } else {
            var magnify_offset = $(this).offset();
            var mx = e.pageX - magnify_offset.left;
            var my = e.pageY - magnify_offset.top;

            if ($("#large").is(":visible")) {
                var rx = Math.round(mx / $("#small").width() * native_width - $("#large").width() / 2) * -1;
                var ry = Math.round(my / $("#small").height() * native_height - $("#large").height() / 2) * -1;
                var bgp = rx + "px " + ry + "px";
                $("#large").css({ backgroundPosition: bgp });
            }
        }
    });
})
// End Image Zoomer