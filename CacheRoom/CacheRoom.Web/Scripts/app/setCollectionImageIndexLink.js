﻿$(document).ready(function () {
    $(".collectionLink").change(function (e) {
        e.preventDefault();

        var parentDiv = $(e.currentTarget).closest("div");
        var parentDivId = parentDiv.attr('id');

        var imageId = $("#" + parentDivId).find('.collectionImageId').val();

        var linkId = $("#" + parentDivId).find('.collectionLink').val();

        var jsonObject = { "ImageId": imageId, "LinkId": linkId };

        $.ajax({
            type: "POST",
            url: "HomePage/SetCollectionImageLink",
            data: JSON.stringify(jsonObject),
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            async: true,
            success: function (data) {
                if (data.IsSetLink === true) {
                    alert("Успешно зададохте линк за снимката на тази марка");
                } else {
                    alert("Възникна проблем. Моля опитайте отново.");
                }
            }
        });
    });
})