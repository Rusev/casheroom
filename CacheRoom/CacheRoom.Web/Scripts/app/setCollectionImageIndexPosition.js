﻿$(document).ready(function () {
    $(".collectionImage").change(function (e) {
        e.preventDefault();

        var parentDiv = $(e.currentTarget).closest("div");
        var parentDivId = parentDiv.attr('id');

        var imageId = $("#" + parentDivId).find('.collectionImageId').val();

        var position = $(e.currentTarget).val();

        var jsonObject = { "Position": position, "ImageId": imageId };

        $.ajax({
            type: "POST",
            url: "HomePage/SetCollectionImagePosition",
            data: JSON.stringify(jsonObject),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async:true,
            success: function (data) {
                
                if (data.IsSetPosition === true) {
                    alert("Успешно зададохте позиция за текущата снимка");
                }
                else {
                    alert("Възникна проблем. Моля опитайте отново.");
                }
            }
        });

    });
});