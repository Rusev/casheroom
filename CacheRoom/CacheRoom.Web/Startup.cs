﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CacheRoom.Web.Startup))]
namespace CacheRoom.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
            ConfigureAuth(app);
        }
    }
}
