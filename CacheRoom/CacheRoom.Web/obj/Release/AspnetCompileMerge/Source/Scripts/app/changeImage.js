﻿$(document).ready(function() {
    $(".img").on('click', function (e) {
        e.preventDefault();
        var imageId = $(e.currentTarget).attr('id');
        var chosenImagePath = $.trim($('input[name=imageUrl-' + imageId + ']').val());
        $('.img').css({
            'border': 'none'
        });
        $('#' + imageId).css(
            {
                'border-style':'solid',
                'border-color': 'black',
                'border-width':'1px'
            });
        
        $("#large").css("background", "url("+ encodeURI(chosenImagePath) +") no-repeat");
        $("#small").attr("src", chosenImagePath);
    });
});
