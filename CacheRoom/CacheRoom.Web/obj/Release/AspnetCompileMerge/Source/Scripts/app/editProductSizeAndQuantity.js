﻿$(document).ready(function() {
    $("#change_size").click(function () {
        var productId = $('input[name=product_id]').val();
        var sizeType = $('input[name=size_type]').val();

        var jsonObject = { "ProductId": productId,"SizeType":sizeType };
        
        $.ajax({
            type: "POST",
            url: "/Admin/Product/GetProductQuantities",
            data: JSON.stringify(jsonObject),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            success: function(data) {
               var trHtml = "<tbody>";
                    $.each(data, function (i, item) {
                        var tr = "<tr class='size_data'><td class='sLabel'>" + i + "</td>" + "<td><input id='"+i+"' name='quantity' type='number' value='"+item+"'/></td></tr>";
                        trHtml += tr;
                    });
                trHtml += "</tbody>";

                $("#edit_sizes tbody").remove();
                $("#edit_sizes").append(trHtml);
            }
        });

        $(".changeSizeQuantity").modal("show");
    });

});
$(document).ready(function () {
    $("#save_sizes").click(function () {
       var jsonObject = [];
        $("#edit_sizes tr").each(function() {
            var label = $(this).find(".sLabel").html();
            var amount = $(this).find("input[name='quantity']").val();
            var object = new Object();
            object.SizeLabel = label;
            object.Amount = amount;
            jsonObject.push(object);
        });

       jsonObject.splice(0, 1);
       var productId = $('input[name=product_id]').val();
       var json = new Object();

       json.Quantities = jsonObject;
       json.ProductId = productId;
       $.ajax({
            type: "POST",
            url: "/Admin/Product/ChangeProductQuantities",
            data: JSON.stringify(json),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            success: function(data) {
                $.each(data, function(i, item) {
                    $("#" + i).val(item);
                });
            }
        });
       $(".changeSizeQuantity").modal("hide");
    });

});
$(document).keyup(function(e) {
    if (e.code == 27) {
        $(".changeSizeQuantity").modal("hide");
    }
});
