﻿$(document).ready(function () {
    $('table').responsiveTable({
        staticColumns: 1,
        scrollRight: true,
        scrollHintEnabled: true,
        scrollHintDuration: 2000
    });
});