﻿$(document).ready(function() {
    $('.sizes').on('click', function (e) {
        e.preventDefault();
        var sizeId = $(e.currentTarget).attr("id");
        var splittedString = sizeId.split("-");
        var chosenSizeId = splittedString[1];
        
        $('input[name=ChosenSizeId]').val(chosenSizeId);

        var chosenSize = $('input[name=size-' + chosenSizeId + ']').val();
        var test = $('#ChosenSizeId').val();
        console.log(test);
        $('.sizes').css({
            'background-color': 'transparent',
            'color': 'black'
        });

        $('#size-' + chosenSize).css({
            'background-color': 'rgb(61, 60, 60)',
            'color': 'white'
        });
    });
});