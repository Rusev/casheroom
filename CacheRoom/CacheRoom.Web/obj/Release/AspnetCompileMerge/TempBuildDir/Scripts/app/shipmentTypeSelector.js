﻿function setRadionButton(buttonId) {
    $('#' + buttonId).prop('checked', true);
    return false;
}

$(document).ready(function () {
    $("#address-info").hide();
    $("#office-info").hide();

    $('#to-door').click(function () {
        if ($('#to-door').is(':checked')) {
            $("#address-info").show(500);
            $("#office-info").hide(500);
            $('#shipmentType').val('to-door');
        }
    });

    $('#to-office').click(function () {
        if ($('#to-office').is(':checked')) {
            $("#address-info").hide(500);
            $("#office-info").show(500);
            $('#shipmentType').val('to-office');
        }
    });
});
