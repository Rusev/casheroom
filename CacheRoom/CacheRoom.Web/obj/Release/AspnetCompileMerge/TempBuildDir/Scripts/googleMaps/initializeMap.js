﻿$(document).ready(function () {
    var markers = [];
    $('#CityOffice').on('blur', function () {
        
        // Add a marker to the map and push to the array.
        function addMarker(office, location, map) {
            var officeCity = "";
            var officeNeighbourhood = "";
            var officeStreet = "";
            var officeStreetNum = "";
            if (office.officeCity !== null) {
                officeCity = office.officeCity;
            }
            if (office.officeNeighbourhood !== null) {
                officeNeighbourhood = " "+office.officeNeighbourhood;
            }
            if (office.Street !== null) {
                officeStreet = " "+office.officeStreet;
            }
            if (office.officeStreetNum !== null) {
                officeStreetNum = " №"+office.officeStreetNum;
            }
            var marker = new google.maps.Marker({
                position: location,
                map: map,
                title: office.officeName,
                customInfo: office.officeId,
                officeAddress: officeCity+officeNeighbourhood+officeStreet+officeStreetNum
            });
            google.maps.event.addListener(marker, 'click', function () {
                var officeId = marker.customInfo;
                var officeAddress = marker.officeAddress;
                var jsonObject = { "OfficeId":officeId};
                $.ajax({
                    type: "POST",
                    url: 'GetOfficeCode',
                    data: JSON.stringify(jsonObject),
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $("#officeCode").val(data);
                        $("#EcontInfo_ToOfficeInformation_Office").val(officeAddress);

                    },
                    error: function (e) {
                        console.log(e);
                    }

                });
                
                
            });
            markers.push(marker);
        }

        // Sets the map on all markers in the array.
        function setAllMap(map) {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
            }
        }

        // Removes the markers from the map, but keeps them in the array.
        function clearMarkers() {
            setAllMap(null);
        }

        // Shows any markers currently in the array.
        function showMarkers() {
            setAllMap(map);
        }

        // Deletes all markers in the array by removing references to them.
        function deleteMarkers() {
            clearMarkers();
            markers = [];
        }

        function initialize(data) {
            var mapCenter = new google.maps.LatLng(data[0].officeLatitude, data[0].officeLongitude);
            var mapProp = {
                center: mapCenter,
                zoom: 8,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
            deleteMarkers();
            for (var i = 0; i < data.length; i++) {
                var latlng = new google.maps.LatLng(data[i].officeLatitude, data[i].officeLongitude);
                addMarker(data[i],latlng,map);
            }

        }

        var cityInfo = $('#CityOffice').val();
        
        var cityInfoRegex = new RegExp("([А-Яа-я]+)+([-])+([0-9]+)");

        if (cityInfoRegex.test(cityInfo)) {
            var postCode = cityInfo.split('-');
            var jsonObject = { 'PostCode': postCode[1] };
            
            $.ajax({
                type: "POST",
                url: 'GetOffices',
                data: JSON.stringify(jsonObject),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (data) {
                    initialize(data);
                },
                error:function(e) {
                    console.log(e);
                }
                
            });
        }
        
    });
});

