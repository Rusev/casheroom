﻿$(function() {
    var chat = $.connection.shipmentsHub;

    chat.client.broadcastMessage = function (message) {
        $('#notSended').text(message);
        $('#newShipments').text(message);
    };

    $.connection.hub.start().done(function() {
        chat.server.send();
    },5000);
});