﻿$("#accordionMenu > li > div").click(function () {

    if (false == $(this).next().is(':visible')) {
        $('#accordionMenu ul').slideUp(300);
    }
    $(this).next().slideToggle(300);
});

$('#accordionMenu ul:eq(0)').show();