﻿$(document).ready(function () {
    $("#City").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: "AutoCompleteCity",
                type: "POST",
                dataType: "json",
                data: { term: request.term },
                async:true,
                success: function(data) {
                    response($.map(data, function(item) {
                        return { label: item.Name-item.PostCode, value:item.Name+"-"+item.PostCode }
                    }));
                }
            });
        }

    });
    $("#CityOffice").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "AutoCompleteCity",
                type: "POST",
                dataType: "json",
                data: { term: request.term },
                async:true,
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item.Name - item.PostCode, value: item.Name + "-" + item.PostCode }
                    }));
                }
            });
        }

    });
    $("#Neighbourhood").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: "AutoCompleteNeighbourhood",
                type: "POST",
                dataType: "json",
                data: { term: request.term },
                async:true,
                success: function(data) {
                    response($.map(data, function(item) {
                        return { label: item.Name, value: item.Name }
                    }));
                }
            });
        }
    });

    $("#Street").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: "AutoCompleteStreet",
                type: "POST",
                dataType: "json",
                data: { term: request.term },
                async:true,
                success: function(data) {
                    response($.map(data, function(item) {
                        return { label: item.Name, value: item.Name }
                    }));
                }
            });
        }
    });
});