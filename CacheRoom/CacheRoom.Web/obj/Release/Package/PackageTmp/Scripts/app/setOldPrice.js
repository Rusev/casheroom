﻿$(document).ready(function () {
    var price = $('#Price').val();
    $('.oldPrice').val(price);

    $('.oldPrice').hide();
    var isOnSale = $("#isOnSale").val();
    if (isOnSale) {
        $('.oldPrice').toggle();
    }

    $('#IsOnSale').click(function() {
        $('.oldPrice').toggle();
    });
})