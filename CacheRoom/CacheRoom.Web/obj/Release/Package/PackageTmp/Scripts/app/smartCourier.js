﻿$(document).ready(function () {
    var cityAddress = '';

    $('#City').blur(function () {
        cityAddress = $('#City').val();
        console.log(cityAddress);
        if (cityAddress === 'София-1000') {
            $('#expressCourier').modal('toggle');

            $('#up-to-60').click(function () {
                if ($('#up-to-60').is(':checked')) {
                    $('#expressCourierType').val('60');
                    console.log('60');
                }
            });

            $('#up-to-90').click(function () {
                if ($('#up-to-90').is(':checked')) {
                    $('#expressCourierType').val('90');
                    console.log('90');
                }
            });

            $('#up-to-120').click(function () {
                if ($('#up-to-120').is(':checked')) {
                    $('#expressCourierType').val('120');
                    console.log('120');
                }
            });
        }
    });
});

$(document).keyup(function (e) {
    if (e.keyCode === 27) {
        $('#expressCourier').modal('hide');
    }
});