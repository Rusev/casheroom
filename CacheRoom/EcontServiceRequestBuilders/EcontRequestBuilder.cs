﻿namespace EcontServiceRequestBuilders
{
    using System.Xml.Linq;
    public class EcontRequestBuilder
    {
        public EcontRequestBuilder()
        {

        }
        public XElement CreateClientPart(string username, string password)
        {
            var clientElement = new XElement("client");
            var usernameElement = new XElement("username", username);
            var passwordElement = new XElement("password", password);

            clientElement.Add(usernameElement);
            clientElement.Add(passwordElement);

            return clientElement;
        }

        public XElement CreateRequestPart()
        {
            var requestElement = new XElement("request");

            return requestElement;
        }

        public XElement CreateRequestType(RequestType type)
        {
            var requestTypeElement = new XElement("request_type", type);

            return requestTypeElement;
        }

        public XDocument CreateDataRequest(XElement client, RequestType type)
        {
            var dataRequest = new XDocument();

            var requestElement = CreateRequestPart();
            var requestTypeElement = CreateRequestType(type);

            requestElement.Add(client);
            requestElement.Add(requestTypeElement);

            dataRequest.Add(requestElement);

            return dataRequest;
        }
    }
}
