﻿namespace EcontServiceRequestBuilders
{
    using System.Xml.Linq;

    public class RefuseShipmentRequestBuilder:EcontRequestBuilder
    {
        public XElement CreateShipmentsPart(string shipmentNumber)
        {
            var shipmentsPart = new XElement("cancel_shipments");
            var shipmentNumberPart = new XElement("num",shipmentNumber);
            shipmentsPart.Add(shipmentNumberPart);

            return shipmentsPart;
        }

        public XDocument CreateCancelShipmentsRequest(XElement client, XElement shipments)
        {
            var cancelShipmentsRequest = new XDocument();
            var request = this.CreateRequestPart();
            var requestType = this.CreateRequestType(RequestType.cancel_shipments);

            request.Add(client);
            request.Add(requestType);
            request.Add(shipments);

            cancelShipmentsRequest.Add(request);

            return cancelShipmentsRequest;
        }
    }
}
