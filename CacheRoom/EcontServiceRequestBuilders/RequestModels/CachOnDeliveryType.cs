﻿namespace EcontServiceRequestBuilders.RequestModels
{
    using System.ComponentModel.DataAnnotations;

    public enum CachOnDeliveryType
    {
        [Display(Name="Да се изплати на получателя")]
        GIVE,
        [Display(Name = "Да се съберат от получателя")]
        GET
    }
}
