﻿namespace EcontServiceRequestBuilders.RequestModels
{
    using System.ComponentModel.DataAnnotations;

    public enum InstructionsReturn
    {
        [Display(Name = "Доставка и връщане за моя сметка")]
        shipping_returns,
        [Display(Name = "Само връщането да е за моя сметка")]
        returns
    }
}
