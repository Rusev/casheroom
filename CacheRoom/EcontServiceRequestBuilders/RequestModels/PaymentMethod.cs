﻿namespace EcontServiceRequestBuilders.RequestModels
{
    using System.ComponentModel.DataAnnotations;

    public enum PaymentMethod
    {
        [Display(Name="Кеш")]
        CASH,
        [Display(Name = "Кредит")]
        CREDIT,
        [Display(Name = "Бонус")]
        BONUS
    }
}
