﻿namespace EcontServiceRequestBuilders.RequestModels
{
    public class PaymentModel
    {
        public string PaymentSide { get; set; }

        public string PaymentMethod { get; set; }

        public decimal ReceiverShareSum { get; set; }

        public int PercentShare { get; set; }

        public string KeyWord { get; set; }
    }
}
