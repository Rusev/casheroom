﻿namespace EcontServiceRequestBuilders.RequestModels
{
    using System.ComponentModel.DataAnnotations;

    public enum PaymentSide
    {
        [Display(Name="Изпращач")]
        SENDER,
        [Display(Name = "Получател")]
        RECEIVER,
        [Display(Name = "Друго")]
        OTHER
    }
}
