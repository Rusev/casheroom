﻿namespace EcontServiceRequestBuilders.RequestModels
{
    using System.ComponentModel.DataAnnotations;
    using System.Security.Principal;

    public enum PriorityType
    {
        [Display(Name="Без приоритетен час")]
        None,
        [Display(Name="Точно в посочения час")]
        IN,
        [Display(Name = "Преди посочения час")]
        BEFORE,
        [Display(Name = "След посочения час")]
        AFTER
    }
}
