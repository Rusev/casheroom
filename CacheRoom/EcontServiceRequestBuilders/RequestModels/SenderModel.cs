﻿namespace EcontServiceRequestBuilders.RequestModels
{
    using System.ComponentModel.DataAnnotations;
    public class SenderModel
    {
        [Required]
        public string CityName { get; set; }

        [Required]
        public string PostCode { get; set; }

        [Required]
        public string CompanyName { get; set; }

        [Required]
        public string PersonName { get; set; }

        [Required]
        public string PhoneNumber { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string EmailOnDelivery { get; set; }

        public string Neighbourhood { get; set; }
        
        public string Street { get; set; }

        public string StreetNumber { get; set; }

        public string AddressBlock { get; set; }

        public string AddressEntrance { get; set; }

        public string AddressFloor { get; set; }

        public string AddressAppartment { get; set; }

        public string AddressOther { get; set; }

        public string OfficeCode { get; set; }

    }
}
