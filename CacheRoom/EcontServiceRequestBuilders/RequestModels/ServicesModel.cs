﻿namespace EcontServiceRequestBuilders.RequestModels
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class ServicesModel
    {
        public string PriorityType { get; set; }

        public string  PriorityHour { get; set; }

        [Required]
        public string BackReceip { get; set; }

        [Required]
        public string BillOfGoods { get; set; }

        [Required]
        public string TwoWayShipment { get; set; }

        [Required]
        public decimal ProductPrice { get; set; }

        [Required]
        public string CachOnDeliveryType { get; set; }

        [Required]
        public string CachCurrency { get; set; }

        public string CachOnDeliveryAgreementNum { get; set; }
    }
}
