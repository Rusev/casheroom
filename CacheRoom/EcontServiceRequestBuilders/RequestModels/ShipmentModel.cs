﻿namespace EcontServiceRequestBuilders.RequestModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    public class ShipmentModel
    {
        public string EnvelopeNumber { get; set; }

        [Required]
        public string ShipmentType { get; set; }

        [Required]
        public string ShipmentDescription { get; set; }

        [Required]
        public int PackCount { get; set; }

        [Required]
        public decimal Weight { get; set; }

        public string TariffCode { get; set; }

        public string TariffSubCode { get; set; }

        public string PayAfterAccept { get; set; }

        public string PayAfterTest { get; set; }

        public string InstructionsReturn { get; set; }

        public DateTime? SendDate { get; set; }

        public DateTime? DeliveryDate { get; set; }

        public bool SizeUnderSixty { get; set; }
    }
}
