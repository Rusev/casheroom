﻿namespace EcontServiceRequestBuilders.RequestModels
{
    using System.ComponentModel.DataAnnotations;

    public enum ShipmentType
    {
        [Display(Name="Колет")]
        PACK,
        [Display(Name = "Документи")]
        DOCUMENT,
        [Display(Name = "Палет")]
        PALLET,
        [Display(Name = "Карго експрес")]
        CARGO,
        [Display(Name = "Палет + Документи")]
        DOCUMENTPALLET
        
    }
}
