﻿namespace EcontServiceRequestBuilders.RequestModels
{
    using System.ComponentModel.DataAnnotations;

    public enum TariffSubCode
    {   
        [Display(Name = "От врата")]
        DOOR,
        [Display(Name = "От офис")]
        OFFICE
    }
}
