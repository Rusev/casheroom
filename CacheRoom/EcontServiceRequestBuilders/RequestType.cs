﻿namespace EcontServiceRequestBuilders
{
    public enum RequestType
    {
        shipments,
        cancel_shipments,
        cities_zones,
        countries,
        cities,
        cities_quarters,
        cities_streets,
        cities_regions,
        offices,
        shipping
    }
}
