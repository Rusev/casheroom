﻿namespace EcontServiceRequestBuilders
{
    using System.ComponentModel.Design;
    using System.Runtime.InteropServices;
    using System.Xml.Linq;

    using EcontServiceRequestBuilders.RequestModels;

    public class ShipmentRequestBuilder:EcontRequestBuilder
    {
        
        public XElement CreateSystemPart(int validateType = 0, string responseType = "XML")
        {
            var systemElement = new XElement("system");
            var validateElement = new XElement("validate", validateType);
            var responseTypeElement = new XElement("response_type", responseType);
            var onlyCalculate = new XElement("only_calculate", "0");

            systemElement.Add(validateElement);
            systemElement.Add(responseTypeElement);
            systemElement.Add(onlyCalculate);

            return systemElement;
        }

        public XElement CreateClientPart(string userName, string password)
        {
            var clientElement = new XElement("client");
            var userNameElement = new XElement("username",userName);
            var passwordElement = new XElement("password", password);

            clientElement.Add(userNameElement);
            clientElement.Add(passwordElement);

            return clientElement;
        }

        public XElement CreateSenderPart(SenderModel sender)
        {
            var senderElement = new XElement("sender");
            var cityElement = new XElement("city", sender.CityName);
            var postCodeElement = new XElement("post_code", sender.PostCode);
            var officeCode = new XElement("office_code", sender.OfficeCode);
            var companyElement = new XElement("name", sender.CompanyName);
            var senderNameElement = new XElement("name_person", sender.PersonName);
            var quarterElement = new XElement("quarter", sender.Neighbourhood);
            var streetElement = new XElement("street", sender.Street);
            var streetNumberElement = new XElement("street_num", sender.StreetNumber);
            var blockElement = new XElement("street_bl", sender.AddressBlock);
            var entranceElement = new XElement("street_vh", sender.AddressEntrance);
            var floorElement = new XElement("street_et", sender.AddressFloor);
            var appartmentElement = new XElement("street_ap", sender.AddressAppartment);
            var other = new XElement("street_other", sender.AddressOther);
            var phoneElement = new XElement("phone_num", sender.PhoneNumber);

            senderElement.Add(cityElement);
            senderElement.Add(postCodeElement);
            senderElement.Add(officeCode);
            senderElement.Add(companyElement);
            senderElement.Add(senderNameElement);
            senderElement.Add(quarterElement);
            senderElement.Add(streetElement);
            senderElement.Add(streetNumberElement);
            senderElement.Add(blockElement);
            senderElement.Add(entranceElement);
            senderElement.Add(floorElement);
            senderElement.Add(appartmentElement);
            senderElement.Add(other);
            senderElement.Add(phoneElement);

            return senderElement;
        }

        public XElement CreateRecieverPart(ReceiverModel receiver)
        {
            var recieverElement = new XElement("receiver");
            var cityElement = new XElement("city", receiver.CityName);
            var postCodeElement = new XElement("post_code", receiver.PostCode);
            var officeCodeElement = new XElement("office_code", receiver.OfficeCode);
            var companyElement = new XElement("name", receiver.PersonName);
            var receiverNameElement = new XElement("name_person",string.Empty);
            var quarterElement = new XElement("quarter", receiver.Neighbourhood);
            var streetElement = new XElement("street", receiver.Street);
            var streetNumberElement = new XElement("street_num", receiver.StreetNumber);
            var blockElement = new XElement("street_bl", receiver.AddressBlock);
            var entranceElement = new XElement("street_vh", receiver.AddressEntrance);
            var floorElement = new XElement("street_et", receiver.AddressFloor);
            var appartmentElement = new XElement("street_ap", receiver.AddressAppartment);
            var other = new XElement("street_other", receiver.AddressOther);
            var phoneElement = new XElement("phone_num", receiver.PhoneNumber);
            var smsElement = new XElement("sms_no", receiver.PhoneNumber);
            var emailElement = new XElement("receiver_email", receiver.Email);

            recieverElement.Add(cityElement);
            recieverElement.Add(postCodeElement);
            recieverElement.Add(officeCodeElement);
            recieverElement.Add(companyElement);
            recieverElement.Add(receiverNameElement);
            recieverElement.Add(emailElement);
            recieverElement.Add(quarterElement);
            recieverElement.Add(streetElement);
            recieverElement.Add(streetNumberElement);
            recieverElement.Add(blockElement);
            recieverElement.Add(entranceElement);
            recieverElement.Add(floorElement);
            recieverElement.Add(appartmentElement);
            recieverElement.Add(other);
            recieverElement.Add(phoneElement);
            recieverElement.Add(smsElement);

            return recieverElement;
        }

        public XElement CreateShipmentPart(ShipmentModel shipment)
        {
            var shipmentElement = new XElement("shipment");
            var envelopeNumberElement = new XElement("envelope_num", shipment.EnvelopeNumber);
            var shipmentTypeElement = new XElement("shipment_type", shipment.ShipmentType);
            var descriptionElement = new XElement("description", shipment.ShipmentDescription);
            var packCountElement = new XElement("pack_count", shipment.PackCount);
            var weightElement = new XElement("weight", shipment.Weight);
            var tariffCodeElement = new XElement("tariff_code", shipment.TariffCode);
            var tariffSubCodeElement = new XElement("tariff_sub_code", shipment.TariffSubCode);
            var payAfterAcceptElement = new XElement("pay_after_accept", shipment.PayAfterAccept);
            var payAfterTestElement = new XElement("pay_after_test", shipment.PayAfterTest);
            var instructionsReturnElement = new XElement("instruction_returns", shipment.InstructionsReturn);

            shipmentElement.Add(envelopeNumberElement);
            shipmentElement.Add(shipmentTypeElement);
            shipmentElement.Add(descriptionElement);
            shipmentElement.Add(packCountElement);
            shipmentElement.Add(weightElement);
            shipmentElement.Add(tariffCodeElement);
            shipmentElement.Add(tariffSubCodeElement);
            shipmentElement.Add(payAfterAcceptElement);
            shipmentElement.Add(payAfterTestElement);
            shipmentElement.Add(instructionsReturnElement);

            return shipmentElement;
        }

        public XElement CreatePaymentPart(PaymentModel payment)
        {
            var paymentElement = new XElement("payment");
            var paymentSideElement = new XElement("side", payment.PaymentSide);
            var paymentMethodElement = new XElement("method", payment.PaymentMethod);
            var recieverShareSumElement = new XElement("recеiver_share_sum", payment.ReceiverShareSum);
            var percentShareElement = new XElement("share_percent", payment.PercentShare);
            var keyWordElement = new XElement("key_word", payment.KeyWord);

            paymentElement.Add(paymentSideElement);
            paymentElement.Add(paymentMethodElement);
            paymentElement.Add(recieverShareSumElement);
            paymentElement.Add(percentShareElement);
            paymentElement.Add(keyWordElement);

            return paymentElement;

        }

        public XElement CreateServicesPart(ServicesModel services)
        {
            
            var servicesElement = new XElement("services");
            var priorityTypeElement = new XElement("p", services.PriorityHour);

            if (services.PriorityType != null)
            {
                priorityTypeElement.Add(new XAttribute("type", services.PriorityType));
            }
            else
            {
                priorityTypeElement.Add(new XAttribute("type", string.Empty));
            }


            var eElement = new XElement("e");
            var e1Element = new XElement("e1");
            var e2Element = new XElement("e2");
            var e3Element = new XElement("e3");
            var receiptElement = new XElement("dc",services.BackReceip);
            var billOfGoods = new XElement("dc_cp",services.BillOfGoods);
            var twoWayShipment = new XElement("dp",services.TwoWayShipment);

            var cashOnDeliveryType = new XElement("cd");
            cashOnDeliveryType.Value = services.ProductPrice.ToString();
            cashOnDeliveryType.Add(new XAttribute("type", services.CachOnDeliveryType));

            var cachOnDeliveryCurrency = new XElement("cd_currency", services.CachCurrency);



            var aggNumberForCashOnDelivery = new XElement("cd_agreement_num",services.CachOnDeliveryAgreementNum);
            var pack1Element = new XElement("pack1");
            var pack2Element = new XElement("pack2");
            var pack3Element = new XElement("pack3");
            var pack4Element = new XElement("pack4");
            var pack5Element = new XElement("pack5");
            var pack6Element = new XElement("pack6");
            var pack7Element = new XElement("pack7");
            var pack8Element = new XElement("pack8");
            var refrigeratedBagElement = new XElement("ref");

            servicesElement.Add(priorityTypeElement);
            servicesElement.Add(eElement);
            servicesElement.Add(e1Element);
            servicesElement.Add(e2Element);
            servicesElement.Add(e3Element);
            servicesElement.Add(receiptElement);
            servicesElement.Add(billOfGoods);
            servicesElement.Add(twoWayShipment);
            servicesElement.Add(cashOnDeliveryType);
            servicesElement.Add(cachOnDeliveryCurrency);
            servicesElement.Add(aggNumberForCashOnDelivery);
            servicesElement.Add(pack1Element);
            servicesElement.Add(pack2Element);
            servicesElement.Add(pack3Element);
            servicesElement.Add(pack4Element);
            servicesElement.Add(pack5Element);
            servicesElement.Add(pack6Element);
            servicesElement.Add(pack7Element);
            servicesElement.Add(pack8Element);
            servicesElement.Add(refrigeratedBagElement);

            return servicesElement;
        }
        public XDocument CreateXmlRequest(XElement system, XElement client, XElement sender, XElement reciever,
            XElement shipment, XElement payment,XElement services)
        {
            var shipmentRequest = new XDocument();

            var requestType = new XElement("request_type", RequestType.shipping);

            var parcelsElement = new XElement("parcels");
            var loadingsElement = new XElement("loadings");
            var rowElement = new XElement("row");

            parcelsElement.Add(client);
            parcelsElement.Add(requestType);
            parcelsElement.Add(system);
            
            rowElement.Add(sender);
            rowElement.Add(reciever);
            rowElement.Add(shipment);
            rowElement.Add(services);
            rowElement.Add(payment);

            loadingsElement.Add(rowElement);

            parcelsElement.Add(loadingsElement);

            shipmentRequest.Add(parcelsElement);
            var decl = new XDeclaration("1.0", "", "");
            shipmentRequest.Declaration = decl;
            return shipmentRequest;
        }
    }
}
