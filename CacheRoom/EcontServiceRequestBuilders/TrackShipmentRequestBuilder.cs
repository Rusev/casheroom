﻿namespace EcontServiceRequestBuilders
{
    using System.Xml.Linq;

    public class TrackShipmentRequestBuilder:EcontRequestBuilder
    {
        public XElement CreateShipmentsPart(string shipmentNumber)
        {
            var shipmentsPart = new XElement("shipments");
            shipmentsPart.Add(new XAttribute("full_tracking","ON"));
            var shipmentNumberPart = new XElement("num", shipmentNumber);
            
            shipmentsPart.Add(shipmentNumberPart);

            return shipmentsPart;
        }

        public XDocument CreateTrackShipmentRequest(XElement client, XElement shipments)
        {
            var trackShipmentRequest = new XDocument();

            var request = this.CreateRequestPart();

            var requestType = this.CreateRequestType(RequestType.shipments);

            request.Add(client);
            request.Add(requestType);
            request.Add(shipments);

            trackShipmentRequest.Add(request);

            return trackShipmentRequest;
        }
    }
}
