﻿namespace CacheRoom.EcontServiceXmlWorkers
{
    using System;
    using System.Data.SqlClient;
    using System.Transactions;
    using System.Xml.Linq;

    using CacheRoom.Data;

    using EntityFramework.BulkInsert.Extensions;

    public class XmlImporter
    {
        
        public XmlImporter()
        {

        }

        public void InitiateZonesImport(XDocument document)
        {
            var xmlParser = new XmlParser();

            using (var db = new CacheRoomDbContext())
            {
                var listZones = xmlParser.ParseZones(document, db);

                using (var transactionScope = new TransactionScope())
                {
                    db.BulkInsert(listZones, new BulkInsertOptions
                    {
                        SqlBulkCopyOptions = SqlBulkCopyOptions.CheckConstraints
                    });
                    db.SaveChanges();
                    transactionScope.Complete();
                }
            }

        }

        public void InitiateCountriesImport(XDocument document)
        {
            var xmlParser = new XmlParser();

            using (var db = new CacheRoomDbContext())
            {
                var listCountries = xmlParser.ParseCountries(document, db);

                using (var transactionScope = new TransactionScope())
                {
                    db.BulkInsert(listCountries, new BulkInsertOptions
                    {
                        SqlBulkCopyOptions = SqlBulkCopyOptions.CheckConstraints
                    });
                    db.SaveChanges();
                    transactionScope.Complete();
                }
            }
        }

        public void InitiateCitiesImport(XDocument document)
        {
            
            var xmlParser = new XmlParser();

            using (var db = new CacheRoomDbContext())
            {
                var listCities = xmlParser.ParseCities(document, db);

                using (var transactionScope = new TransactionScope())
                {
                    db.BulkInsert(listCities, new BulkInsertOptions
                    {
                        SqlBulkCopyOptions = SqlBulkCopyOptions.CheckConstraints
                    });
                    db.SaveChanges();
                    transactionScope.Complete();
                }
            }
        }

        public void InitiateNeigbourhoodsImport(XDocument document)
        {
           
            var xmlParser = new XmlParser();

            using (var db = new CacheRoomDbContext())
            {

                using (var transactionScope = new TransactionScope())
                {
                    var listNeighbours = xmlParser.ParseNeighbours(document, db);

                    db.BulkInsert(listNeighbours, new BulkInsertOptions
                    {
                        SqlBulkCopyOptions = SqlBulkCopyOptions.CheckConstraints
                    });
                    db.SaveChanges();
                    transactionScope.Complete();
                }
            }
        }

        public void InitiateStreetsImport(XDocument document)
        {
           
            var xmlParser = new XmlParser();

            using (var db = new CacheRoomDbContext())
            {
                var listStreets = xmlParser.ParseStreets(document, db);

                using (var transactionScope = new TransactionScope())
                {
                    db.BulkInsert(listStreets, new BulkInsertOptions
                    {
                        SqlBulkCopyOptions = SqlBulkCopyOptions.CheckConstraints
                    });
                    db.SaveChanges();
                    transactionScope.Complete();
                }
            }
        }

        public void InitiateOfficesImport(XDocument document)
        {
            
            var xmlParser = new XmlParser();

            using (var db = new CacheRoomDbContext())
            {
                var listOffices = xmlParser.ParseOffices(document, db);

                using (var transactionScope = new TransactionScope())
                {
                    db.BulkInsert(listOffices, new BulkInsertOptions
                    {
                        SqlBulkCopyOptions = SqlBulkCopyOptions.CheckConstraints
                    });

                    db.SaveChanges();
                    transactionScope.Complete();
                }
            }
        }
    }
}
