﻿namespace CacheRoom.EcontServiceXmlWorkers
{
    using System.Linq;
    using System.Xml.Linq;
    using System.Xml.XPath;
    using System.Collections.Generic;
    using CacheRoom.Data;
    using CacheRoom.Models.Econt;

    public class XmlParser
    {
        public XmlParser()
        {

        }
        public IList<Zone> ParseZones(XDocument document, CacheRoomDbContext db)
        {
            var zones = document.XPathSelectElements("response/zones/e");
            var listZones = new List<Zone>();

            foreach (var z in zones)
            {

                var id = int.Parse(z.Element("id").Value);

                var nameBg = z.Element("name").Value;

                var nameEn = z.Element("name_en").Value;

                var national = int.Parse(z.Element("national").Value);

                var isEcontServed = int.Parse(z.Element("is_ee").Value);

                var lastUpdated = z.Element("updated_time").Value;


                var zone = new Zone();

                zone.Id = id;
                zone.Name = nameBg;
                zone.NameEn = nameEn;
                zone.IsEcontServed = isEcontServed;
                zone.IsNational = national;

                if (lastUpdated.Equals("0000-00-00 00:00:00"))
                {
                    zone.UpdatedTime = "1000-01-01 00:00:00";
                }
                else
                {
                    zone.UpdatedTime = lastUpdated;
                }

                listZones.Add(zone);
            }

            return listZones;
        }

        public IList<Country> ParseCountries(XDocument document, CacheRoomDbContext db)
        {

            var countries = document.XPathSelectElements("response/e");
            var zones = db.Zones.ToList();
            var listCountries = new List<Country>();

            foreach (var c in countries)
            {
                var name = c.Element("country_name").Value;

                var nameEn = c.Element("country_name_en").Value;

                var zoneId = int.Parse(c.Element("id_zone").Value);

                var countryZone = zones.Where(z => z.Id == zoneId).FirstOrDefault();

                var country = new Country();

                country.Name = name;
                country.NameEn = nameEn;
                country.Zone = countryZone;
                country.ZoneId = countryZone.Id;
                listCountries.Add(country);

            }

            return listCountries;
        }

        public IList<City> ParseCities(XDocument document, CacheRoomDbContext db)
        {
            var cities = document.XPathSelectElements("response/cities/e");

            var zones = db.Zones.ToList();

            var listCities = new List<City>();
            foreach (var c in cities)
            {

                var id = int.Parse(c.Element("id").Value);

                var postCode = c.Element("post_code").Value;

                var type = c.Element("type").Value;

                var idZone = int.Parse(c.Element("id_zone").Value);

                var name = c.Element("name").Value;

                var nameEn = c.Element("name_en").Value;

                var region = c.Element("region").Value;

                var regionEn = c.Element("region_en").Value;

                var idCountry = c.Element("id_country").Value;

                var idOffice = c.Element("id_office").Value;

                var lastUpdated = c.Element("updated_time").Value;

                var cityZone = zones.Where(z => z.Id == idZone).FirstOrDefault();


                var city = new City();

                city.Id = id;
                city.Name = name;
                city.NameEn = nameEn;
                city.PostCode = int.Parse(postCode);
                city.Zone = cityZone;
                city.ZoneId = cityZone.Id;
                city.Type = type;
                city.MainOfficeId = int.Parse(idOffice);
                city.RegionName = region;
                city.RegionNameEn = regionEn;

                if (lastUpdated.Equals("0000-00-00 00:00:00"))
                {
                    city.UpdatedTime = "01-01-0001 12:00:00";
                }
                else
                {
                    city.UpdatedTime = lastUpdated;
                }

                listCities.Add(city);
            }

            return listCities;
        }

        public IList<Neighbourhood> ParseNeighbours(XDocument document, CacheRoomDbContext db)
        {
            var neighbourhoods = document.XPathSelectElements("response/cities_quarters/e");
            var cities = db.Cities.ToList();
            var listNeighbours = new List<Neighbourhood>();

            foreach (var n in neighbourhoods)
            {
                var id = int.Parse(n.Element("id").Value);

                var name = n.Element("name").Value;

                var nameEn = n.Element("name_en").Value;

                var idCity = int.Parse(n.Element("id_city").Value);

                var cityPostCode = int.Parse(n.Element("city_post_code").Value);

                var lastUpdated = n.Element("updated_time").Value;


                var cityNeighbourhood = cities.Where(c => c.Id == idCity).FirstOrDefault();


                var neighbourhood = new Neighbourhood();

                neighbourhood.Id = id;
                neighbourhood.Name = name;
                neighbourhood.NameEn = nameEn;
                neighbourhood.City = cityNeighbourhood;
                neighbourhood.CityId = cityNeighbourhood.Id;
                neighbourhood.CityPostCode = cityNeighbourhood.PostCode;

                if (lastUpdated.Equals("0000-00-00 00:00:00"))
                {
                    neighbourhood.UpdatedTime = "01-01-0001 12:00:00";
                }
                else
                {
                    neighbourhood.UpdatedTime = lastUpdated;
                }

                listNeighbours.Add(neighbourhood);
            }

            return listNeighbours;
        }

        public IList<Street> ParseStreets(XDocument document, CacheRoomDbContext db)
        {
            var streets = document.XPathSelectElements("response/cities_street/e");
            var cities = db.Cities.ToList();
            var listStreets = new List<Street>();

            foreach (var s in streets)
            {
                var id = int.Parse(s.Element("id").Value);

                var name = s.Element("name").Value;

                var nameEn = s.Element("name_en").Value;

                var cityPostCode = int.Parse(s.Element("city_post_code").Value);

                var idCity = int.Parse(s.Element("id_city").Value);

                var lastUpdated = s.Element("updated_time").Value;

                var cityStreet = cities.Where(t => t.Id == idCity).FirstOrDefault();
                
                var street = new Street();
                street.Id = id;
                street.Name = name;
                street.NameEn = nameEn;
                street.City = cityStreet;
                street.CityId = cityStreet.Id;
                street.CityPostCode = cityStreet.PostCode;

                if (lastUpdated.Equals("0000-00-00 00:00:00"))
                {
                    street.UpdatedTime = "01-01-0001 12:00:00";
                }
                else
                {
                    street.UpdatedTime = lastUpdated;
                }

                listStreets.Add(street);
                
            }
            return listStreets;
        }

        public IList<Office> ParseOffices(XDocument document, CacheRoomDbContext db)
        {
            var offices = document.XPathSelectElements("response/offices/e");
            var neighbourhoodsDb = db.Neighbourhoods.ToList();
            var streetsDb = db.Streets.ToList();
            var citiesDb = db.Cities.ToList();
            var listOffices = new List<Office>();

            foreach (var o in offices)
            {
                var id = int.Parse(o.Element("id").Value);

                var name = o.Element("name").Value;

                var nameEn = o.Element("name_en").Value;

                var officeCode = o.Element("office_code").Value;

                var cityId = int.Parse(o.Element("id_city").Value);

                var latitude = o.Element("latitude").Value;

                var longitude = o.Element("longitude").Value;


                var neighbourhoodId = int.Parse(o.Element("address_details").Element("id_quarter").Value);

                var streetId = int.Parse(o.Element("address_details").Element("id_street").Value);

                var streetNumber = o.Element("address_details").Element("num").Value;

                var block = o.Element("address_details").Element("bl").Value;

                var entrance = o.Element("address_details").Element("vh").Value;

                var floor = o.Element("address_details").Element("et").Value;

                var appartment = o.Element("address_details").Element("ap").Value;

                var other = o.Element("address_details").Element("other").Value;

                var phone = o.Element("phone").Value;

                var workBegin = o.Element("work_begin").Value;

                var workEnd = o.Element("work_end").Value;

                var workBeginSat = o.Element("work_begin_saturday").Value;

                var workEndSat = o.Element("work_end_saturday").Value;

                var timePriority = o.Element("time_priority").Value;

                var lastUpdated = o.Element("updated_time").Value;

                var officeCity = citiesDb.Where(c => c.Id == cityId).FirstOrDefault();

                var officeNeighbourhood = neighbourhoodsDb.Where(n => n.Id == neighbourhoodId).FirstOrDefault();

                var officeStreet = streetsDb.Where(s => s.Id == streetId).FirstOrDefault();

                var office = new Office();

                office.Id = id;
                office.Name = name;
                office.NameEn = nameEn;
                office.OfficeCode = officeCode;

                office.City = officeCity;
                if (office.City != null)
                {
                    office.CityId = officeCity.Id;
                }

                office.Lattitude = latitude;
                office.Longitude = longitude;

                office.Neighbourhood = officeNeighbourhood;
                if (office.Neighbourhood != null)
                {
                    office.NeighbourhoodId = officeNeighbourhood.Id;
                }

                office.Street = officeStreet;
                if (office.Street != null)
                {
                    office.StreetId = officeStreet.Id;
                }

                office.StreetNumber = streetNumber;
                office.Phone = phone;
                office.WorkBegin = workBegin;
                office.WorkEnd = workEnd;
                office.WorkBeginSaturday = workBeginSat;
                office.WorkEndSaturday = workEndSat;
                office.TimePriority = timePriority;

                if (lastUpdated.Equals("0000-00-00 00:00:00"))
                {
                    office.UpdatedTime = "01-01-0001 12:00:00";
                }
                else
                {
                    office.UpdatedTime = lastUpdated;
                }

                listOffices.Add(office);

            }

            return listOffices;
        }
    }
}
