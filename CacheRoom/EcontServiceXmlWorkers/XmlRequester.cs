﻿namespace CacheRoom.EcontServiceXmlWorkers
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Xml.Linq;
    using RestSharp;
    public class XmlRequester
    {
        private const string XmlHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";

        public XmlRequester()
        {

        }
        public string HttpXmlRequest(string destinationUrl, string requestXml)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
            byte[] bytes;
            bytes = Encoding.UTF8.GetBytes(requestXml);
            request.ContentType = "text/xml";
            request.Accept = "text/xml";

            request.ContentLength = bytes.Length;
            request.Method = "POST";
            
            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(bytes, 0, bytes.Length);
            }
            
            var response = (HttpWebResponse)request.GetResponse();
            
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream responseStream = response.GetResponseStream();
                string responseStr = new StreamReader(responseStream).ReadToEnd();
                return responseStr;
            }
            return null;
        }

        public XDocument InitiateRequest(string serviceUrl, XDocument document)
        {
            var requestData = XmlHeader + document.ToString();

            string result = HttpXmlRequest(serviceUrl, requestData);
          
            XDocument xmlResult = XDocument.Parse(result);

            return xmlResult;
        }

    }
}
