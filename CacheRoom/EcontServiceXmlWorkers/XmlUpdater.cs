﻿namespace CacheRoom.EcontServiceXmlWorkers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using System.Xml.XPath;
    using System.Data.Entity;
    using CacheRoom.Data.Migrations;
    using System.Globalization;
    using System.Data.Entity.Migrations;
    using EntityFramework.Extensions;
    using EntityFramework.BulkInsert.Extensions;
    using EntityFramework.Audit;
    using System.Transactions;
    using System.Data.SqlClient;

    using CacheRoom.Models.Econt;
    using CacheRoom.Data;
    using EcontServiceRequestBuilders;

    public class XmlUpdater
    {
        public Dictionary<string, XDocument> DownloadNewData(XElement client)
        {
            var econtLiveServiceUrl = "http://e-econt.econt.com/xml_service_tool.php";

            //Downloading the necessary data for syncronization with the ECONT System

            XmlRequester downloader = new XmlRequester();
            var xmlData = new Dictionary<string, XDocument>();
            var requestBuilder = new EcontRequestBuilder();

            var offices = downloader.InitiateRequest(econtLiveServiceUrl, requestBuilder.CreateDataRequest(client, RequestType.offices));
            xmlData.Add("offices", offices);

            var zones = downloader.InitiateRequest(econtLiveServiceUrl, requestBuilder.CreateDataRequest(client, RequestType.cities_zones));
            xmlData.Add("zones", zones);

            var cities = downloader.InitiateRequest(econtLiveServiceUrl, requestBuilder.CreateDataRequest(client, RequestType.cities));
            xmlData.Add("cities", cities);

            var streets = downloader.InitiateRequest(econtLiveServiceUrl, requestBuilder.CreateDataRequest(client, RequestType.cities_streets));
            xmlData.Add("streets", streets);

            var neighbours = downloader.InitiateRequest(econtLiveServiceUrl, requestBuilder.CreateDataRequest(client, RequestType.cities_quarters));
            xmlData.Add("neighbours", neighbours);

            var countries = downloader.InitiateRequest(econtLiveServiceUrl, requestBuilder.CreateDataRequest(client, RequestType.countries));
            xmlData.Add("countries", countries);

            var regions = downloader.InitiateRequest(econtLiveServiceUrl, requestBuilder.CreateDataRequest(client, RequestType.cities_regions));
            xmlData.Add("regions", regions);

            return xmlData;
        }

        private List<AuditEntity> EntityDelete<T>(IList<T> entitiesToDelete) where T : class
        {
            var db = new CacheRoomDbContext();
            var audit = db.BeginAudit();
            var log = new List<AuditEntity>();
            var type = typeof(T);

            if (type.IsAssignableFrom(typeof(Zone)))
            {

                var deletedZones = new List<Zone>();
                foreach (var e in entitiesToDelete)
                {
                    var zone = e as Zone;
                    db.Cities.Where(c => c.ZoneId == zone.Id);
                    db.Zones.Remove(zone);
                    deletedZones.Add(zone);
                }
                db.SaveChanges();
                log = audit.LastLog.Entities;
            }
            else if (type.IsAssignableFrom(typeof(Country)))
            {
                foreach (var e in entitiesToDelete)
                {
                    db.Countries.Remove(e as Country);

                }
                db.SaveChanges();
                log = audit.LastLog.Entities;
            }
            else if (type.IsAssignableFrom(typeof(City)))
            {
                foreach (var e in entitiesToDelete)
                {
                    var city = e as City;

                    db.Offices.Where(t => t.CityId == city.Id).Delete();
                    db.Streets.Where(c => c.CityId == city.Id).Delete();
                    db.Neighbourhoods.Where(n => n.CityId == city.Id).Delete();

                    db.Cities.Where(c => c.Id == city.Id).Delete();

                }
                db.SaveChanges();
                log = audit.LastLog.Entities;
            }
            else if (type.IsAssignableFrom(typeof(Neighbourhood)))
            {
                foreach (var e in entitiesToDelete)
                {
                    var neighbourhood = e as Neighbourhood;
                    db.Offices.Where(o => o.NeighbourhoodId == neighbourhood.Id).Delete();
                    db.Neighbourhoods.Where(n => n.Id == neighbourhood.Id).Delete();

                }
                db.SaveChanges();
                log = audit.LastLog.Entities;
            }
            else if (type.IsAssignableFrom(typeof(Street)))
            {
                foreach (var e in entitiesToDelete)
                {
                    var street = e as Street;
                    db.Offices.Where(o => o.StreetId == street.Id).Delete();
                    db.Streets.Where(s => s.Id == street.Id).Delete();

                }
                db.SaveChanges();
                log = audit.LastLog.Entities;
            }
            else if (type.IsAssignableFrom(typeof(Office)))
            {
                foreach (var e in entitiesToDelete)
                {
                    var office = e as Office;
                    db.Offices.Where(o => o.Id == office.Id).Delete();

                }
                db.SaveChanges();
                log = audit.LastLog.Entities;
            }

            return log;
        }

        private List<AuditEntity> EntityAdd<T>(IList<T> entitiesToAdd) where T : class
        {
            var log = new List<AuditEntity>();
            using (var db = new CacheRoomDbContext())
            {
                var audit = db.BeginAudit();
                using (var transactionScope = new TransactionScope())
                {
                    db.BulkInsert(entitiesToAdd, new BulkInsertOptions
                    {
                        SqlBulkCopyOptions = SqlBulkCopyOptions.CheckConstraints
                    });
                    db.SaveChanges();
                    log = audit.LastLog.Entities;
                    transactionScope.Complete();
                }
            }
            return log;
        }

        private List<AuditEntity> EntityUpdate<T>(IList<T> entitiesToUpdate, IList<T> newData) where T : class
        {
            var type = typeof(T);
            var db = new CacheRoomDbContext();
            var audit = db.BeginAudit();
            var log = new List<AuditEntity>();

            if (type.IsAssignableFrom(typeof(Zone)))
            {
                foreach (var e in entitiesToUpdate)
                {
                    var zone = e as Zone;
                    var oldZone = db.Zones.Where(z => z.Id == zone.Id).FirstOrDefault();

                    var newZones = newData as List<Zone>;
                    var newZone = newZones.Where(z => z.Id == oldZone.Id).FirstOrDefault();

                    oldZone.Name = newZone.Name;
                    oldZone.NameEn = newZone.NameEn;
                    oldZone.IsNational = newZone.IsNational;
                    oldZone.IsEcontServed = newZone.IsEcontServed;
                    oldZone.UpdatedTime = newZone.UpdatedTime;
                }
                db.SaveChanges();
                log = audit.LastLog.Entities;
            }
            else if (type.IsAssignableFrom(typeof(Country)))
            {
                foreach (var e in entitiesToUpdate)
                {
                    var country = e as Country;
                    var oldCountry = db.Countries.Where(c => c.Id == country.Id).FirstOrDefault();

                    var newCountries = newData as List<Country>;
                    var newCountry = newCountries.Where(c => c.Id == oldCountry.Id).FirstOrDefault();

                    oldCountry.Name = newCountry.Name;
                    oldCountry.NameEn = newCountry.NameEn;
                    oldCountry.ZoneId = newCountry.ZoneId;

                }
                db.SaveChanges();
                log = audit.LastLog.Entities;
            }
            else if (type.IsAssignableFrom(typeof(City)))
            {

                foreach (var e in entitiesToUpdate)
                {
                    var city = e as City;
                    var newCitiesData = newData as List<City>;
                    var oldCity = db.Cities.Where(c => c.Id == city.Id).FirstOrDefault();
                    var newCity = newCitiesData.Where(c => c.Id == city.Id).FirstOrDefault();



                    oldCity.Name = newCity.Name;
                    oldCity.NameEn = newCity.NameEn;
                    oldCity.PostCode = newCity.PostCode;
                    oldCity.Type = newCity.Type;
                    oldCity.ZoneId = newCity.ZoneId;
                    oldCity.RegionName = newCity.RegionName;
                    oldCity.RegionNameEn = newCity.RegionNameEn;
                    oldCity.MainOfficeId = newCity.MainOfficeId;
                    oldCity.ServiceDayId = newCity.ServiceDayId;
                    oldCity.UpdatedTime = newCity.UpdatedTime;
                    oldCity.FromDoor = newCity.FromDoor;
                    oldCity.ToDoor = newCity.ToDoor;
                    oldCity.FromOffice = newCity.FromOffice;
                    oldCity.ToOffice = newCity.ToOffice;


                }
                db.SaveChanges();
                log = audit.LastLog.Entities;

            }
            else if (type.IsAssignableFrom(typeof(Neighbourhood)))
            {
                foreach (var e in entitiesToUpdate)
                {
                    var neighbourhood = e as Neighbourhood;
                    var oldNeighbourhood = db.Neighbourhoods.Where(n => n.Id == neighbourhood.Id).FirstOrDefault();

                    var newNeighbourhoods = newData as List<Neighbourhood>;
                    var newNeighbourhood = newNeighbourhoods.Where(n => n.Id == oldNeighbourhood.Id).FirstOrDefault();

                    oldNeighbourhood.Name = newNeighbourhood.Name;
                    oldNeighbourhood.NameEn = newNeighbourhood.NameEn;
                    oldNeighbourhood.CityId = newNeighbourhood.CityId;
                    oldNeighbourhood.CityPostCode = newNeighbourhood.CityPostCode;
                    oldNeighbourhood.UpdatedTime = newNeighbourhood.UpdatedTime;

                }
                db.SaveChanges();
                log = audit.LastLog.Entities;
            }
            else if (type.IsAssignableFrom(typeof(Street)))
            {
                foreach (var e in entitiesToUpdate)
                {
                    var street = e as Street;
                    var oldStreet = db.Streets.Where(n => n.Id == street.Id).FirstOrDefault();

                    var newStreets = newData as List<Street>;
                    var newStreet = newStreets.Where(n => n.Id == oldStreet.Id).FirstOrDefault();

                    oldStreet.Name = newStreet.Name;
                    oldStreet.NameEn = newStreet.NameEn;
                    oldStreet.CityId = newStreet.CityId;
                    oldStreet.CityPostCode = newStreet.CityPostCode;
                    oldStreet.UpdatedTime = newStreet.UpdatedTime;

                }
                db.SaveChanges();
                log = audit.LastLog.Entities;
            }
            else if (type.IsAssignableFrom(typeof(Office)))
            {
                foreach (var e in entitiesToUpdate)
                {
                    var office = e as Office;
                    var oldOffice = db.Offices.Where(o => o.Id == office.Id).FirstOrDefault();

                    var newOffices = newData as List<Office>;
                    var newOffice = newOffices.Where(o => o.Id == oldOffice.Id).FirstOrDefault();

                    oldOffice.Name = newOffice.Name;
                    oldOffice.NameEn = newOffice.NameEn;
                    oldOffice.OfficeCode = newOffice.OfficeCode;
                    oldOffice.IsMachine = newOffice.IsMachine;
                    oldOffice.CityId = newOffice.CityId;
                    oldOffice.Lattitude = newOffice.Lattitude;
                    oldOffice.Longitude = newOffice.Longitude;
                    oldOffice.NeighbourhoodId = newOffice.NeighbourhoodId;
                    oldOffice.StreetId = newOffice.StreetId;
                    oldOffice.StreetNumber = newOffice.StreetNumber;
                    oldOffice.Entrance = newOffice.Entrance;
                    oldOffice.Floor = newOffice.Floor;
                    oldOffice.Appartment = newOffice.Appartment;
                    oldOffice.Other = newOffice.Other;
                    oldOffice.Phone = newOffice.Phone;
                    oldOffice.WorkBegin = newOffice.WorkBegin;
                    oldOffice.WorkEnd = newOffice.WorkEnd;
                    oldOffice.WorkBeginSaturday = newOffice.WorkBeginSaturday;
                    oldOffice.WorkEndSaturday = newOffice.WorkEndSaturday;
                    oldOffice.TimePriority = newOffice.TimePriority;
                    oldOffice.UpdatedTime = newOffice.UpdatedTime;


                }
                db.SaveChanges();
                log = audit.LastLog.Entities;
            }

            return log;
        }

        private TimeSpan GetTimeDifference(string oldDate, string newDate)
        {
            var dbDate = DateTime.ParseExact(oldDate, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
            var xmlDate = DateTime.ParseExact(newDate, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);

            TimeSpan difference = xmlDate - dbDate;

            return difference;
        }

        public Dictionary<string, List<AuditEntity>> InitiateZonesUpdate(Dictionary<string, XDocument> xmlData)
        {
            var db = new CacheRoomDbContext();
            var xmlParser = new XmlParser();
            XDocument zonesXmlDocument;
            xmlData.TryGetValue("zones", out zonesXmlDocument);

            var xmlZones = xmlParser.ParseZones(zonesXmlDocument, db);
            var dbZones = db.Zones.ToList();

            var entitiesToAdd = xmlZones.Where(c => !dbZones.Any(c2 => c2.Id == c.Id)).ToList();

            var entitiesToDelete = dbZones.Where(c => !xmlZones.Any(c2 => c2.Id == c.Id)).ToList();

            var entitiesToUpdate = dbZones.Where(c => xmlZones.Any(c2 => c2.Id == c.Id &&
                GetTimeDifference(c.UpdatedTime, c2.UpdatedTime).TotalHours > 0)).ToList();

            var reportDictionary = new Dictionary<string, List<AuditEntity>>();

            if (entitiesToAdd.Count != 0)
            {
                var logReport = EntityAdd<Zone>(entitiesToAdd);
                reportDictionary.Add("AddedZones", logReport);
            }
            if (entitiesToDelete.Count != 0)
            {
                var logReport = EntityDelete<Zone>(entitiesToDelete);
                reportDictionary.Add("DeletedZones", logReport);
            }
            if (entitiesToUpdate.Count != 0)
            {
                var logReport = EntityUpdate<Zone>(entitiesToUpdate, xmlZones);
                reportDictionary.Add("UpdatedZones", logReport);
            }

            return reportDictionary;
        }

        public Dictionary<string, List<AuditEntity>> InitiateCountriesUpdate(Dictionary<string, XDocument> xmlData)
        {
            var db = new CacheRoomDbContext();
            var xmlParser = new XmlParser();
            XDocument countriesXmlDocument;
            xmlData.TryGetValue("countries", out countriesXmlDocument);

            var xmlCountries = xmlParser.ParseCountries(countriesXmlDocument, db);
            var dbCountries = db.Countries.ToList();

            var entitiesToAdd = xmlCountries.Where(c => !dbCountries.Any(c2 => c2.Id == c.Id)).ToList();

            var entitiesToDelete = new List<Country>();

            var reportDictionary = new Dictionary<string, List<AuditEntity>>();

            if (entitiesToAdd.Count != 0)
            {
                var logReport = EntityAdd<Country>(entitiesToAdd);
                reportDictionary.Add("AddedCountries", logReport);
            }
            if (entitiesToDelete.Count != 0)
            {
                var logReport = EntityDelete<Country>(entitiesToDelete);
                reportDictionary.Add("DeletedCountries", logReport);
            }
            return reportDictionary;
        }

        public Dictionary<string, List<AuditEntity>> InitiateCitiesUpdate(Dictionary<string, XDocument> xmlData)
        {
            var db = new CacheRoomDbContext();
            var xmlParser = new XmlParser();
            XDocument citiesXmlDocument;
            xmlData.TryGetValue("cities", out citiesXmlDocument);

            var xmlCities = xmlParser.ParseCities(citiesXmlDocument, db);
            var dbCities = db.Cities.ToList();

            var entitiesToAdd = xmlCities.Where(c => !dbCities.Any(c2 => c2.Id == c.Id)).ToList();

            var entitiesToDelete = dbCities.Where(c => !xmlCities.Any(c2 => c2.Id == c.Id)).ToList();

            var entitiesToUpdate = dbCities.Where(c => xmlCities.Any(c2 => c2.Id == c.Id &&
                GetTimeDifference(c.UpdatedTime, c2.UpdatedTime).TotalHours > 0)).ToList();

            var reportDictinary = new Dictionary<string, List<AuditEntity>>();

            if (entitiesToAdd.Count != 0)
            {
                var logReport = EntityAdd<City>(entitiesToAdd);
                reportDictinary.Add("AddedCities", logReport);
            }
            if (entitiesToDelete.Count != 0)
            {
                var logReport = EntityDelete<City>(entitiesToDelete);
                reportDictinary.Add("DeletedCities", logReport);
            }
            if (entitiesToUpdate.Count != 0)
            {
                var logReport = EntityUpdate<City>(entitiesToUpdate, xmlCities);
                reportDictinary.Add("UpdatedCities", logReport);
            }

            return reportDictinary;
        }

        public Dictionary<string, List<AuditEntity>> InitiateNeighbourhoodsUpdate(Dictionary<string, XDocument> xmlData)
        {
            var db = new CacheRoomDbContext();
            var xmlParser = new XmlParser();
            XDocument neighbourhoodsXmlDocument;
            xmlData.TryGetValue("neighbours", out neighbourhoodsXmlDocument);

            var xmlNeighbourhoods = xmlParser.ParseNeighbours(neighbourhoodsXmlDocument, db);
            var dbNeighbourhoods = db.Neighbourhoods.ToList();

            var entitiesToAdd = xmlNeighbourhoods.Where(c => !dbNeighbourhoods.Any(c2 => c2.Id == c.Id)).ToList();

            var entitiesToDelete = dbNeighbourhoods.Where(c => !xmlNeighbourhoods.Any(c2 => c2.Id == c.Id)).ToList();

            var entitiesToUpdate = dbNeighbourhoods.Where(c => xmlNeighbourhoods.Any(c2 => c2.Id == c.Id &&
                GetTimeDifference(c.UpdatedTime, c2.UpdatedTime).TotalHours > 0)).ToList();

            var reportDictionary = new Dictionary<string, List<AuditEntity>>();
            if (entitiesToAdd.Count != 0)
            {
                var logReport = EntityAdd<Neighbourhood>(entitiesToAdd);
                reportDictionary.Add("AddedNeighbourhoods", logReport);
            }
            if (entitiesToDelete.Count != 0)
            {
                var logReport = EntityDelete<Neighbourhood>(entitiesToDelete);
                reportDictionary.Add("DeletedNeighbourhoods", logReport);
            }
            if (entitiesToUpdate.Count != 0)
            {
                var logReport = EntityUpdate<Neighbourhood>(entitiesToUpdate, xmlNeighbourhoods);
                reportDictionary.Add("UpdatedNeighbourhoods", logReport);
            }

            return reportDictionary;
        }

        public Dictionary<string, List<AuditEntity>> InitiateStreetsUpdate(Dictionary<string, XDocument> xmlData)
        {
            var db = new CacheRoomDbContext();
            var xmlParser = new XmlParser();
            XDocument streetsXmlDocument;
            xmlData.TryGetValue("streets", out streetsXmlDocument);

            var xmlStreets = xmlParser.ParseStreets(streetsXmlDocument, db);
            var dbStreets = db.Streets.ToList();

            var entitiesToAdd = xmlStreets.Where(c => !dbStreets.Any(c2 => c2.Id == c.Id)).ToList();

            var entitiesToDelete = dbStreets.Where(c => !xmlStreets.Any(c2 => c2.Id == c.Id)).ToList();

            var entitiesToUpdate = dbStreets.Where(c => xmlStreets.Any(c2 => c2.Id == c.Id &&
                GetTimeDifference(c.UpdatedTime, c2.UpdatedTime).TotalHours > 0)).ToList();

            var reportDictionary = new Dictionary<string, List<AuditEntity>>();

            if (entitiesToAdd.Count != 0)
            {
                var logReport = EntityAdd<Street>(entitiesToAdd);
                reportDictionary.Add("AddedStreets", logReport);
            }
            if (entitiesToDelete.Count != 0)
            {
                var logReport = EntityDelete<Street>(entitiesToDelete);
                reportDictionary.Add("DeletedStreets", logReport);
            }
            if (entitiesToUpdate.Count != 0)
            {
                var logReport = EntityUpdate<Street>(entitiesToUpdate, xmlStreets);
                reportDictionary.Add("UpdatedStreets", logReport);
            }
            return reportDictionary;
        }

        public Dictionary<string, List<AuditEntity>> InitiateOfficesUpdate(Dictionary<string, XDocument> xmlData)
        {
            var db = new CacheRoomDbContext();
            var xmlParser = new XmlParser();
            XDocument officesXmlDocument;
            xmlData.TryGetValue("offices", out officesXmlDocument);

            var xmlOffices = xmlParser.ParseOffices(officesXmlDocument, db);
            var dbOffices = db.Offices.ToList();

            var entitiesToAdd = xmlOffices.Where(c => !dbOffices.Any(c2 => c2.Id == c.Id)).ToList();

            var entitiesToDelete = dbOffices.Where(c => !xmlOffices.Any(c2 => c2.Id == c.Id)).ToList();

            var entitiesToUpdate = dbOffices.Where(c => xmlOffices.Any(c2 => c2.Id == c.Id &&
                GetTimeDifference(c.UpdatedTime, c2.UpdatedTime).TotalHours > 0)).ToList();

            var reportDictionary = new Dictionary<string, List<AuditEntity>>();
            if (entitiesToAdd.Count != 0)
            {
                var logReport = EntityAdd<Office>(entitiesToAdd);
                reportDictionary.Add("AddedOffices", logReport);
            }
            if (entitiesToDelete.Count != 0)
            {
                var logReport = EntityDelete<Office>(entitiesToDelete);
                reportDictionary.Add("DeletedOffices", logReport);
            }
            if (entitiesToUpdate.Count != 0)
            {
                var logReport = EntityUpdate<Office>(entitiesToUpdate, xmlOffices);
                reportDictionary.Add("UpdatedOffices", logReport);
            }

            return reportDictionary;
        }
    }
}
